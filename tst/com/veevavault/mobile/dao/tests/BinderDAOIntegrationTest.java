package com.veevavault.mobile.dao.tests;

import junit.framework.Assert;
import android.test.AndroidTestCase;

import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7_;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.request.RetrieveBinderRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.dao.request.SelectVaultRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveBinderResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;
import com.veevavault.mobile.model.Document;

public class BinderDAOIntegrationTest extends AndroidTestCase implements AuthenticateUserRequest.Listener,
   RetrieveDocumentsRequest.Listener, SelectVaultRequest.Listener, RetrieveBinderRequest.Listener {
   private static volatile AuthenticateUserResponse authResponse;
   private static volatile RetrieveBinderResponse binderResponse;
   private static volatile RetrieveDocumentsResponse documentsResponse;
   
   private static VeevaRestClientv7 veevaClient = new VeevaRestClientv7_();
   
   public void testRetrieveBinder() throws InterruptedException {
      AuthenticateUserRequest authRequest = new AuthenticateUserRequest();
      SelectVaultRequest selectVaultRequest = new SelectVaultRequest();
      RetrieveDocumentsRequest documentsRequest = new RetrieveDocumentsRequest();
      RetrieveBinderRequest binderRequest = new RetrieveBinderRequest();
      documentsResponse = null;
      authResponse = null;
      binderResponse = null;
      
      authRequest.setRestClient(veevaClient);
      authRequest.setUsername("mnichols@calpoly-capstone.com");
      authRequest.setPassword("QWERTY123");
      authRequest.setCallback(this);
      authRequest.setContext(this.getContext());
      
      authRequest.execute();
      
      while (authResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, authResponse.getResponseStatus());
      Assert.assertNotNull(authResponse.getVaults());
      
      selectVaultRequest.setVaultEndpoint(authResponse.getVaults().get(0));
      selectVaultRequest.setContext(this.getContext());
      selectVaultRequest.setCallback(this);
      selectVaultRequest.execute();
      
      documentsRequest.setNamed_filter("My Documents");
      documentsRequest.setLimit(200);
      documentsRequest.setCallback(this);
      documentsRequest.setContext(this.getContext());
      
      documentsRequest.execute();
      
      while (documentsResponse == null) {
         Thread.sleep(1000);
      }
      
      for (Document document : documentsResponse.getDocuments()) {
         if (document.isBinder()) {
            binderRequest.setBinderId(document.getId());
            binderRequest.setCallback(this);
            binderRequest.setContext(this.getContext());
            binderRequest.execute();
            break;
         }
      }
      
      while (binderResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertTrue(binderResponse.getBinderContents().size() > 0);
   }
   
   @Override
   public void onRetrieveDocumentsResponse(RetrieveDocumentsResponse response) {
      documentsResponse = response;
   }

   @Override
   public void onAuthenticateResponse(AuthenticateUserResponse response) {
      authResponse = response;      
   }

   @Override
   public void onSelectVaultResponse() {
      // TODO Auto-generated method stub
      
   }

   @Override
   public void onRetrieveBinderResponse(RetrieveBinderResponse response) {
      binderResponse = response;
   }
}

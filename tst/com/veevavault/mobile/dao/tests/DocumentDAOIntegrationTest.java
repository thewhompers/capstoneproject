package com.veevavault.mobile.dao.tests;

import java.math.BigInteger;

import junit.framework.Assert;
import android.test.AndroidTestCase;

import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7_;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentThumbnailRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.dao.request.SelectVaultRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveDocumentResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentThumbnailResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;

public class DocumentDAOIntegrationTest extends AndroidTestCase implements AuthenticateUserRequest.Listener, RetrieveDocumentRequest.Listener,
   RetrieveDocumentsRequest.Listener, RetrieveDocumentThumbnailRequest.Listener, SelectVaultRequest.Listener {
   private static volatile AuthenticateUserResponse authResponse;
   private static volatile RetrieveDocumentResponse retrieveOneResponse;
   private static volatile RetrieveDocumentsResponse retrieveResponse;
   private static volatile RetrieveDocumentThumbnailResponse thumbnailResponse;
   
   private static VeevaRestClientv7 veevaClient = new VeevaRestClientv7_();
   
   public void testRetrieveDocuments() throws InterruptedException {
      AuthenticateUserRequest authRequest = new AuthenticateUserRequest();
      SelectVaultRequest selectVaultRequest = new SelectVaultRequest();
      RetrieveDocumentsRequest retrieveRequest = new RetrieveDocumentsRequest();
      retrieveResponse = null;
      authResponse = null;
      
      authRequest.setRestClient(veevaClient);
      authRequest.setUsername("mnichols@calpoly-capstone.com");
      authRequest.setPassword("QWERTY123");
      authRequest.setCallback(this);
      authRequest.setContext(this.getContext());
      
      authRequest.execute();
      
      while (authResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, authResponse.getResponseStatus());
      Assert.assertNotNull(authResponse.getVaults());
      
      selectVaultRequest.setVaultEndpoint(authResponse.getVaults().get(0));
      selectVaultRequest.setContext(this.getContext());
      selectVaultRequest.execute();
      
      retrieveRequest.setNamed_filter("My Documents");
      retrieveRequest.setLimit(1);
      retrieveRequest.setCallback(this);
      retrieveRequest.setContext(this.getContext());
      
      retrieveRequest.execute();
      
      while (retrieveResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(1, retrieveResponse.getDocuments().get(0).getId());
   }
   
   public void testRetrieveDocument() throws InterruptedException {
      AuthenticateUserRequest authRequest = new AuthenticateUserRequest();
      SelectVaultRequest selectVaultRequest = new SelectVaultRequest();
      RetrieveDocumentRequest retrieveRequest = new RetrieveDocumentRequest();
      retrieveOneResponse = null;
      authResponse = null;
      
      authRequest.setRestClient(veevaClient);
      authRequest.setUsername("mnichols@calpoly-capstone.com");
      authRequest.setPassword("QWERTY123");
      authRequest.setCallback(this);
      authRequest.setContext(this.getContext());
      
      authRequest.execute();
      
      while (authResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, authResponse.getResponseStatus());
      Assert.assertNotNull(authResponse.getVaults());
      
      selectVaultRequest.setVaultEndpoint(authResponse.getVaults().get(0));
      selectVaultRequest.setCallback(this);
      selectVaultRequest.setContext(this.getContext());
      selectVaultRequest.execute();
      
      retrieveRequest.setDocumentId(BigInteger.valueOf(121));
      retrieveRequest.setCallback(this);
      retrieveRequest.setContext(this.getContext());
      
      retrieveRequest.execute();
      
      while (retrieveOneResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(121, retrieveOneResponse.getDocument().getId());
   }
   
   public void testRetrieveDocumentThumbnail() throws InterruptedException {
      AuthenticateUserRequest authRequest = new AuthenticateUserRequest();
      SelectVaultRequest selectVaultRequest = new SelectVaultRequest();
      RetrieveDocumentThumbnailRequest thumbnailRequest = new RetrieveDocumentThumbnailRequest();
      thumbnailResponse = null;
      authResponse = null;
      
      authRequest.setRestClient(veevaClient);
      authRequest.setUsername("mnichols@calpoly-capstone.com");
      authRequest.setPassword("QWERTY123");
      authRequest.setCallback(this);
      authRequest.setContext(this.getContext());
      
      authRequest.execute();
      
      while (authResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, authResponse.getResponseStatus());
      Assert.assertNotNull(authResponse.getVaults());
      
      selectVaultRequest.setVaultEndpoint(authResponse.getVaults().get(0));
      selectVaultRequest.setContext(this.getContext());
      selectVaultRequest.execute();
      
      thumbnailRequest.setDocumentId(BigInteger.valueOf(1));
      thumbnailRequest.setMajorVersion("1");
      thumbnailRequest.setMinorVersion("0");
      thumbnailRequest.setMaxHeight(30);
      thumbnailRequest.setMaxWidth(30);
      thumbnailRequest.setCallback(this);
      thumbnailRequest.setContext(this.getContext());
      
      thumbnailRequest.execute();
      
      while (thumbnailResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertNotNull(thumbnailResponse.getImage());
   }
   
   @Override
   public void onRetrieveDocumentResponse(RetrieveDocumentResponse response) {
      retrieveOneResponse = response;
   }
   
   @Override
   public void onRetrieveDocumentsResponse(RetrieveDocumentsResponse response) {
      retrieveResponse = response;
   }

   @Override
   public void onAuthenticateResponse(AuthenticateUserResponse response) {
      authResponse = response;      
   }

   @Override
   public void onThumbnailResponse(RetrieveDocumentThumbnailResponse response) {
      thumbnailResponse = response;
   }

   @Override
   public void onSelectVaultResponse() {
      // TODO Auto-generated method stub
      
   }
}

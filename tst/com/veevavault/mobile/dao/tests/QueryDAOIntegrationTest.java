package com.veevavault.mobile.dao.tests;

import junit.framework.Assert;
import android.test.AndroidTestCase;

import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7_;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentPropertiesRequest;
import com.veevavault.mobile.dao.request.SelectVaultRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveDocumentPropertiesResponse;

public class QueryDAOIntegrationTest extends AndroidTestCase
   implements AuthenticateUserRequest.Listener, RetrieveDocumentPropertiesRequest.Listener {
   private static volatile AuthenticateUserResponse authResponse;
   private static volatile RetrieveDocumentPropertiesResponse queryResponse;
   
   private static VeevaRestClientv7 veevaClient = new VeevaRestClientv7_();
   
   public void testRetrieveWorkflows() throws InterruptedException {
      AuthenticateUserRequest authRequest = new AuthenticateUserRequest();
      SelectVaultRequest selectVaultRequest = new SelectVaultRequest();
      RetrieveDocumentPropertiesRequest retrieveRequest = new RetrieveDocumentPropertiesRequest(null);
      authResponse = null;
      queryResponse = null;
      
      authRequest.setRestClient(veevaClient);
      authRequest.setUsername("mnichols@calpoly-capstone.com");
      authRequest.setPassword("QWERTY123");
      authRequest.execute();
      
      while (authResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, authResponse.getResponseStatus());
      Assert.assertNotNull(authResponse.getVaults());
      
      selectVaultRequest.setVaultEndpoint(authResponse.getVaults().get(0));
      selectVaultRequest.execute();
      
      retrieveRequest.setRestClient(veevaClient);
      retrieveRequest.execute();
      
      while (queryResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, queryResponse.getResponseStatus());
      Assert.assertEquals("OK", queryResponse.getResponseMessage());
   }
   
   public void testRetrieveDocumentProperties() throws InterruptedException {
      AuthenticateUserRequest authRequest = new AuthenticateUserRequest();
      SelectVaultRequest selectVaultRequest = new SelectVaultRequest();
//      QueryRequest retrieveRequest = new QueryRequest();
      authResponse = null;
      queryResponse = null;
      
      authRequest.setRestClient(veevaClient);
      authRequest.setUsername("mnichols@calpoly-capstone.com");
      authRequest.setPassword("QWERTY123");
      authRequest.execute();
      
      while (authResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, authResponse.getResponseStatus());
      Assert.assertNotNull(authResponse.getVaults());
      
      selectVaultRequest.setVaultEndpoint(authResponse.getVaults().get(0));
      selectVaultRequest.execute();
      
//      retrieveRequest.setCaller(getContext());
//      retrieveRequest.setListener(this);
//      
//      retrieveRequest.setQuery(QueryDAO.makeMetadataQuery("documents"));
//      
//      QueryDAO.query(retrieveRequest);
      
//      while (QueryDAOIntegrationTest.queryResponse == null) {
//         Thread.sleep(1000);
//      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, queryResponse.getResponseStatus());
      Assert.assertEquals("OK", queryResponse.getResponseMessage());
   }

   @Override
   public void onAuthenticateResponse(AuthenticateUserResponse response) {
      authResponse = response;
   }

   @Override
   public void onDocumentPropertiesResponse(
      RetrieveDocumentPropertiesResponse response) {
      queryResponse = response;
   }

}

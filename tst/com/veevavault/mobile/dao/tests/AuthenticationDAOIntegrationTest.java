package com.veevavault.mobile.dao.tests;

import java.util.concurrent.ExecutionException;

import junit.framework.Assert;

import org.json.JSONException;

import android.test.AndroidTestCase;

import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7_;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.model.VaultEndpoint;

public class AuthenticationDAOIntegrationTest extends AndroidTestCase
   implements AuthenticateUserRequest.Listener {
   private static volatile AuthenticateUserResponse daoResponse;
   
   private static VeevaRestClientv7 veevaClient = new VeevaRestClientv7_();
   
   public void testAuthenticateUser() throws JSONException, InterruptedException, ExecutionException {
      AuthenticateUserRequest daoRequest = new AuthenticateUserRequest();
      AuthenticationDAOIntegrationTest.daoResponse = null;
      
      daoRequest.setRestClient(veevaClient);
      daoRequest.setUsername("mnichols@calpoly-capstone.com");
      daoRequest.setPassword("QWERTY123");
      daoRequest.setCallback(this);
      daoRequest.setContext(this.getContext());
      daoRequest.execute();
      
      while (daoResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, daoResponse.getResponseStatus());
      Assert.assertNotNull(daoResponse.getVaults());
   }
   
   public void testAuthenticateUserDns() throws JSONException, InterruptedException, ExecutionException {
      AuthenticateUserRequest daoRequest = new AuthenticateUserRequest();
      AuthenticationDAOIntegrationTest.daoResponse = null;
      
      VaultEndpoint endpoint = new VaultEndpoint();
      endpoint.setDnsName("https://calpoly-capstone5.veevavault.com/api");
      endpoint.setName("Capstone 5");
      endpoint.setVaultID("2289");
      
      daoRequest.setRestClient(veevaClient);
      daoRequest.setUsername("mnichols@calpoly-capstone.com");
      daoRequest.setPassword("QWERTY123");
      daoRequest.setDomain(endpoint);
      daoRequest.setCallback(this);
      daoRequest.setContext(this.getContext());
      daoRequest.execute();
      
      while (daoResponse == null) {
         Thread.sleep(1000);      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, daoResponse.getResponseStatus());
      Assert.assertNotNull(daoResponse.getVaults());
   }
   
   public void testAuthenticateUserBadPassword() throws JSONException, InterruptedException, ExecutionException {
      AuthenticateUserRequest daoRequest = new AuthenticateUserRequest();
      AuthenticationDAOIntegrationTest.daoResponse = null;
      
      daoRequest.setRestClient(veevaClient);
      daoRequest.setUsername("mnichols@calpoly-capstone.com");
      daoRequest.setPassword("PASSWORD");
      daoRequest.setCallback(this);
      daoRequest.setContext(this.getContext());
      daoRequest.execute();
      
      while (daoResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.FAILURE, daoResponse.getResponseStatus());
   }

   @Override
   public void onAuthenticateResponse(AuthenticateUserResponse response) {
      daoResponse = response;
   }
}

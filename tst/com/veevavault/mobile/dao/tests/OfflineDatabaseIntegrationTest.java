package com.veevavault.mobile.dao.tests;

import java.math.BigInteger;
import java.util.HashSet;

import android.graphics.BitmapFactory;
import android.test.AndroidTestCase;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7_;
import com.veevavault.mobile.dao.request.LoadDownloadedDocumentsRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.dao.request.SaveDownloadedDocumentsRequest;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Property;

public class OfflineDatabaseIntegrationTest extends AndroidTestCase implements SaveDownloadedDocumentsRequest.Listener,
      LoadDownloadedDocumentsRequest.Listener, RetrieveDocumentsRequest.Listener {
   private static volatile boolean saveDocumentsResponse;
   private static volatile boolean loadDocumentsResponse;
   private static volatile RetrieveDocumentsResponse retrieveDocumentsResponse;
   
   Property p1, p2;
   
   private static VeevaRestClientv7 veevaClient = new VeevaRestClientv7_();
   
   public void testPropertyEquals() {
      p1 = new Property();
      p1.setHidden(true);
      p1.setId(0);
      p1.setLabel("label");
      p1.setName("name");
      p1.setQueryable(false);
      p1.setSection("section");
      p1.setSectionPosition(1);
      p1.setValue("Property 0 Value");
      
      p2 = new Property();
      p2.setHidden(true);
      p2.setId(0);
      p2.setLabel("label");
      p2.setName("name");
      p2.setQueryable(false);
      p2.setSection("section");
      p2.setSectionPosition(1);
      p2.setValue("Property 0 Value");
      
      assertTrue(p1.equals(p2));
   }
   
   public void testDocumentEquals() {
      Document document1 = new Document(BigInteger.valueOf(1337));
      document1.setId(BigInteger.valueOf(0));
      document1.setFilePath(null);
      document1.setThumbnail(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.doc_icon_doc));
      document1.setProperties(constructTestDocumentProperties());
      
      Document document2 = new Document(BigInteger.valueOf(1337));
      document2.setId(BigInteger.valueOf(0));
      document2.setFilePath(null);
      document2.setThumbnail(document1.getThumbnail());
      document2.setProperties(constructTestDocumentProperties());
      
      assertEquals(document1, document2);
   }
   
   public void testSaveDocumentToCache() throws InterruptedException {
      saveDocumentsResponse = false;
      loadDocumentsResponse = false;
      retrieveDocumentsResponse = null;
      
      Document document = new Document(BigInteger.valueOf(1337));
      document.setId(BigInteger.valueOf(0));
      document.setFilePath(null);
      document.setThumbnail(BitmapFactory.decodeResource(getContext().getResources(), R.drawable.doc_icon_doc));
      document.setProperties(constructTestDocumentProperties());
      DocumentDAO.saveDocumentToDownloads(document);
      
      SaveDownloadedDocumentsRequest request = new SaveDownloadedDocumentsRequest();
      request.setContext(getContext());
      request.setCallback(this);
      request.execute();
      
      while (!saveDocumentsResponse) {
         Thread.sleep(1000);
      }
      
      LoadDownloadedDocumentsRequest req = new LoadDownloadedDocumentsRequest();
      req.setContext(getContext());
      req.setCallback(this);
      req.execute();
      
      while (!loadDocumentsResponse) {
         Thread.sleep(1000);
      }
      
      RetrieveDocumentsRequest docRequest = new RetrieveDocumentsRequest();
      docRequest.setCallback(this);
      docRequest.setOnDevice(true);
      docRequest.execute();
      
      while (retrieveDocumentsResponse == null) {
         Thread.sleep(1000);
      }
      
      
     // assertEquals(1, retrieveDocumentsResponse.getDocuments().size());
      Document doc = retrieveDocumentsResponse.getDocuments().get(0);
      assertEquals(document, doc);
      
   }

   private HashSet<Property> constructTestDocumentProperties() {
      HashSet<Property> properties = new HashSet<Property>();
      
      p1 = new Property();
      p1.setHidden(true);
      p1.setId(0);
      p1.setLabel("label");
      p1.setName("name");
      p1.setQueryable(false);
      p1.setSection("section");
      p1.setSectionPosition(1);
      p1.setValue("Property 0 Value");
      
      properties.add(p1);
      
      p2 = new Property();
      p2.setHidden(true);
      p2.setId(1);
      p2.setLabel("label");
      p2.setName("id");
      p2.setQueryable(false);
      p2.setSection("section");
      p2.setSectionPosition(0);
      p2.setValue("1");
      
      properties.add(p2);
      return properties;  
   }

   @Override
   public void onSaveDownloadedDocumentsResponse() {
      saveDocumentsResponse = true;
   }

   public void onLoadDownloadedDocumentsResponse() {
      loadDocumentsResponse = true;
   }

   @Override
   public void onRetrieveDocumentsResponse(RetrieveDocumentsResponse response) {
      retrieveDocumentsResponse = response;
   }
}

package com.veevavault.mobile.dao.tests;

import junit.framework.Assert;
import android.test.AndroidTestCase;

import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7_;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.request.RetrieveTasksRequest;
import com.veevavault.mobile.dao.request.SelectVaultRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveTasksResponse;

public class WorkflowDAOIntegrationTest extends AndroidTestCase 
   implements AuthenticateUserRequest.Listener, RetrieveTasksRequest.Listener {
   private static volatile AuthenticateUserResponse authResponse;
   private static volatile RetrieveTasksResponse retrieveResponse;
   public void testRetrieveWorkflows() throws InterruptedException {
      AuthenticateUserRequest authRequest = new AuthenticateUserRequest();
      SelectVaultRequest selectVaultRequest = new SelectVaultRequest();
      RetrieveTasksRequest retrieveRequest = new RetrieveTasksRequest();
      authResponse = null;
      retrieveResponse = null;
      
      VeevaRestClientv7 veevaClient = new VeevaRestClientv7_();
      
      authRequest.setRestClient(veevaClient);
      authRequest.setUsername("mnichols@calpoly-capstone.com");
      authRequest.setPassword("QWERTY123");
      authRequest.execute();
      
      while (authResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, authResponse.getResponseStatus());
      Assert.assertNotNull(authResponse.getVaults());
      
      selectVaultRequest.setVaultEndpoint(authResponse.getVaults().get(0));
      selectVaultRequest.execute();
      retrieveRequest.execute();
      
      while (retrieveResponse == null) {
         Thread.sleep(1000);
      }
      
      Assert.assertEquals(ResponseStatus.SUCCESS, retrieveResponse.getResponseStatus());
      Assert.assertEquals("OK", retrieveResponse.getResponseMessage());
   }
   @Override
   public void onRetrieveTasksResponse(RetrieveTasksResponse response) {
      retrieveResponse = response;
   }
   
   @Override
   public void onAuthenticateResponse(AuthenticateUserResponse response) {
      authResponse = response;
   }
}

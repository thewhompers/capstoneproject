package com.veevavault.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.veevavault.mobile.model.Task;
import com.veevavault.mobile.view.TaskListItemView;

public class TaskListAdapter extends BaseAdapter{

	private Context context;
	private List<Task> taskList = new ArrayList<Task>();
	
	public TaskListAdapter(Context context, List<Task> tasks)
	{
		this.context = context;
	    this.taskList = tasks;
	}
	
	@Override
	public int getCount() {
		return taskList.size();
	}

	@Override
	public Object getItem(int position) {
		return taskList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		
		return new TaskListItemView(this.context, this.taskList.get(position));
	}

}

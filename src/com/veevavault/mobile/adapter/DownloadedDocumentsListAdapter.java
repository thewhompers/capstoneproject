package com.veevavault.mobile.adapter;

import java.util.List;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import com.veevavault.mobile.view.DownloadedDocumentItemView;

public class DownloadedDocumentsListAdapter extends BaseAdapter {
	private final Context context;
	private final List<String> filePathList;

	public DownloadedDocumentsListAdapter(Context context, List<String> paths) {
		super();
		this.context = context;
		this.filePathList = paths;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		String filePath = filePathList.get(position);
		return new DownloadedDocumentItemView(context, filePath);
	}

	@Override
	public int getCount() {
		return filePathList.size();
	}

	@Override
	public Object getItem(int position) {
		return filePathList.get(position);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}
}

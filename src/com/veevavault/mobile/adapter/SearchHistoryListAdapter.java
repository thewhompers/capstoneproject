package com.veevavault.mobile.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.view.SearchHistoryItemView;

public class SearchHistoryListAdapter extends BaseAdapter {
	private final Context context;
	private final List<RetrieveDocumentsRequest> searchRequestList;

	public SearchHistoryListAdapter(Context context, List<RetrieveDocumentsRequest> searchedDocuments) {
		super();
		this.context = context;
		this.searchRequestList = searchedDocuments;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		RetrieveDocumentsRequest request = searchRequestList.get(position);
		request.setContext(context);
		return new SearchHistoryItemView(context, request);
	}

	@Override
	public int getCount() {
		return searchRequestList.size();
	}

	@Override
	public Object getItem(int position) {
		return searchRequestList.get(position);
	}

	@Override
	public long getItemId(int id) {
		return id;
	}
}
package com.veevavault.mobile.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Section;
import com.veevavault.mobile.view.DocumentListItemView;
import com.veevavault.mobile.view.DocumentListItemView.OnDownloadDocumentListener;
import com.veevavault.mobile.view.SectionListItemView;

public class BindableListAdapter extends BaseAdapter {
   protected final Context context;
   protected final List<Bindable> values;
   protected OnDownloadDocumentListener callback;

   public BindableListAdapter(Context context, OnDownloadDocumentListener callback, List<Bindable> values) {
      super();
      this.context = context;
      this.values = values;
      this.callback = callback;
   }


   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
      View view;
      if (values.get(position).getClass() == Document.class) {
         Document doc = (Document) values.get(position);
         view = new DocumentListItemView(context, callback, doc);
      }
      else {
         Section section = (Section) values.get(position);
         view = new SectionListItemView(context, section);
      }
         
      return view;
   }

   @Override
   public int getCount() {
      return values.size();
   }

   @Override
   public Object getItem(int arg0) {
      return values.get(arg0);
   }

   @Override
   public long getItemId(int arg0) {
      return arg0;
   }
}

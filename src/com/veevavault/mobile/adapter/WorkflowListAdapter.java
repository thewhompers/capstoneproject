package com.veevavault.mobile.adapter;

import java.util.List;

import com.veevavault.mobile.model.ActiveWorkflow;
import com.veevavault.mobile.view.WorkflowListItemView;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

public class WorkflowListAdapter extends BaseAdapter {
	
	/** The application Context in which this WorkflowListAdapter is being used. */
	private Context context;

	/** The data set to which this WorkflowListAdapter is bound. */
	private List<ActiveWorkflow> activeWorkflowList;
	
	
	private int selectedPosition;

	/**
	 * Parameterized constructor that takes in the application Context in which
	 * it is being used and the Collection of ActiveWorkflow objects to which it is bound.
	 * 
	 * @param context
	 *            The application Context in which this JokeListAdapter is being
	 *            used.
	 * 
	 * @param activeWorkflowList
	 *            The Collection of ActiveWorkflow objects to which this WorkflowListAdapter
	 *            is bound.
	 */
	public WorkflowListAdapter(Context context, List<ActiveWorkflow> activeWorkflowList) {
		this.context = context;
		this.activeWorkflowList = activeWorkflowList;
	}

	@Override
	public int getCount() {
		return this.activeWorkflowList.size();
	}

	@Override
	public Object getItem(int position) {
		return this.activeWorkflowList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return new WorkflowListItemView(this.context, this.activeWorkflowList.get(position));
	}
}

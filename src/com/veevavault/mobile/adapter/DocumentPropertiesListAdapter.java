package com.veevavault.mobile.adapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.Property;
import com.veevavault.mobile.view.PropertySectionChildItemView;

public class DocumentPropertiesListAdapter extends BaseExpandableListAdapter{
	
	private Context context;
	private List<String> sectionNames;
	private MultiValueMap<String,Property> sectionValueMap;
	//private HashMap<String,List<PropertyMetadata>> sectionValueMap;
	
	public DocumentPropertiesListAdapter(Context context,
											MultiValueMap<String,Property> map){
		
		this.context = context;
		this.sectionValueMap = configureSectionValueMap(map);
		this.sectionNames = new ArrayList<String>(sectionValueMap.keySet());
	}

	private MultiValueMap<String, Property> configureSectionValueMap(
         MultiValueMap<String, Property> sectionValueOld) {
      MultiValueMap<String, Property> propertiesBySection = new LinkedMultiValueMap<String, Property>();
      
	   for (Entry<String, List<Property>> entry : sectionValueOld.entrySet()) {
	      List<Property> propertyList = entry.getValue();
         List<Property> filteredPropertyList = new ArrayList<Property>();
	      
	      for (Property property : propertyList) {
	         if (property.isHidden()) {
	            continue;
	         }
	         filteredPropertyList.add(property);
	      }
	      
	      handleSpecialCases(filteredPropertyList);
	      Collections.sort(filteredPropertyList);
	      
	      if (!filteredPropertyList.isEmpty()) {
	         propertiesBySection.put(entry.getKey(), filteredPropertyList);
	      }
	   }
	   
	   return propertiesBySection;
   }

   private void handleSpecialCases(List<Property> list) {
      Property majorVersion = null, minorVersion = null, size = null, version = null;
      
      for (Property p : list) {
         if (p.getName().equals("major_version_number__v")) {
            majorVersion = p;
         } else if (p.getName().equals("minor_version_number__v")) {
            minorVersion = p;
         }
      }
      
      if (majorVersion != null) {
         version = new Property(majorVersion);
         list.remove(majorVersion);
         list.remove(minorVersion);
         version.setName("version");
         version.setLabel("Version");
         version.setValue(StringUtils.join(
            Arrays.asList(majorVersion.getValue(), minorVersion.getValue()), "."));
         list.add(version);
      }
      
   }

   @Override
	public Object getChild(int groupPosition, int childPosition) {
		
		return sectionValueMap.get(sectionNames.get(groupPosition)).get(childPosition);
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		
		return childPosition;
	}

	@Override
	public View getChildView(int groupPosition, int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		return new PropertySectionChildItemView(context, sectionNames.get(groupPosition), 
		      sectionValueMap.get(sectionNames.get(groupPosition)).get(childPosition));
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		return sectionValueMap.get(sectionNames.get(groupPosition)).size();
	}

	@Override
	public Object getGroup(int groupPosition) {
		return sectionNames.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return sectionValueMap.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String sectionName = (String) getGroup(groupPosition);
		
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			/*if(sectionName.equals("Supporting Documents")){
				convertView = infalInflater.inflate(R.layout.supporting_section_child_item, null);
			}*/
			convertView = infalInflater.inflate(R.layout.properties_section_group_item, null);
			
			
		}

		TextView sectionTextView = (TextView) convertView.findViewById(R.id.p_sectionName);
		sectionTextView.setText(sectionName);
		
		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return false;
	}
}

package com.veevavault.mobile.adapter;

import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Section;
import com.veevavault.mobile.view.DocumentGridItemView;
import com.veevavault.mobile.view.DocumentListItemView.OnDownloadDocumentListener;
import com.veevavault.mobile.view.SectionGridItemView;

public class BindableGridAdapter extends BindableListAdapter {

   public BindableGridAdapter(Context context,
         OnDownloadDocumentListener callback, List<Bindable> docList) {
      super(context, callback, docList);
   }

   @Override
   public View getView(int position, View convertView, ViewGroup parent) {
      View view;
      if (values.get(position).getClass() == Document.class) {
         Document doc = (Document) values.get(position);
         view = new DocumentGridItemView(context, callback, doc);
      }
      else {
         Section section = (Section) values.get(position);
         view = new SectionGridItemView(context, section);
      }
         
      return view;
   }
}

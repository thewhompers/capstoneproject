package com.veevavault.mobile.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.VaultEndpoint;
import com.veevavault.mobile.view.VaultListItemView;

public class VaultArrayAdapter extends BaseAdapter {

	private Context mContext;
	private List<VaultEndpoint> mVaultList = new ArrayList<VaultEndpoint>();
	
	
	public VaultArrayAdapter(Context context, ArrayList<VaultEndpoint> vaults) {
	     mContext = context;
	     mVaultList = vaults;
	}
	
	public int getCount() {
		return mVaultList.size();
	}

	@Override
	public Object getItem(int position) {
		return mVaultList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView;
		
		if (convertView == null) {
			rowView = new VaultListItemView(mContext, mVaultList.get(position));
		} 
		else {
			VaultEndpoint vaultep = mVaultList.get(position);
			
			LayoutInflater inflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	 
			rowView = inflater.inflate(R.layout.vault_listitem, parent, false);
			TextView id = (TextView) rowView.findViewById(R.id.vault_nameTextView);				
			id.setText(vaultep.getName());
		}
 
		return rowView;
	}

}

package com.veevavault.mobile.dao.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.Property;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveDocumentPropertiesResponse extends DAOResponse {
   private List<Property> properties;
}

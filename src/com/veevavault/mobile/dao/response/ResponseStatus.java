package com.veevavault.mobile.dao.response;

public enum ResponseStatus {
   SUCCESS,
   FAILURE,
   EXCEPTION;
}

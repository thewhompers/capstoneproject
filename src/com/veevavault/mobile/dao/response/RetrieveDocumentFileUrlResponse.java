package com.veevavault.mobile.dao.response;

import com.veevavault.mobile.model.Document;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveDocumentFileUrlResponse extends DAOResponse {
   private Document document;
   private String documentFileUrl;
   private String sessionId;
}

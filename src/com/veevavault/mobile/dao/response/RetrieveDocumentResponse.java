package com.veevavault.mobile.dao.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.Document;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveDocumentResponse extends DAOResponse {
   private Document document;
}

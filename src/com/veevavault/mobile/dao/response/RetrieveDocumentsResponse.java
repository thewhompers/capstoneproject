package com.veevavault.mobile.dao.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.Document;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveDocumentsResponse extends DAOResponse {
	private List<Document> documents;
	private int maxSize;
}

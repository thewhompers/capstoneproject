package com.veevavault.mobile.dao.response;

import lombok.Data;

@Data
public abstract class DAOResponse {
   private ResponseStatus responseStatus;
   private String responseMessage;
   private ErrorType errorType;
}

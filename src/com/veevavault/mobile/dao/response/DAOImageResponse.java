package com.veevavault.mobile.dao.response;

import java.math.BigInteger;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.graphics.Bitmap;

@Data
@EqualsAndHashCode(callSuper=false)
public class DAOImageResponse extends DAOResponse {
   private BigInteger documentId;
   private Bitmap image;
}

package com.veevavault.mobile.dao.response;

import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.dao.api.PropertySchema.PropertyMetadata;

@Data
@EqualsAndHashCode(callSuper=false)
public class QueryResponse extends DAOResponse {
   private List<Map<String, PropertyMetadata>> results;
}

package com.veevavault.mobile.dao.response;

public enum ErrorType {
   INVALID_DOCUMENT,
   INVALID_BINDER;
}

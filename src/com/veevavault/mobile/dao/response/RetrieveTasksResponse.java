package com.veevavault.mobile.dao.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.Task;


@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveTasksResponse extends DAOResponse {
   private List<Task> tasks;
}

package com.veevavault.mobile.dao.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.BindableIdentifier;
import com.veevavault.mobile.model.Document;


@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveBinderResponse extends DAOResponse {
   private Document binderDocument;
   private List<BindableIdentifier> binderContents;
}

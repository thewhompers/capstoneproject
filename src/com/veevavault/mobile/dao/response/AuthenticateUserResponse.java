package com.veevavault.mobile.dao.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.VaultEndpoint;

@Data
@EqualsAndHashCode(callSuper=false)
public class AuthenticateUserResponse extends DAOResponse {
   private String sessionId;
   private List<VaultEndpoint> vaults;
}

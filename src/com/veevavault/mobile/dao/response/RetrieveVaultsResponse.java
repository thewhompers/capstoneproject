package com.veevavault.mobile.dao.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.VaultEndpoint;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveVaultsResponse extends DAOResponse {
   private String username;
   private List<VaultEndpoint> vaults;
}

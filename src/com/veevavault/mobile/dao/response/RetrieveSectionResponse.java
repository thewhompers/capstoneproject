package com.veevavault.mobile.dao.response;

import java.util.List;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.model.BindableIdentifier;
import com.veevavault.mobile.model.Section;


@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveSectionResponse extends DAOResponse {
   private Section section;
   private List<BindableIdentifier> sectionContents;
}

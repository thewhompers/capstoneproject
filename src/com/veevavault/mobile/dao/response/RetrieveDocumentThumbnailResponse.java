package com.veevavault.mobile.dao.response;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveDocumentThumbnailResponse extends DAOImageResponse {
}

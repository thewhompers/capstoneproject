package com.veevavault.mobile.dao.response;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveSearchHistoryResponse extends DAOResponse {
   private Map<RetrieveDocumentsRequest, RetrieveDocumentsResponse> searchHistory;
}

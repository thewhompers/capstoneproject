package com.veevavault.mobile.dao.response;

import lombok.Data;
import lombok.EqualsAndHashCode;
import com.veevavault.mobile.model.User;

@Data
@EqualsAndHashCode(callSuper=false)
public class LoadUserResponse extends DAOResponse {
   private User user;
}

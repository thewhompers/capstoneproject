package com.veevavault.mobile.dao;

import static com.veevavault.mobile.dao.JsonParser.getElement;
import static com.veevavault.mobile.dao.JsonParser.getElementList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lombok.Data;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonElement;
import com.veevavault.mobile.dao.api.PropertySchema;
import com.veevavault.mobile.dao.api.PropertySchema.PropertyMetadata;
import com.veevavault.mobile.dao.api.PropertySchemaType;
import com.veevavault.mobile.dao.request.DAORequest;
import com.veevavault.mobile.dao.response.DAOResponse;
import com.veevavault.mobile.dao.response.QueryResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;

public class QueryDAO extends BaseDAO {
   @Data
   public static class Query {
      private List<String> columnNames;
      private PropertySchemaType table;
      private Map<String, String> constraints;
      
      public Query(List<String> columnNames, PropertySchemaType table, Map<String, String> constraints) {
         this.columnNames = columnNames;
         this.table = table;
         this.constraints = constraints;
      }
      
      public String toString() {
         String query = String.format("SELECT %s FROM %s", StringUtils.join(columnNames, ", "), table);
         
         if (constraints != null && constraints.size() > 0) {            
            List<String> constraintString = new ArrayList<String>();
            
            for (Entry<String, String> entry : constraints.entrySet()) {
               constraintString.add(String.format("%s = '%s'", entry.getKey(), entry.getValue()));
            }
            
            query = String.format("%s WHERE %s", query, StringUtils.join(constraintString, "&&"));
         }
         
         return query;
      }
   }
   
   private static volatile Map<PropertySchemaType, PropertySchema> tableSchemas
      = new HashMap<PropertySchemaType, PropertySchema>();
   
   public static QueryResponse query(final DAORequest<? extends DAOResponse> queryRequest, final Query query) {
      Map<String, String> requestParams = new HashMap<String, String>();
      requestParams.put("q", query.toString());
      
      setAuthorizationHeader();
      
      QueryResponse daoResponse = buildQueryResponse(
         query.getTable(),
         BaseDAO.getVeevaClient().query(getCurrentVaultEndpoint().getDnsName(), query.toString()).getBody());
      
      return daoResponse;
   }
   
   //Important: Schemas only exist within the context of a vault. Don't let this be the
   //first DAO call made as part of the app
   private static void retrieveSchema(PropertySchemaType table) {
      String dnsName = getCurrentVaultEndpoint().getDnsName();
      setAuthorizationHeader();
      
      String schemaJson = null;
      
      switch (table) {
         case WORKFLOWS:
            schemaJson = getVeevaClient().retrieveWorkflowSchema(dnsName).getBody();
            break;
         case DOCUMENTS:
            schemaJson = getVeevaClient().retrieveDocumentPropertySchema(dnsName).getBody();
            break;
         case USERS:
            schemaJson = getVeevaClient().retrieveUserSchema(dnsName).getBody();
            break;
      }
      
      if (schemaJson != null) {
         tableSchemas.put(table, GSON.fromJson(schemaJson, PropertySchema.class));
      }
   }
   
   public static Query makeMetadataQuery(PropertySchemaType table, Map<String, String> constraints) {
      if (!tableSchemas.containsKey(table)) {
         retrieveSchema(table);
      }
      
      List<String> columnNames = new ArrayList<String>();
      
      for (PropertyMetadata property : tableSchemas.get(table).getProperties()) {
         if (property.getQueryable() == null || property.getQueryable()) {
            columnNames.add(property.getName());
         }
      }
      
      Query query = new Query(columnNames, table, constraints);
      
      return query;
   }
   
   private static QueryResponse buildQueryResponse(PropertySchemaType table,
      String jsonResponse) {
      List<Map<String, PropertyMetadata>> resultTable = new ArrayList<Map<String, PropertyMetadata>>();
      List<JsonElement> columns = JsonParser.getElementList(jsonResponse, "columns");
      List<JsonElement> resultSet;
      if (JsonParser.getElement(jsonResponse) == null) {
         resultSet = new ArrayList<JsonElement>();
      }
      else {
         resultSet = JsonParser.getElementList(jsonResponse, "resultSet");
      }
      
      PropertySchema schema = tableSchemas.get(table);
      List<PropertyMetadata> schemaProperties = schema.getProperties();
      
      for (JsonElement element : resultSet) {
         Map<String, PropertyMetadata> row = new HashMap<String, PropertyMetadata>();
         List<JsonElement> values = getElementList(element, "values");
         for (int i = 0; i < schemaProperties.size() && i < values.size(); i++) {
            PropertyMetadata property = null;
            for (PropertyMetadata mp : schemaProperties) {
               if (mp.getName().equals(columns.get(i).getAsString())) {
                  property = new PropertyMetadata();
                  property.setName(mp.getName());
                  property.setType(mp.getType());
                  break;
               }
            }
            property.setValue(values.get(i));
            row.put(property.getName(), property);
         }
         
         resultTable.add(row);
      }
      
      QueryResponse daoResponse = new QueryResponse();
      daoResponse.setResults(resultTable);
      daoResponse.setResponseStatus(ResponseStatus.valueOf(getElement(jsonResponse, "responseStatus").getAsString()));
      daoResponse.setResponseMessage(getElement(jsonResponse, "responseMessage").getAsString());
      
      return daoResponse;
   }
   
   public static PropertySchema getSchema(PropertySchemaType schemaName) {
      if (!tableSchemas.containsKey(schemaName)) {
         retrieveSchema(schemaName);
      }
      
      return tableSchemas.get(schemaName);
   }
   
   public static void dumpAllSchemas() {
      tableSchemas = new HashMap<PropertySchemaType, PropertySchema>();
   }
}

package com.veevavault.mobile.dao;

import static com.veevavault.mobile.dao.JsonParser.getElement;
import static com.veevavault.mobile.dao.JsonParser.getElementList;

import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import lombok.Data;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.util.LruCache;
import android.util.Log;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.veevavault.mobile.activity.VaultApplication;
import com.veevavault.mobile.dao.api.PropertySchema;
import com.veevavault.mobile.dao.api.PropertySchema.PropertyMetadata;
import com.veevavault.mobile.dao.api.PropertySchemaType;
import com.veevavault.mobile.dao.request.RetrieveDocumentFileUrlRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentThumbnailRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.dao.response.ErrorType;
import com.veevavault.mobile.dao.response.LoadDownloadedDocumentsResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveDocumentFileUrlResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentThumbnailResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;
import com.veevavault.mobile.dao.response.SaveDownloadedDocumentsResponse;
import com.veevavault.mobile.database.DatabaseAccessor;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Property;

public class DocumentDAO extends BaseDAO {
   @Data
   private static class RetrieveDocumentsJsonObject {
      private String responseStatus;
      private int size;
      private ArrayList<DocumentEntry> documents;
   }
   
   @Data
   private static class DocumentEntry {
      private JsonObject document;
   }
   
   @Data
   private static class RetrieveDocumentJsonObject {
      private String responseStatus;
      private JsonObject document;
   }

   private static int documentCacheSize = 100000;
   private static LruCache<BigInteger, Document> documentCache;
   private static ArrayList<Document> downloadedDocuments;
   
   static {
      documentCache = new LruCache<BigInteger, Document>(documentCacheSize);
      downloadedDocuments = new ArrayList<Document>();
   }
   
   public static RetrieveDocumentsResponse retrieveDocuments(
         final RetrieveDocumentsRequest daoRequest) {      
      RetrieveDocumentsResponse daoResponse = new RetrieveDocumentsResponse();
      String named_filter = daoRequest.getNamed_filter();
      String search = daoRequest.getSearch();
      String scope = daoRequest.getScope();
      int start = daoRequest.getStart();
      int limit = daoRequest.getLimit();
      
      if (daoRequest.isOnDevice()) {
         daoResponse = retrieveDownloadedDocuments(daoRequest);
         return daoResponse;
      }
      
      setAuthorizationHeader();
      String jsonResponse = getVeevaClient()
         .retrieveDocuments(getCurrentVaultEndpoint().getDnsName(),
            limit, start, scope, search, named_filter).getBody();
      RetrieveDocumentsJsonObject jsonObject =
         GSON.fromJson(jsonResponse, RetrieveDocumentsJsonObject.class);

      daoResponse.setResponseStatus(ResponseStatus.valueOf(jsonObject.getResponseStatus()));
      daoResponse.setMaxSize(jsonObject.getSize());
      if (jsonObject.getDocuments() != null && jsonObject.getDocuments().size() > 0) {
         List<Document> documentList = new ArrayList<Document>();
         
         for (DocumentEntry entry : jsonObject.getDocuments()) {
            documentList.add(createDocument(entry.getDocument()));
         }
         
         daoResponse.setDocuments(documentList);
      }
      return daoResponse;
   }

   public static RetrieveDocumentResponse retrieveDocument(
      final RetrieveDocumentRequest daoRequest) {
      BigInteger documentId = daoRequest.getDocumentId();
      
      RetrieveDocumentResponse daoResponse = new RetrieveDocumentResponse();
      daoResponse.setDocument(documentCache.get(documentId));
      
      if (daoResponse.getDocument() == null) {
         setAuthorizationHeader();
         String jsonResponse = getVeevaClient().retrieveDocument(getCurrentVaultEndpoint().getDnsName(),
               documentId.toString()).getBody();
         RetrieveDocumentJsonObject jsonObject =
            GSON.fromJson(jsonResponse, RetrieveDocumentJsonObject.class);
         daoResponse.setResponseStatus(ResponseStatus.valueOf(jsonObject.getResponseStatus()));
         
         if (daoResponse.getResponseStatus() != ResponseStatus.SUCCESS) {
            JsonElement firstError = getElementList(jsonResponse, "errors").get(0);
            daoResponse.setResponseMessage(getElement(firstError, "message").getAsString());
            daoResponse.setErrorType(ErrorType.valueOf(getElement(firstError, "type").getAsString()));
         } else {
            daoResponse.setDocument(createDocument(jsonObject.getDocument()));
         }
      }
      else {
         daoResponse.setResponseStatus(ResponseStatus.SUCCESS);
      }
      
      return daoResponse;
   }

   public static RetrieveDocumentThumbnailResponse retrieveDocumentThumbnail(
         final RetrieveDocumentThumbnailRequest daoRequest) {      
      setAuthorizationHeader();
      Resource response = getVeevaClient().retrieveDocumentThumbnail(getCurrentVaultEndpoint().getDnsName(),
         daoRequest.getDocumentId().toString(),
         Integer.parseInt(daoRequest.getMajorVersion()),
         Integer.parseInt(daoRequest.getMinorVersion())).getBody();
      Bitmap bitmap = null;
      try {
         bitmap = BitmapFactory.decodeStream(response.getInputStream());
      } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
      }
      
      RetrieveDocumentThumbnailResponse daoResponse = new RetrieveDocumentThumbnailResponse();
      
      daoResponse.setDocumentId(daoRequest.getDocumentId());
      if (bitmap != null) {
         daoResponse.setResponseStatus(ResponseStatus.SUCCESS);
         daoResponse.setImage(bitmap);
      }      
      
      return daoResponse;
   }
   
   public static void saveDocumentToDownloads(Document document) {
      downloadedDocuments.add(document);
   }
   
   public static RetrieveDocumentFileUrlResponse retrieveDocumentFileUrl(
      final RetrieveDocumentFileUrlRequest daoRequest) {
      RetrieveDocumentFileUrlResponse daoResponse = new RetrieveDocumentFileUrlResponse();
      
      String documentFileUrl = String.format("%s/v7.0/objects/documents/%s/file",
         getCurrentVaultEndpoint().getDnsName(), daoRequest.getDocument().getId().toString());
      
      daoResponse.setDocumentFileUrl(documentFileUrl);
      daoResponse.setSessionId(getCurrentSessionId());
      
      return daoResponse;
   }

   protected static Document createDocument(JsonObject documentJson) {
      Document document = new Document(getCurrentVaultEndpoint().getVaultID());
      document.setProperties(new HashSet<Property>());
      PropertySchema documentPropertySchema = QueryDAO.getSchema(PropertySchemaType.DOCUMENTS);

      if (documentJson != null) {
         for (Map.Entry<String, JsonElement> entry : documentJson.entrySet()) {
            populateDocumentProperty(document, entry.getKey(), entry.getValue(), documentPropertySchema);
         }
      }
      

      if (document.getPropertyByName("binder__v") == null) {
         document.setBinder(false);
      }
      else {
         document.setBinder(Boolean.parseBoolean(document.getPropertyByName("binder__v").getValue()));
      }
      BigInteger documentId = new BigInteger(document.getPropertyByName("id").getValue());
      if (documentCache.get(documentId) == null) {
         document.setId(documentId);
         documentCache.put(documentId, document);
      }
      
      return documentCache.get(documentId);
   }
   
   private static RetrieveDocumentsResponse retrieveDownloadedDocuments(RetrieveDocumentsRequest request) {
         RetrieveDocumentsResponse daoResponse = new RetrieveDocumentsResponse();
         
         ArrayList<Document> documents = new ArrayList<Document>();
         
         for (Document d : downloadedDocuments) {
            if (request.getVaultId() == null || d.getVaultId().equals(request.getVaultId())) {
               documents.add(d);
            }
         }
         
         daoResponse.setResponseStatus(ResponseStatus.SUCCESS);
         daoResponse.setDocuments(documents);
         daoResponse.setMaxSize(documents.size());
         return daoResponse;
   }
   
   private static void populateDocumentProperty(Document document, String propertyName,
      JsonElement propertyJson, PropertySchema documentPropertySchema) {
      if (propertyJson.isJsonObject()) {
         for (Entry<String, JsonElement> member : propertyJson.getAsJsonObject().entrySet()) {
            String fullMemberName = StringUtils.join(Arrays.asList(propertyName, member.getKey()), ".");
            if (documentPropertySchema.getPropertyByName(fullMemberName) == null) {
               fullMemberName = propertyName;
            }
            populateDocumentProperty(document, fullMemberName, member.getValue(), documentPropertySchema);
         }
      } else {
         PropertyMetadata propertyMetadata = documentPropertySchema.getPropertyByName(propertyName);
         
         if (propertyMetadata != null) {
            propertyMetadata.setValue(propertyJson);
            
            Property property = createProperty(propertyMetadata);
            
            document.addProperty(property);
         }
      }
   }
   
   private static Property createProperty(PropertyMetadata propertyMetadata) {
      Property property = new Property();
      property.setName(propertyMetadata.getName());
      property.setLabel(propertyMetadata.getLabel());
      property.setSection(propertyMetadata.getSection());
      property.setSectionPosition(propertyMetadata.getSectionPosition());
      property.setHidden(propertyMetadata.getHidden());
      property.setValue(propertyMetadata.toString());
      
      if (StringUtils.isBlank(property.getValue()) &&
         !propertyMetadata.showWhenBlank()) {
         property.setHidden(true);
      }
      
      if (property.getName().equals("size__v")) {
         property.setValue(FileUtils.byteCountToDisplaySize(Long.parseLong(propertyMetadata.toString())));
      }
      
      return property;
   }

   public static SaveDownloadedDocumentsResponse saveDownloadedDocumentsToDb(Context context) {
      int hash = downloadedDocuments.hashCode();
      if (VaultApplication.getPreviousDownloadChacheHash() != hash) {
         DatabaseAccessor db = new DatabaseAccessor(context);
         
         for (Document d : downloadedDocuments) {
            db.saveDocumentToDb(d);
         }

         Log.d("saveDownloadedDocumentsToDb", "saved " + downloadedDocuments.size() + " to disk");
         VaultApplication.setPreviousDownloadChacheHash(hash);
      }
      
      return new SaveDownloadedDocumentsResponse();
   }

   public static LoadDownloadedDocumentsResponse loadDownloadedDocumentsFromDb(Context context) {
      DatabaseAccessor db = new DatabaseAccessor(context);
      
      downloadedDocuments = db.retrieveDownloadedDocuments();
      
      for (Document d : downloadedDocuments) {
         documentCache.put(d.getId(), d);
      }
      
      Log.d("loadDownloadedDocumentsToDb", "loaded " + downloadedDocuments.size() + " from disk");
      
      return new LoadDownloadedDocumentsResponse();
   }
   
   public static void dumpCache() {
      initCache();
   }
   
   private static void initCache() {
      documentCache = new LruCache<BigInteger, Document>(documentCacheSize);
   }
}

package com.veevavault.mobile.dao;

import static com.veevavault.mobile.dao.JsonParser.getElement;
import static com.veevavault.mobile.dao.JsonParser.getElementList;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonElement;
import com.veevavault.mobile.model.Product;

public class ProductDAO extends BaseDAO {   
   private static Map<String, Product> productsById;
   
   static {
      initCache();
   }
   
   public static Product retrieveProduct(String id) {
      if (!productsById.containsKey(id)) {
         String jsonResponse = getVeevaClient().retrieveVaultProducts(getCurrentVaultEndpoint().getDnsName()).getBody();
         
         for (JsonElement catalogValueEntry : getElementList(jsonResponse, "catalogValues")) {            
            Product product = new Product();
            product.setName(getElement(catalogValueEntry, "catalogValue", "product_name__v").getAsString());
            product.setId(getElement(catalogValueEntry, "catalogValue", "id").getAsString());
            
            productsById.put(product.getId(), product);
         }
      }
      
      return productsById.get(id);
   }
   
   public static void dumpCache() {
      initCache();
   }
   
   private static void initCache() {
      productsById = new HashMap<String, Product>();
   }
}

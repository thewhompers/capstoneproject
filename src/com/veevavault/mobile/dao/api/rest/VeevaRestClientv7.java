package com.veevavault.mobile.dao.api.rest;

import org.androidannotations.annotations.rest.Get;
import org.androidannotations.annotations.rest.Post;
import org.androidannotations.annotations.rest.RequiresHeader;
import org.androidannotations.annotations.rest.Rest;
import org.springframework.core.io.Resource;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.util.LinkedMultiValueMap;

@Rest(converters = { FormHttpMessageConverter.class, ResourceHttpMessageConverter.class, StringHttpMessageConverter.class })
public interface VeevaRestClientv7 {
   @Post("https://login.veevavault.com/auth/api")
   ResponseEntity<String> authenticateUser(LinkedMultiValueMap<String, String> request);

   @Post("{domain}/v7.0/auth/")
   ResponseEntity<String> authenticateUser(String domain, LinkedMultiValueMap<String, String> request);
   
   @Get("{domain}/v7.0/objects/documents/{documentId}")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveDocument(String domain, String documentId);
   
   @Get("{domain}/v7.0/objects/documents?"
         + "limit={limit}"
         + "&start={start}"
         + "&scope={scope}"
         + "&search={search}"
         + "&named_filter={named_filter}")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveDocuments(String domain, int limit, int start,
      String scope, String search, String named_filter);
   
   @Get("{domain}/v7.0/objects/documents/{documentId}/versions/{major}/{minor}/thumbnail")
   @RequiresHeader("Authorization")
   ResponseEntity<Resource> retrieveDocumentThumbnail(String domain, String documentId, int major, int minor);
   
   @Get("{domain}/v7.0/objects/binders/{binderId}")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveBinder(String domain, String binderId);
   
   @Get("{domain}/v7.0/objects/binders/{binderId}/sections/{sectionId}")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveSection(String domain, String binderId, String sectionId);
   
   @Get("{domain}/v7.0/objects/groups")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveGroups(String domain);
   
   @Get("{domain}/v7.0/objects/catalogs/product__v")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveVaultProducts(String domain);
   
   @Get("{domain}/v7.0/metadata/objects/catalogs/product__v")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveProductSchema(String domain);
   
   @Get("{domain}/v7.0/metadata/objects/workflows")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveWorkflowSchema(String domain);
   
   @Get("{domain}/v7.0/metadata/objects/documents/properties")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveDocumentPropertySchema(String domain);
   
   @Get("{domain}/v7.0/metadata/objects/users")
   @RequiresHeader("Authorization")
   ResponseEntity<String> retrieveUserSchema(String domain);
   
   @Get("{domain}/v7.0/query?q={query}")
   @RequiresHeader("Authorization")
   ResponseEntity<String> query(String domain, String query);
   
   void setHeader(String name, String value);
   String getHeader(String name);
}

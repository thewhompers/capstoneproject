package com.veevavault.mobile.dao.api;

public enum PropertySchemaType {
   DOCUMENTS("documents"),
   USERS("users"),
   WORKFLOWS("workflows");
   
   private String tableName;
   
   private PropertySchemaType(String tableName) {
      this.tableName = tableName;
   }
   
   public String toString() {
      return tableName;
   }
}

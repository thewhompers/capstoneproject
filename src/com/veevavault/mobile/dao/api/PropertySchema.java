package com.veevavault.mobile.dao.api;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.GroupDAO;
import com.veevavault.mobile.dao.ProductDAO;
import com.veevavault.mobile.dao.UserDAO;

@Data
public class PropertySchema {
   private static final Set<String> OBJECT_TYPE_HIDE_IF_BLANK = new HashSet<String>() {
      {
         add("User");
         add("Group");
      }
   };
   
   private enum MetadataType {
      id,
      Number,
      Autonumber,
      Boolean,
      String,
      Picklist,
      Date,
      DateTime,
      ObjectReference;
   }
   
   @Data
   public static class PropertyMetadata {
      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private String name;

      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private String label;
      
      @Getter(AccessLevel.PUBLIC)
      private MetadataType type;

      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private String objectType;
      
      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private Boolean queryable;
      
      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private Boolean editable;
      
      @Setter(AccessLevel.PUBLIC)
      private String section;
      
      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private int sectionPosition;
      
      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private JsonElement value;
      
      @Getter(AccessLevel.PUBLIC)
      @Setter(AccessLevel.PUBLIC)
      private Boolean hidden;
      
      public String toString() {
         if (value == null || value.isJsonNull()) {
            return "";
         }
         
         switch (type) {
            case id:
               return Integer.toString(value.getAsInt());
            case Number:
               return Integer.toString(value.getAsInt());
            case Autonumber:
               return value.getAsString();
            case Boolean:
               return Boolean.toString(value.getAsBoolean());
            case String:
               return value.getAsString();
            case Picklist:
               return value.getAsString();
            case Date:
               return DateTimeFormat.shortDate()
                  .withZone(BaseDAO.getCurrentUser().getTimezone())
                  .print(DateTime.parse(value.getAsString()));
            case DateTime:
               return DateTimeFormat.shortDateTime()
                  .withZone(BaseDAO.getCurrentUser().getTimezone())
                  .print(DateTime.parse(value.getAsString()));
            case ObjectReference:
               if (value.isJsonNull() || value.isJsonObject()) {
                  return "";
               }
               else if (value.isJsonObject()) {
                  return value.getAsString();
               }
               else if (value.isJsonArray()) {
                  ArrayList<String> memberVals = new ArrayList<String>();
                  
                  for (JsonElement element : value.getAsJsonArray()) {
                     memberVals.add(computeValue(element));
                  }
                  
                  return StringUtils.join(memberVals, ", ");
               }
               else if (value.isJsonPrimitive()) {
                  return computeValue(value);
               }
               if (value.isJsonArray())
               {
            	   JsonArray jArr = value.getAsJsonArray();
            	   if (jArr.size() == 0)
            	   {
            		   return "";
            	   }
               }
               return value.getAsString();
            default:
               return value.getAsString();
         }
      }
      
      private String computeValue(JsonElement value) {
         if (StringUtils.isBlank(objectType)) {
            return value.getAsString();
         } else if (objectType.equals("User")) {
            return UserDAO.retrieveUserById(value.getAsString()).getFullName();
         } else if (objectType.equals("Group")) {
            return GroupDAO.retrieveGroupById(value.getAsString()).getName();
         } else if (objectType.equals("Product")) {
            return ProductDAO.retrieveProduct(value.getAsString()).getName();
         }
         
         return value.getAsString();
      }
      
      public boolean showWhenBlank() {
         return !OBJECT_TYPE_HIDE_IF_BLANK.contains(objectType);
      }
      
      public String getSection() {
         String[] splitStr = StringUtils.splitByCharacterTypeCamelCase(section);
         String rtn = new String();
         
         rtn = StringUtils.join(splitStr, " ");
         
         return StringUtils.capitalize(rtn);
      }
      
      public PropertyMetadata() {
      }
   }

   @Getter(AccessLevel.PUBLIC)
   @Setter(AccessLevel.PUBLIC)
   private List<PropertyMetadata> properties;
   
   public PropertyMetadata getPropertyByName(String name) {
      for (PropertyMetadata propertyMetadata : properties) {
         if (propertyMetadata.getName().equals(name)) {
            return propertyMetadata;
         }
      }
      
      return null;
   }
}

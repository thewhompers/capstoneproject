package com.veevavault.mobile.dao.api;

public enum NamedFilter {
   ALL_DOCUMENTS(""),
   RECENT_DOCUMENTS("Recent Documents"),
   MY_DOCUMENTS("My Documents"),
   FAVORITES("Favorites"),
   SEARCH("");
   
   private String namedFilter;
   
   private NamedFilter(String namedFilter) {
      this.namedFilter = namedFilter;
   }
   
   public String toString() {
      return namedFilter;
   }
}

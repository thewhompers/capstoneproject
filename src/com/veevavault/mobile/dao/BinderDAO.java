package com.veevavault.mobile.dao;

import static com.veevavault.mobile.dao.JsonParser.getElement;
import static com.veevavault.mobile.dao.JsonParser.getElementList;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonElement;
import com.veevavault.mobile.activity.VaultApplication;
import com.veevavault.mobile.dao.request.RetrieveBinderRequest;
import com.veevavault.mobile.dao.response.ErrorType;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveBinderResponse;
import com.veevavault.mobile.model.BindableIdentifier;
import com.veevavault.mobile.model.BindableType;

public class BinderDAO extends BaseDAO {   
   public static RetrieveBinderResponse retrieveBinder(RetrieveBinderRequest request) {     
      String jsonResponse = getVeevaClient().retrieveBinder(
         getCurrentVaultEndpoint().getDnsName(),
         request.getBinderId().toString()).getBody();
      
      return buildRetrieveBinderResponse(jsonResponse);
   }
   
   private static RetrieveBinderResponse buildRetrieveBinderResponse(String jsonResponse) {
      RetrieveBinderResponse response = new RetrieveBinderResponse();
      response.setResponseStatus(ResponseStatus.valueOf(getElement(jsonResponse, "responseStatus").getAsString()));
      
      if (response.getResponseStatus() != ResponseStatus.SUCCESS) {
         JsonElement firstError = getElementList(jsonResponse, "errors").get(0);
         response.setResponseMessage(getElement(firstError, "message").getAsString());
         response.setErrorType(ErrorType.valueOf(getElement(firstError, "type").getAsString()));
      } else {
         response.setBinderDocument(DocumentDAO.createDocument(getElement(jsonResponse, "document").getAsJsonObject()));
         List<BindableIdentifier> binderContents = new ArrayList<BindableIdentifier>();
         
         for (JsonElement node : getElementList(jsonResponse, "binder", "nodes")) {
            BindableIdentifier binderContent = new BindableIdentifier();
            binderContent.setType(
               BindableType.valueOf(
                  StringUtils.upperCase(
                     getElement(node, "properties", "type__v").getAsString())));
            
            switch (binderContent.getType()) {
            case DOCUMENT:
            case BINDER:
               binderContent.setId(getElement(node, "properties", "document_id__v").getAsBigInteger());
               break;
            case SECTION:
               binderContent.setId(getElement(node, "properties", "id").getAsBigInteger());
               break;
            }
            binderContents.add(binderContent);
         }
         
         response.setBinderContents(binderContents);
      }
      
      return response;
   }
}

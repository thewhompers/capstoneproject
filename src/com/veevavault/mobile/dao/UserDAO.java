package com.veevavault.mobile.dao;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTimeZone;

import com.veevavault.mobile.dao.QueryDAO.Query;
import com.veevavault.mobile.dao.api.PropertySchema.PropertyMetadata;
import com.veevavault.mobile.dao.api.PropertySchemaType;
import com.veevavault.mobile.dao.response.QueryResponse;
import com.veevavault.mobile.model.User;

public class UserDAO {
   private static Map<String, User> usersById;
 
   static {
      initCache();
   }
   
   public static User retrieveUserById(String id) {
      if (!usersById.containsKey(id)) {
         Map<String, String> constraints = new HashMap<String, String>();
         constraints.put("id", id);
         
         Query userQuery = QueryDAO.makeMetadataQuery(PropertySchemaType.USERS, constraints);
         
         QueryResponse userQueryResponse = QueryDAO.query(null, userQuery);
         
         for (Map<String, PropertyMetadata> row : userQueryResponse.getResults()) {
            User retrievedUser = new User();
            retrievedUser.setId(row.get("id").toString());
            retrievedUser.setName(row.get("user_name__v").toString());
            retrievedUser.setFirstName(row.get("user_first_name__v").toString());
            retrievedUser.setLastName(row.get("user_last_name__v").toString());
            
            usersById.put(retrievedUser.getId(), retrievedUser);
         }
      }
      
      return usersById.get(id);
   }
   
   public static User retrieveUserByName(String name) {
      for (User cachedUser : usersById.values()) {
         if (name.equals(cachedUser.getName())) {
            return cachedUser;
         }
      }
      
      Map<String, String> constraints = new HashMap<String, String>();
      constraints.put("user_name__v", name);
      
      Query userQuery = QueryDAO.makeMetadataQuery(PropertySchemaType.USERS, constraints);
      
      QueryResponse userQueryResponse = QueryDAO.query(null, userQuery);
      
      for (Map<String, PropertyMetadata> row : userQueryResponse.getResults()) {
         User retrievedUser = new User();
         retrievedUser.setId(row.get("id").toString());
         retrievedUser.setName(row.get("user_name__v").toString());
         retrievedUser.setFirstName(row.get("user_first_name__v").toString());
         retrievedUser.setLastName(row.get("user_last_name__v").toString());
         retrievedUser.setTimezone(DateTimeZone.forID(row.get("user_timezone__v").toString()));
         
         usersById.put(retrievedUser.getId(), retrievedUser);
         
         return retrievedUser;
      }
      
      return null;
   }
   
   public static void dumpCache() {
      initCache();
   }
   
   private static void initCache() {
      usersById = new HashMap<String, User>();
      User anon = new User();
      anon.setId("1");
      anon.setName("user@domain.suffix");
      usersById.put("1", anon);
   }
}

package com.veevavault.mobile.dao;

import static com.veevavault.mobile.dao.JsonParser.getElement;
import static com.veevavault.mobile.dao.JsonParser.getElementList;

import java.util.HashMap;
import java.util.Map;

import com.google.gson.JsonElement;
import com.veevavault.mobile.model.Group;

public class GroupDAO extends BaseDAO {
   private static Map<String, Group> groupsById;
   
   static {
      initCache();
   }

   public static Group retrieveGroupById(String id) {
      if (groupsById.get(id) == null) {
         String jsonResponse = getVeevaClient().retrieveGroups(
            getCurrentVaultEndpoint().getDnsName()).getBody();
         
         for (JsonElement groupsJson : getElementList(jsonResponse, "groups")) {
            Group group = new Group();
            group.setId(getElement(groupsJson, "group", "id").getAsString());
            group.setName(getElement(groupsJson, "group", "group_name__v").getAsString());
            groupsById.put(group.getId(), group);
         }
      }
      
      return groupsById.get(id);
   }
   
   public static void dumpCache() {
      initCache();
   }
   
   private static void initCache() {
      groupsById = new HashMap<String, Group>();
   }
}

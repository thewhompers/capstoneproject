package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.util.Log;

import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.response.RetrieveDocumentFileUrlResponse;
import com.veevavault.mobile.model.Document;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"callback"})
public class RetrieveDocumentFileUrlRequest extends DAORequest<RetrieveDocumentFileUrlResponse> {
   private Document document;
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveDocumentFileUrlResponse(RetrieveDocumentFileUrlResponse response);
   }

   @Override
   protected RetrieveDocumentFileUrlResponse doInBackground(Void... arg0) {
      Log.d("RetrieveDocumentFileUrlRequest", "Attempting to retrieve document file URL");
      RetrieveDocumentFileUrlResponse response = 
         (RetrieveDocumentFileUrlResponse) recall(this);
      
      if (response == null) {
         response = DocumentDAO.retrieveDocumentFileUrl(this);
         cache(this, response);
      }
      
      return response;
   }

   @Override
   protected void onPostExecute(RetrieveDocumentFileUrlResponse response) {
      response.setDocument(document);
      callback.onRetrieveDocumentFileUrlResponse(response);
   }
}

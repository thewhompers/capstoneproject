package com.veevavault.mobile.dao.request;

import java.math.BigInteger;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.content.Context;
import android.graphics.BitmapFactory;

import com.veevavault.mobile.R;
import com.veevavault.mobile.activity.VaultApplication;
import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveDocumentThumbnailResponse;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"callback"})
public class RetrieveDocumentThumbnailRequest extends DAOImageRequest<RetrieveDocumentThumbnailResponse> {
   private BigInteger documentId;
   private String majorVersion;
   private String minorVersion;
   private Listener callback;
   
   public interface Listener {
      public void onThumbnailResponse(RetrieveDocumentThumbnailResponse response);
   }
   
   @Override
   protected RetrieveDocumentThumbnailResponse doInBackground(Void... params) {
      
      RetrieveDocumentThumbnailResponse response = 
         (RetrieveDocumentThumbnailResponse) recall(this);
      
      if (VaultApplication.isOffline()) {
         response = new RetrieveDocumentThumbnailResponse();
         response.setDocumentId(documentId);
         response.setImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.doc_icon_doc));
         response.setResponseStatus(ResponseStatus.SUCCESS);
         return response;
      }
      
      if (response == null) {
         response = DocumentDAO.retrieveDocumentThumbnail(this);
         cache(this, response);
      }
      
      return response;
   }
   
   @Override
   protected void onPostExecute(RetrieveDocumentThumbnailResponse response) {
      callback.onThumbnailResponse(response);
   }
}

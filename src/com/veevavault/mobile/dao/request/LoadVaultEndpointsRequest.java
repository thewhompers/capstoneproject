package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.content.Context;

import com.veevavault.mobile.dao.response.LoadUserResponse;
import com.veevavault.mobile.dao.response.LoadVaultEndpointsResponse;
import com.veevavault.mobile.database.DatabaseAccessor;
import com.veevavault.mobile.model.User;

@Data
@EqualsAndHashCode(callSuper=false)
public class LoadVaultEndpointsRequest extends DAORequest<LoadVaultEndpointsResponse> {
   private Context context;
   private User user;
   private Listener callback;
   
   public interface Listener {
      public void onLoadEnpointsResponse(LoadVaultEndpointsResponse response);
   }
   
   @Override
   public void onPreExecute() {
      
   }
   
   @Override
   protected LoadVaultEndpointsResponse doInBackground(Void... params) {
      LoadVaultEndpointsResponse response = new LoadVaultEndpointsResponse();
      
      DatabaseAccessor db = new DatabaseAccessor(context);
      
      response.setEndpoints(db.loadVaultEndpoints(user));

      return response;
   }
   
   @Override
   protected void onPostExecute(LoadVaultEndpointsResponse response) {
      callback.onLoadEnpointsResponse(response);
   }
}

package com.veevavault.mobile.dao.request;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.util.Log;

import com.veevavault.mobile.dao.QueryDAO;
import com.veevavault.mobile.dao.QueryDAO.Query;
import com.veevavault.mobile.dao.api.PropertySchema.PropertyMetadata;
import com.veevavault.mobile.dao.api.PropertySchemaType;
import com.veevavault.mobile.dao.response.QueryResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveDocumentPropertiesResponse;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Property;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"callback"})
public class RetrieveDocumentPropertiesRequest extends DAORequest<RetrieveDocumentPropertiesResponse> {
   private Document document;
   private Listener callback;
   
   public interface Listener {
      public void onDocumentPropertiesResponse(RetrieveDocumentPropertiesResponse response);
   }
   
   public RetrieveDocumentPropertiesRequest(Document document) {
      this.document = document;
   }

   @Override
   protected RetrieveDocumentPropertiesResponse doInBackground(Void... params) {
      RetrieveDocumentPropertiesResponse response = 
         (RetrieveDocumentPropertiesResponse) recall(this);
      
      if (response == null) {
         response = new RetrieveDocumentPropertiesResponse();
         
         Map<String, String> constraints = new HashMap<String, String>();
         
         constraints.put("id", document.getId().toString());
         

         Log.d("RetrieveDocumentPropertiesRequest", "Document: " + document.getId() + " - making metadata query");
         Query query = QueryDAO.makeMetadataQuery(PropertySchemaType.DOCUMENTS, constraints);
         Log.d("RetrieveDocumentPropertiesRequest", "Document: " + document.getId() + " - making query");
         QueryResponse queryResponse = QueryDAO.query(this, query);
         
         ArrayList<Property> properties = new ArrayList<Property>();
         
         for (Map<String, PropertyMetadata> documentRow : queryResponse.getResults()) {
            if (!BigInteger.valueOf(documentRow.get("id").getValue().getAsInt()).equals(document.getId())) {
               continue;
            }
            
            for (PropertyMetadata propertyMetadata : documentRow.values()) {
               Property property = new Property();
               property.setName(propertyMetadata.getName());
               property.setLabel(propertyMetadata.getLabel());
               property.setSection(propertyMetadata.getSection());
               property.setSectionPosition(propertyMetadata.getSectionPosition());
               property.setValue(propertyMetadata.toString());
               
               properties.add(property);
               Log.d("Retrieving Properties", "property: " + property.getName() + " section: " + property.getSection());
            }
         }
         
         response.setResponseStatus(ResponseStatus.SUCCESS);
         response.setProperties(properties);
         
         cache(this, response);
      }
      
      return response;
   }

   @Override
   public void onPostExecute(RetrieveDocumentPropertiesResponse response) {
      callback.onDocumentPropertiesResponse(response);
   }
}

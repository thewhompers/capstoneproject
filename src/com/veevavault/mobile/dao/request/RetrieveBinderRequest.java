package com.veevavault.mobile.dao.request;

import java.math.BigInteger;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.dao.BinderDAO;
import com.veevavault.mobile.dao.response.RetrieveBinderResponse;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveBinderRequest extends DAORequest<RetrieveBinderResponse> {
   private BigInteger binderId;
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveBinderResponse(RetrieveBinderResponse response);
   }
   
   @Override
   protected RetrieveBinderResponse doInBackground(Void... params) {
      RetrieveBinderResponse response = (RetrieveBinderResponse) recall(this);
      
      if (response == null) {
         response = BinderDAO.retrieveBinder(this);
         cache(this, response);
      }
      
      return response;
   }
   
   @Override 
   protected void onPostExecute(RetrieveBinderResponse response) {
      callback.onRetrieveBinderResponse(response);
   }
}

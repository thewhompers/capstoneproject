package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.content.Context;

import com.veevavault.mobile.activity.VaultApplication;
import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.request.DAORequest;
import com.veevavault.mobile.database.DatabaseAccessor;

@Data
@EqualsAndHashCode(callSuper=false)
public class SaveOfflineItemsToDatabaseRequest extends DAORequest<Void> {
   private Context context;
   
   @Override
   public void onPreExecute() {
      
   }
   
   @Override
   protected Void doInBackground(Void... params) {
      DatabaseAccessor db = new DatabaseAccessor(context);
      
      int userHash = BaseDAO.getCurrentUser().hashCode();
      if (VaultApplication.getPreviousUserHash() != userHash) {
         db.saveCurrentUser(BaseDAO.getCurrentUser());
         VaultApplication.setPreviousUserHash(userHash);
      }

      int endpointsHash = BaseDAO.getAvailableVaultEndpoints().hashCode();
      if (VaultApplication.getPreviousEndpointsHash() != endpointsHash) {
         db.saveVaultEndpoints(BaseDAO.getCurrentUser(), BaseDAO.getAvailableVaultEndpoints());
         VaultApplication.setPreviousEndpointsHash(endpointsHash);
      }  

      DocumentDAO.saveDownloadedDocumentsToDb(context); // hash handled in dao

      return null;
   }
}

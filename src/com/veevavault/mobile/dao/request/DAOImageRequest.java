package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class DAOImageRequest<T> extends DAORequest<T> {
   private int maxWidth;
   private int maxHeight;
}

package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.content.Context;

import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.request.DAORequest;
import com.veevavault.mobile.database.DatabaseAccessor;

@Data
@EqualsAndHashCode(callSuper=false)
public class LoadDownloadedDocumentsRequest extends DAORequest<Void> {
   private Context context;
   private Listener callback;
   
   public interface Listener {
      public void onLoadDocumentsResponse();
   }
   
   @Override
   public void onPreExecute() {
      
   }
   
   @Override
   protected Void doInBackground(Void... params) {
      DatabaseAccessor db = new DatabaseAccessor(context);

      //BaseDAO.setAvailableVaultEndpoints(db.loadVaultEndpoints());
      DocumentDAO.loadDownloadedDocumentsFromDb(context);

      return null;
   }
   
   @Override
   public void onPostExecute(Void response) {
      callback.onLoadDocumentsResponse();
   }
}

package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.util.Log;

import com.veevavault.mobile.activity.VaultApplication;
import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"callback"})
public class RetrieveDocumentsRequest extends DAORequest<RetrieveDocumentsResponse> {
   private String named_filter;
   private String search;
   private String scope;
   private int start;
   private int limit;
   private Listener callback;
   private boolean onDevice;
   private String vaultId;
   private boolean noCache;
   
   public interface Listener {
      public void onRetrieveDocumentsResponse(RetrieveDocumentsResponse response);
   }

   @Override
   protected RetrieveDocumentsResponse doInBackground(Void... params) {
      if (!VaultApplication.isOffline())
         vaultId = BaseDAO.getCurrentVaultEndpoint().getVaultID();
      
      RetrieveDocumentsResponse response =
         (RetrieveDocumentsResponse) recall(this);
      
      if (response == null) {
         response = DocumentDAO.retrieveDocuments(this);
         if (!noCache)
            cache(this, response);
         Log.d("", "Retrieved documents from network");
      } else {
         Log.d("", "Retrieved documents from cache");
      }
      
      return response;
   }
   
   @Override 
   protected void onPostExecute(RetrieveDocumentsResponse response) {
      callback.onRetrieveDocumentsResponse(response);
   }
}

package com.veevavault.mobile.dao.request;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.apache.commons.lang3.StringUtils;

import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.QueryDAO;
import com.veevavault.mobile.dao.QueryDAO.Query;
import com.veevavault.mobile.dao.api.PropertySchema.PropertyMetadata;
import com.veevavault.mobile.dao.api.PropertySchemaType;
import com.veevavault.mobile.dao.response.QueryResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveTasksResponse;
import com.veevavault.mobile.model.Task;
import com.veevavault.mobile.model.Task.TaskStatus;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"callback"})
public class RetrieveTasksRequest extends DAORequest<RetrieveTasksResponse> {
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveTasksResponse(RetrieveTasksResponse response);
   }
   
   @Override
   protected RetrieveTasksResponse doInBackground(Void... params) {
      RetrieveTasksResponse response = (RetrieveTasksResponse) recall(this);
      
      if (response == null) {
         response = new RetrieveTasksResponse();
         Query query = QueryDAO.makeMetadataQuery(PropertySchemaType.WORKFLOWS, null);
         QueryResponse queryResponse = QueryDAO.query(this, query);
         
         List<Task> tasks = new ArrayList<Task>();
         
         for (Map<String, PropertyMetadata> responseRow : queryResponse.getResults()) {
               Task task = new Task();
               task.setStatus(TaskStatus.valueOf(StringUtils.upperCase(responseRow.get("task_status__v").toString())));
               task.setTaskName(responseRow.get("task_name__v").toString());
               task.setTaskComment(responseRow.get("task_comment__v").toString());
               task.setDateDue(responseRow.get("task_dueDate__v").toString());
               task.setDateAssigned(responseRow.get("task_assignmentDate__v").toString());
               task.setDocId(new BigInteger(responseRow.get("workflow_document_id__v").toString()));
               task.setWorkflowInitiatorName(responseRow.get("workflow_initiator_name__v").toString());
               task.setAssigneeId(responseRow.get("task_assignee__v").toString());
               
               if (task.getAssigneeId().equals(BaseDAO.getCurrentUser().getId()) &&
                     (task.getStatus() == TaskStatus.ASSIGNED ||
                     task.getStatus() == TaskStatus.AVAILABLE)) {
                  tasks.add(task);
               }

         }
         
         response.setResponseStatus(ResponseStatus.SUCCESS);
         response.setTasks(tasks);
         
         cache(this, response);
      }
      
      return response;
   }
   
   @Override
   protected void onPostExecute(RetrieveTasksResponse response) {
      callback.onRetrieveTasksResponse(response);
   }
}

package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.response.RetrieveVaultsResponse;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveVaultsRequest extends DAORequest<RetrieveVaultsResponse> {
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveVaultsResponse(RetrieveVaultsResponse response);
   }

   @Override
   protected RetrieveVaultsResponse doInBackground(Void... params) {
      RetrieveVaultsResponse response = new RetrieveVaultsResponse();

      response.setUsername(BaseDAO.getCurrentUser().getName());
      response.setVaults(BaseDAO.getAvailableVaultEndpoints());

      return response;
   }
   
   @Override
   protected void onPostExecute(RetrieveVaultsResponse response) {
      callback.onRetrieveVaultsResponse(response);
   }
}

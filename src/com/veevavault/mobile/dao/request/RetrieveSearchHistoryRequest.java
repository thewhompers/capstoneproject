package com.veevavault.mobile.dao.request;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.apache.commons.lang3.StringUtils;

import com.veevavault.mobile.dao.response.DAOResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;
import com.veevavault.mobile.dao.response.RetrieveSearchHistoryResponse;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"callback"})
public class RetrieveSearchHistoryRequest extends DAORequest<RetrieveSearchHistoryResponse> {
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveSearchHistory(RetrieveSearchHistoryResponse response);
   }

   @Override
   protected RetrieveSearchHistoryResponse doInBackground(Void... params) {
      RetrieveSearchHistoryResponse response = new RetrieveSearchHistoryResponse();
      Map<RetrieveDocumentsRequest, RetrieveDocumentsResponse> searchHistory =
         new LinkedHashMap<RetrieveDocumentsRequest, RetrieveDocumentsResponse>();
      
      for (Entry<DAORequest<? extends DAOResponse>, DAOResponse> entry : getCacheAsMap().entrySet()) {
         if (entry.getKey() instanceof RetrieveDocumentsRequest) {
            RetrieveDocumentsRequest searchRequest =
               (RetrieveDocumentsRequest) entry.getKey();
            if (StringUtils.isNotBlank(searchRequest.getSearch()) && searchRequest.getStart() == 0) {
               searchHistory.put(searchRequest, (RetrieveDocumentsResponse) entry.getValue());
            }
         }
      }
      
      response.setSearchHistory(searchHistory);
      
      return response;
   }
   
   @Override 
   protected void onPostExecute(RetrieveSearchHistoryResponse response) {
      callback.onRetrieveSearchHistory(response);
   }
}

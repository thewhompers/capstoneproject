package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.util.Log;

import com.veevavault.mobile.dao.AuthenticationDAO;
import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.GroupDAO;
import com.veevavault.mobile.dao.ProductDAO;
import com.veevavault.mobile.dao.QueryDAO;
import com.veevavault.mobile.dao.UserDAO;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.model.VaultEndpoint;

@Data
@EqualsAndHashCode(callSuper=false)
public class AuthenticateUserRequest extends DAORequest<AuthenticateUserResponse> {
   private VaultEndpoint domain;
   private String username;
   private String password;
   private Listener callback;
   
   public interface Listener {
      public void onAuthenticateResponse(AuthenticateUserResponse response);
   }
   
   @Override
   protected AuthenticateUserResponse doInBackground(Void... params) {
      Log.d("AuthenticateUserRequest", "Attempting to authenticate user");
      DAORequest.clearRequestCache();
      DocumentDAO.dumpCache();
      GroupDAO.dumpCache();
      QueryDAO.dumpAllSchemas();
      ProductDAO.dumpCache();
      UserDAO.dumpCache();
      return AuthenticationDAO.authenticateUser(this);
   }
   
   @Override
   public void onPostExecute(AuthenticateUserResponse response) {
      callback.onAuthenticateResponse(response);
   }
}

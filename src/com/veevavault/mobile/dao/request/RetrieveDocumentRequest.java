package com.veevavault.mobile.dao.request;

import java.math.BigInteger;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.response.RetrieveDocumentResponse;

@Data
@EqualsAndHashCode(callSuper=false, exclude={"callback"})
public class RetrieveDocumentRequest extends DAORequest<RetrieveDocumentResponse> {
   private BigInteger documentId;
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveDocumentResponse(RetrieveDocumentResponse response);
   }

   @Override
   protected RetrieveDocumentResponse doInBackground(Void... arg0) {
      RetrieveDocumentResponse response =
         (RetrieveDocumentResponse) recall(this);
      
      if (response == null) {
         response = DocumentDAO.retrieveDocument(this);
         cache(this, response);
      }
      
      return response;
   }
   
   @Override 
   protected void onPostExecute(RetrieveDocumentResponse response) {
      callback.onRetrieveDocumentResponse(response);
   }
}

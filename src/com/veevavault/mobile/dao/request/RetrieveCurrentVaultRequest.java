package com.veevavault.mobile.dao.request;

import android.util.Log;

import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.model.VaultEndpoint;

public class RetrieveCurrentVaultRequest extends DAORequest<VaultEndpoint> {
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveCurrentVaultResponse(VaultEndpoint response);
   }

   @Override
   protected VaultEndpoint doInBackground(Void... params) {
      Log.d("RetrieveCurrentVaultRequest", "Attempting to get current vault endpoint");
      return BaseDAO.getCurrentVaultEndpoint();
   }
   
   @Override
   protected void onPostExecute(VaultEndpoint response) {
      callback.onRetrieveCurrentVaultResponse(response);
   }
}

package com.veevavault.mobile.dao.request;

import java.util.Map;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.support.v4.util.LruCache;
import android.widget.Toast;

import com.veevavault.mobile.activity.VaultApplication;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.response.DAOResponse;

@Data
@EqualsAndHashCode(callSuper=false)
public abstract class DAORequest<T> extends AsyncTask<Void, Void, T> {
   private static int requestCacheSize = 100000;
   private static LruCache<DAORequest<? extends DAOResponse>, DAOResponse> requestCache;
   protected Context context;
   private boolean offline;
   private transient VeevaRestClientv7 restClient;
   
   @Override
   protected void onPreExecute() {
      

	  super.onPreExecute();
	  if (!VaultApplication.isOffline()) {
	     //ADD setContext to all request calls!
	     ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
	     NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
	     NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);
	     if (mobile != null && wifi != null)
	     {
	        if (!mobile.isConnected() && !wifi.isConnected()) {
	           Toast.makeText(context, "No internet found.", Toast.LENGTH_LONG).show();
	           this.cancel(true);
	        }
	     }
	  }
   }
   
   protected static void cache(DAORequest<? extends DAOResponse> request, DAOResponse response) {
      if (requestCache == null) {
         requestCache = new LruCache<DAORequest<? extends DAOResponse>, DAOResponse>(requestCacheSize);
      }
      
      requestCache.put(request, response);
   }
   
   protected static DAOResponse recall(DAORequest<? extends DAOResponse> request) {
      if (requestCache == null) {
         return null;
      }
      
      return requestCache.get(request);
   }
   
   protected static Map<DAORequest<? extends DAOResponse>, DAOResponse> getCacheAsMap() {
      if (requestCache == null) {
         return null;
      }
      
      return requestCache.snapshot();
   }
   
   public static void clearRetrieveDocumentCache() {
      if (requestCache != null) {
         for (DAORequest<? extends DAOResponse> request : getCacheAsMap().keySet()) {
            if (request instanceof RetrieveDocumentsRequest ||
                  request instanceof RetrieveDocumentThumbnailRequest ||
                  request instanceof RetrieveDocumentRequest ||
                  request instanceof RetrieveDocumentPropertiesRequest) {
               requestCache.remove(request);
            }
         }
      }
   }
   
   public static void clearRequestCache() {
	   if (requestCache != null) {
		   requestCache.evictAll();
	   }
   }
}

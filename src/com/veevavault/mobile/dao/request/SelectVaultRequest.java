package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.dao.GroupDAO;
import com.veevavault.mobile.dao.ProductDAO;
import com.veevavault.mobile.dao.QueryDAO;
import com.veevavault.mobile.dao.UserDAO;
import com.veevavault.mobile.model.VaultEndpoint;

@Data
@EqualsAndHashCode(callSuper=false)
public class SelectVaultRequest extends DAORequest<Void> {
   private VaultEndpoint vaultEndpoint;
   private Listener callback;
   
   public interface Listener {
      public void onSelectVaultResponse();
   }

   @Override
   protected Void doInBackground(Void... params) {
      BaseDAO.setCurrentVaultEndpoint(vaultEndpoint);
      
      DAORequest.clearRequestCache();
      DocumentDAO.dumpCache();
      GroupDAO.dumpCache();
      QueryDAO.dumpAllSchemas();
      ProductDAO.dumpCache();
      UserDAO.dumpCache();
      return null;
   }
   
   @Override
   protected void onPostExecute(Void response) {
      callback.onSelectVaultResponse();
   }
}

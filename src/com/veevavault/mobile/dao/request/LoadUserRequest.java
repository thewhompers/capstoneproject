package com.veevavault.mobile.dao.request;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.content.Context;

import com.veevavault.mobile.dao.request.DAORequest;
import com.veevavault.mobile.dao.response.LoadUserResponse;
import com.veevavault.mobile.database.DatabaseAccessor;
import com.veevavault.mobile.model.User;

@Data
@EqualsAndHashCode(callSuper=false)
public class LoadUserRequest extends DAORequest<LoadUserResponse> {
   private Context context;
   private String username;
   private Listener callback;
   
   public interface Listener {
      public void onLoadUserResponse(LoadUserResponse response);
   }
   
   @Override
   public void onPreExecute() {
      
   }
   
   @Override
   protected LoadUserResponse doInBackground(Void... params) {
      LoadUserResponse response = new LoadUserResponse();
      
      DatabaseAccessor db = new DatabaseAccessor(context);
      
      response.setUser(db.loadUser(username));

      return response;
   }
   
   @Override
   protected void onPostExecute(LoadUserResponse response) {
      callback.onLoadUserResponse(response);
   }
}

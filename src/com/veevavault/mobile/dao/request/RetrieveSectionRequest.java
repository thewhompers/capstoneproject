package com.veevavault.mobile.dao.request;

import java.math.BigInteger;

import lombok.Data;
import lombok.EqualsAndHashCode;

import com.veevavault.mobile.dao.SectionDAO;
import com.veevavault.mobile.dao.response.RetrieveSectionResponse;

@Data
@EqualsAndHashCode(callSuper=false)
public class RetrieveSectionRequest extends DAORequest<RetrieveSectionResponse> {
   private BigInteger binderId;
   private BigInteger sectionId;
   private Listener callback;
   
   public interface Listener {
      public void onRetrieveSectionResponse(RetrieveSectionResponse response);
   }
   
   @Override
   protected RetrieveSectionResponse doInBackground(Void... params) {
      return SectionDAO.retrieveSection(this);
   }
   
   @Override 
   protected void onPostExecute(RetrieveSectionResponse response) {
      callback.onRetrieveSectionResponse(response);
   }
}

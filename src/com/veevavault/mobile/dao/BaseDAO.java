package com.veevavault.mobile.dao;

import java.util.List;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import com.google.gson.Gson;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.model.User;
import com.veevavault.mobile.model.VaultEndpoint;

public abstract class BaseDAO {
   @Getter(AccessLevel.PROTECTED)
   @Setter(AccessLevel.PROTECTED)
   private static String currentSessionId;
   
   @Getter(AccessLevel.PUBLIC)
   @Setter(AccessLevel.PUBLIC)
   private static User currentUser;
   
   @Getter(AccessLevel.PUBLIC)
   @Setter(AccessLevel.PUBLIC)
   private static VaultEndpoint currentVaultEndpoint;
   
   @Getter(AccessLevel.PUBLIC)
   @Setter(AccessLevel.PUBLIC)
   private static List<VaultEndpoint> availableVaultEndpoints;
   
   @Getter(AccessLevel.PROTECTED)
   @Setter(AccessLevel.PROTECTED)
   private static VeevaRestClientv7 veevaClient;
   
   protected static final Gson GSON = new Gson();

   protected static void setAuthorizationHeader() {
      veevaClient.setHeader("Authorization", getCurrentSessionId());
   }
}

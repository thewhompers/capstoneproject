package com.veevavault.mobile.dao;

import static com.veevavault.mobile.dao.JsonParser.getElement;
import static com.veevavault.mobile.dao.JsonParser.getElementList;

import java.util.ArrayList;
import java.util.List;

import org.springframework.util.LinkedMultiValueMap;

import com.google.gson.JsonElement;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.model.VaultEndpoint;

public class AuthenticationDAO extends BaseDAO {
   public static AuthenticateUserResponse authenticateUser(final AuthenticateUserRequest daoRequest) {   
      AuthenticateUserResponse daoResponse;
      LinkedMultiValueMap<String, String> requestBody = new LinkedMultiValueMap<String, String>();
      requestBody.add("username", daoRequest.getUsername());
      requestBody.add("password", daoRequest.getPassword());
      
      if (getVeevaClient() == null) {
         setVeevaClient(daoRequest.getRestClient());
      }
      
      String responseJson = null;
      if (daoRequest.getDomain() == null) {
         responseJson = getVeevaClient().authenticateUser(requestBody).getBody();
         daoResponse = buildAuthenticationResponseFromUniversalEndpoint(responseJson);
      } else {
         responseJson = getVeevaClient().authenticateUser(daoRequest.getDomain().getDnsName(),
            requestBody).getBody();
         daoResponse = buildAuthenticationResponseFromVaultEndpoint(responseJson);
         
         setCurrentVaultEndpoint(daoRequest.getDomain());
         setCurrentUser(UserDAO.retrieveUserByName(daoRequest.getUsername()));
      }
      
      setCurrentSessionId(daoResponse.getSessionId());    
      setAvailableVaultEndpoints(daoResponse.getVaults());
      
      return daoResponse;
   }
   
   private static AuthenticateUserResponse buildAuthenticationResponseFromUniversalEndpoint(String jsonResponse) {
      AuthenticateUserResponse daoResponse = new AuthenticateUserResponse();
      
      daoResponse.setResponseStatus(ResponseStatus.valueOf(getElement(jsonResponse, "responseStatus").getAsString()));
      if (daoResponse.getResponseStatus() == ResponseStatus.SUCCESS) {
         daoResponse.setSessionId(getElement(jsonResponse, "sessionid").getAsString());
      
         List<VaultEndpoint> vaults = new ArrayList<VaultEndpoint>();
      
         for (JsonElement vaultObject : getElementList(jsonResponse, "vaults")) {
            VaultEndpoint endpoint = new VaultEndpoint();
            endpoint.setVaultID(getElement(vaultObject, "vaultID").getAsString());
            endpoint.setName(getElement(vaultObject, "name").getAsString());
            endpoint.setDnsName(getElement(vaultObject, "dnsName").getAsString());
         
            vaults.add(endpoint);
         }

         daoResponse.setVaults(vaults);
      } else {
         daoResponse.setResponseMessage(getElement(getElementList(jsonResponse, "errors").get(0), "message").getAsString());
      }
      
      return daoResponse;
   }
   
   private static AuthenticateUserResponse buildAuthenticationResponseFromVaultEndpoint(String jsonResponse) {
      AuthenticateUserResponse daoResponse = new AuthenticateUserResponse();
      daoResponse.setResponseStatus(ResponseStatus.valueOf(getElement(jsonResponse, "responseStatus").getAsString()));
      
      if (daoResponse.getResponseStatus() == ResponseStatus.SUCCESS) {
         daoResponse.setSessionId(getElement(jsonResponse, "sessionId").getAsString());
      
         List<VaultEndpoint> vaults = new ArrayList<VaultEndpoint>();
      
         for (JsonElement vaultObject : getElementList(jsonResponse, "vaultIds")) {
            VaultEndpoint endpoint = new VaultEndpoint();
            endpoint.setVaultID(getElement(vaultObject, "id").getAsString());
            endpoint.setName(getElement(vaultObject, "name").getAsString());
            endpoint.setDnsName(getElement(vaultObject, "url").getAsString());
         
            vaults.add(endpoint);
         }
      
         daoResponse.setVaults(vaults);
      }
      else {
          daoResponse.setResponseMessage(getElement(getElementList(jsonResponse, "errors").get(0), "message").getAsString());
      }
      
      return daoResponse;
   }
}

package com.veevavault.mobile.dao;

import static com.veevavault.mobile.dao.JsonParser.getElement;
import static com.veevavault.mobile.dao.JsonParser.getElementList;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.google.gson.JsonElement;
import com.veevavault.mobile.dao.request.RetrieveSectionRequest;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveSectionResponse;
import com.veevavault.mobile.model.BindableIdentifier;
import com.veevavault.mobile.model.BindableType;
import com.veevavault.mobile.model.Section;

public class SectionDAO extends BaseDAO {   
   public static RetrieveSectionResponse retrieveSection(RetrieveSectionRequest request) {
      String jsonResponse = getVeevaClient().retrieveSection(
         getCurrentVaultEndpoint().getDnsName(),
         request.getBinderId().toString(),
         request.getSectionId().toString()).getBody();
      
      return buildRetrieveSectionResponse(jsonResponse);
   }
   
   private static RetrieveSectionResponse buildRetrieveSectionResponse(String jsonResponse) {
      RetrieveSectionResponse response = new RetrieveSectionResponse();
      Section section = new Section();
      
      section.setId(
         getElement(jsonResponse, "node", "properties", "id").getAsBigInteger());
      section.setName(
         getElement(jsonResponse, "node", "properties", "name__v").getAsString());
      
      response.setSection(section);

      List<BindableIdentifier> sectionContents = new ArrayList<BindableIdentifier>();
      
      for (JsonElement node : getElementList(jsonResponse, "node", "nodes")) {
         BindableIdentifier sectionContent = new BindableIdentifier();
         sectionContent.setType(
            BindableType.valueOf(
               StringUtils.upperCase(
                  getElement(node.getAsJsonObject(), "properties", "type__v").getAsString())));
         
         switch (sectionContent.getType()) {
         case DOCUMENT:
         case BINDER:
            sectionContent.setId(getElement(node, "properties", "document_id__v").getAsBigInteger());
            break;
         case SECTION:
            sectionContent.setId(getElement(node, "properties", "id").getAsBigInteger());
            break;
         }
         response.setResponseStatus(ResponseStatus.valueOf(getElement(jsonResponse, "responseStatus").getAsString()));
         sectionContents.add(sectionContent);
      }
      
      response.setSectionContents(sectionContents);
      
      return response;
   }
}

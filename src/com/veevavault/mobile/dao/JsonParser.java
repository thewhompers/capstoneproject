package com.veevavault.mobile.dao;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class JsonParser {
   private static final Gson GSON = new Gson();
   
   protected static List<JsonElement> getElementList(String jsonStr, String... path) {
      JsonObject jsonObject = GSON.fromJson(jsonStr, JsonObject.class);
      
      return getElementList(jsonObject, path);
   }
   
   protected static List<JsonElement> getElementList(JsonElement jsonEle, String... path) {
      return getElementList(jsonEle.getAsJsonObject(), path);
   }
   
   protected static List<JsonElement> getElementList(JsonObject jsonObj, String... path) {
      List<JsonElement> values = new ArrayList<JsonElement>();
      
      if (getElement(jsonObj, path) != null && !(getElement(jsonObj, path).isJsonNull())) {
         for (JsonElement element : getElement(jsonObj, path).getAsJsonArray()) {
            values.add(element);
         }
      }
      return values;
   }
   
   protected static JsonElement getElement(String jsonStr, String... path) {
      JsonObject jsonObject = GSON.fromJson(jsonStr, JsonObject.class);
      
      return getElement(jsonObject, path);
   }
   
   protected static JsonElement getElement(JsonElement jsonEle, String... path) {
      return getElement(jsonEle.getAsJsonObject(), path);
   }
   
   protected static JsonElement getElement(JsonObject jsonObj, String... path) {
      JsonElement value = jsonObj;
      
      for (String key : path) {
         value = jsonObj.get(key);
         if (value.isJsonObject()) {
            jsonObj = value.getAsJsonObject();
         }
      }
      
      return value;
   }
}

package com.veevavault.mobile.activity;

import com.veevavault.mobile.R;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class WebViewForgotPasswordActivity extends Activity {
	private WebView webView;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web_view);
		
		webView = (WebView) findViewById(R.id.webView1);
		webView.getSettings().setJavaScriptEnabled(true);
		webView.loadUrl("https://login.veevavault.com/auth/forgotpassword");
	}
}

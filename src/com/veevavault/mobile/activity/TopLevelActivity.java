package com.veevavault.mobile.activity;

import java.util.ArrayList;

import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.request.RetrieveVaultsRequest;
import com.veevavault.mobile.dao.response.RetrieveVaultsResponse;
import com.veevavault.mobile.model.VaultEndpoint;

public abstract class TopLevelActivity extends FragmentActivity implements OnNavigationListener, RetrieveVaultsRequest.Listener {
	
	// We will be doing a lot of fragment managing, so lets just keep one instance around.
	protected FragmentManager mFragmentManager;

	// The ActionBar
	protected ActionBar mActionBar;
	protected SpinnerAdapter mSpinnerAdapter;
	protected ArrayList<String> mSpinnerTitles;
	protected CharSequence mTitle;
	
	// Navigation Drawer stuff
	protected String[] mDrawerTitles;
	protected DrawerLayout mDrawerLayout;
	protected ListView mDrawerList;
	protected ActionBarDrawerToggle mDrawerToggle;
	protected CharSequence mDrawerTitle;
	
	protected Resources res;
	private SharedPreferences sharedPreferencesPin;
	private static final String MY_APP_PREFS = "myAppPrefs";
	private static final int MIN_PIN_LEN = 4;
	private static final int MAX_PIN_LEN = 6;
	
	private static final int HOME_ACTIVITY = 0;
	private static final int LIBRARY_ACTIVITY = 1;
	private static final int ARBITRARY_PENDING_ID = 1249100;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_top_level_drawer);
		
        res = getResources();		
		mFragmentManager = getSupportFragmentManager();
		sharedPreferencesPin = this.getApplicationContext().getSharedPreferences(MY_APP_PREFS, 0);
		initNavDrawer();
		initActionBar();
	}
	
	@Override
	protected void onPause() {
	   if (!VaultApplication.isOffline()) {
	      VaultApplication app = VaultApplication.getInstance();
	      app.cleanupBeforeClosing();
	   }
	   super.onPause();
	}

	public void initNavDrawer() {
		mDrawerTitles = getResources().getStringArray(
				R.array.nav_drawer_titles_array);
		mDrawerLayout = (DrawerLayout) findViewById(R.id.home_drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.top_drawer_list);

		// Set the adapter for the nav drawer list view
		mDrawerList.setAdapter(new ArrayAdapter<String>(this,
				R.layout.drawer_list_item, mDrawerTitles));

		// Set the list's click listener
		mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

	}

	class DrawerItemClickListener implements
			ListView.OnItemClickListener {

		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			Intent intent = null;
			
			switch(position) {		
			case HOME_ACTIVITY:
				intent = new Intent(TopLevelActivity.this, HomeActivity_.class);
				break;
			case LIBRARY_ACTIVITY:
				intent = new Intent(TopLevelActivity.this, LibraryActivity_.class);
				break;
			default:
				Toast.makeText(getBaseContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();
				return;
			}
			
			intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
			mDrawerList.setItemChecked(position, true);
			mDrawerLayout.closeDrawer(mDrawerList);
			
			startActivity(intent);
		}
	}
	
	protected void initActionBar() {
		mActionBar = getActionBar();
		mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		mActionBar.setDisplayShowTitleEnabled(false);
		mDrawerTitle = getResources().getString(R.string.drawer_title);
		mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
                ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                getActionBar().setTitle(mTitle);
                getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
                getActionBar().setDisplayShowTitleEnabled(false);
                invalidateOptionsMenu();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActionBar().setTitle(mDrawerTitle);
                getActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
                getActionBar().setDisplayShowTitleEnabled(true);
                invalidateOptionsMenu();
            }
        };
        
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        // Make action bar icon clickable and add the drawer indicator
        getActionBar().setDisplayHomeAsUpEnabled(true);
       // getActionBar().setHomeButtonEnabled(true);
	}
	
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
          return true;
        }
        // Handle your other action bar items...
        switch (item.getItemId()) {
        case R.id.action_select_vault:
        	//Retrieve list of vault endpoints
        	RetrieveVaultsRequest vaultsRequest = new RetrieveVaultsRequest();
        	vaultsRequest.setCallback(this);
        	vaultsRequest.setContext(this);
        	vaultsRequest.execute();
            return true;
        case R.id.action_offline_pin:
        	offlinePinSetupDialogBox();
        	return true;
        case R.id.action_logout:
           activityLogout();
           return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }
    
    public void offlinePinSetupDialogBox() {
		final EditText input = new EditText(TopLevelActivity.this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER);
		input.setTransformationMethod(PasswordTransformationMethod.getInstance());
		input.setImeOptions(EditorInfo.IME_ACTION_DONE);
		
    	final AlertDialog pinDialog = new AlertDialog.Builder(TopLevelActivity.this)
	        .setView(input)
	        .setTitle(getResources().getString(R.string.offline_title))
	        .setMessage(getResources().getString(R.string.message_offline_prompt))
	        .setPositiveButton(getResources().getString(R.string.offline_button_name), null)
	        .create();
	 
		pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {
		 
		    @Override
		    public void onShow(DialogInterface dialog) {
		 
		        Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
		        b.setOnClickListener(new View.OnClickListener() {	 
		            @Override
		            public void onClick(View view) {
		            	Boolean wantToCloseDialog = false;
		            	String pin = input.getText().toString();
				    	if (pin.length() >= MIN_PIN_LEN && pin.length() <= MAX_PIN_LEN) {
				    		String username = BaseDAO.getCurrentUser().getName();
				    		//check and see if the user already has a pin
				    		String oldPin = sharedPreferencesPin.getString(username + "pin", "");
				    		if (!oldPin.equals("")) {
				    			Editor prefsEditor = sharedPreferencesPin.edit();
				    			prefsEditor.remove(username+"pin");
				    			prefsEditor.commit();
				    		}
				    		
							Editor editor = sharedPreferencesPin.edit();
							editor.putString(username + "pin", pin);
							editor.commit();
				    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.offline_confirmation), Toast.LENGTH_LONG).show();
					    	wantToCloseDialog = true;
				    	}
				    	else if (pin.length() < MIN_PIN_LEN) {
				    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_offline_pin_short), Toast.LENGTH_LONG).show();
				    	}
				    	else if (pin.length() > MAX_PIN_LEN) {
				    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_offline_pin_long), Toast.LENGTH_LONG).show();
				    	}
						if (wantToCloseDialog) {
							pinDialog.dismiss();
						}
		 
		            }
		        });
		    }
		});
		pinDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		pinDialog.show();
    }
    
    @Override
    public void onRetrieveVaultsResponse(RetrieveVaultsResponse response) {   
    	// LAUNCH INTENT FOR SELECTING VAULT
        Intent selectVaultIntent = new Intent(getApplicationContext(),
              SelectVaultActivity.class)
        	  .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        selectVaultIntent.putParcelableArrayListExtra("vaults",
              (ArrayList<VaultEndpoint>) response.getVaults());
        selectVaultIntent.putExtra("username", response.getUsername());
        selectVaultIntent.putExtra("password", "");
        selectVaultIntent.putExtra("overflow", true);   
        startActivity(selectVaultIntent);

    }
	
    public void activityLogout() {
       //http://stackoverflow.com/questions/6609414/howto-programatically-restart-android-app
       //This does a hard reset of the application, this way it ensures a clean wipe of all persistant data throughout the activity
       //Commented out is the more cleaner way of doing it. just clearing all the activities and going back to the top
      /* Intent mStartActivity = new Intent(this, LoginActivity_.class);
       int mPendingIntentId = ARBITRARY_PENDING_ID;
       PendingIntent mPendingIntent = PendingIntent.getActivity(this, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
       AlarmManager mgr = (AlarmManager)this.getSystemService(Context.ALARM_SERVICE);
       mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 100, mPendingIntent);
       System.exit(0);*/

       
       Intent intent = new Intent(TopLevelActivity.this, LoginActivity_.class);
       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | 
             Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
       startActivity(intent);
       
    }

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
		
		//TODO: SET CERTAIN ITEMS DEPENDING ON WHETHER OR NOT DRAWER IS OPEN
		
		return super.onPrepareOptionsMenu(menu);
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.toplevel, menu);
		return true;
	}
	
}

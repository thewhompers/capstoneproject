package com.veevavault.mobile.activity;

import java.math.BigInteger;
import java.util.List;

import org.androidannotations.annotations.EActivity;
import org.springframework.util.LinkedMultiValueMap;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnGroupCollapseListener;
import android.widget.ExpandableListView.OnGroupExpandListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.adapter.DocumentPropertiesListAdapter;
import com.veevavault.mobile.dao.request.RetrieveBinderRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentFileUrlRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentPropertiesRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentThumbnailRequest;
import com.veevavault.mobile.dao.response.ErrorType;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveBinderResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentFileUrlResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentPropertiesResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentThumbnailResponse;
import com.veevavault.mobile.etc.DropDownAnim;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Property;

@EActivity
public class DocumentPropertiesActivity extends Activity implements
      RetrieveDocumentRequest.Listener,
      RetrieveDocumentThumbnailRequest.Listener,
      RetrieveDocumentPropertiesRequest.Listener,
      RetrieveDocumentFileUrlRequest.Listener,
      RetrieveBinderRequest.Listener,
      OnGroupExpandListener,
      OnGroupCollapseListener {
   private BigInteger docId;
   private Document document;
   private TextView documentName;
   private ImageView thumbnail;
   private Button downloadButton;
   private Button openButton;
   private ExpandableListView listView;
   private DocumentPropertiesListAdapter listAdapter;
   LinearLayout linLayout;
   private LibraryActivity libActivity;
   private Context context;
   private int groupsExpanded;
   private List<String> sectionNames;
   private LinkedMultiValueMap<String, Property> sectionProperties;  
   private List<Property> properties;
   private boolean itsDown;

   private ProgressBar spinner;


   private static final String DOCUMENT_ID_KEY = "DocumentID";

   @Override
   public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      itsDown = false;
      docId = new BigInteger(this.getIntent().getExtras().getString("id"));
      groupsExpanded = 0;
      
      int ot = getResources().getConfiguration().orientation;
      if (ot == Configuration.ORIENTATION_LANDSCAPE) {
         this.setContentView(R.layout.document_properties_view_landscape);
      } else {
         this.setContentView(R.layout.document_properties_view);
         linLayout = (LinearLayout) findViewById(R.id.doc_prop_lin_layout);
      }
      
      documentName = (TextView) findViewById(R.id.p_documentName);
      thumbnail = (ImageView) findViewById(R.id.p_documentThumbnail);
      downloadButton = (Button) findViewById(R.id.p_downloadButton);
      openButton = (Button) findViewById(R.id.p_openButton);
      spinner = (ProgressBar) findViewById(R.id.p_progress_bar);
      spinner.setVisibility(View.VISIBLE);
      
      RetrieveDocumentRequest req = new RetrieveDocumentRequest();
      req.setDocumentId(docId);
      req.setCallback(this);
      req.setContext(this);
      req.execute();
   }
   
   @Override
   public void onResume() {
      if (document != null) {
         setupButtons();
      }
      super.onResume();
   }

   @Override
   public void onRestart() {
      super.onRestart();
   }

   @Override
   public void onDocumentPropertiesResponse(
         RetrieveDocumentPropertiesResponse response) {
      properties = response.getProperties();
   }

   @Override
   public void onRetrieveDocumentFileUrlResponse(
         RetrieveDocumentFileUrlResponse response) {
      Log.d(null, "doc url: " + response.getDocumentFileUrl());
      String format = document.getPropertyValue("format__v");
      if (format.equals("")) {
         Toast.makeText(this,
               getResources().getString(R.string.download_doc_property_null),
               Toast.LENGTH_SHORT).show();
      } else {
         String name = document.getPropertyValue("name__v");
         name = name.replace(" ", "%20");
         Log.d(null, "doc info: " + name + " " + format);
         DownloadDocument downloadDoc = new DownloadDocument(response.getDocument(),
               response.getDocumentFileUrl(), this, name, format,
               response.getSessionId());
         if (document.getFilePath() != null) {
            downloadDoc.launchApplicationIntent();
         } else {
            downloadDoc.downloadDocument();
            downloadDoc.setUpBroadcastReciever();
         }
      }
   }

   @Override
   public void onRetrieveDocumentResponse(RetrieveDocumentResponse response) {
      spinner.setVisibility(View.GONE);
      
      if (response.getResponseStatus() != ResponseStatus.SUCCESS) {
         if (response.getErrorType() != ErrorType.INVALID_DOCUMENT) {
            Toast.makeText(this, "Error Retrieving Document!", Toast.LENGTH_SHORT)
               .show();
         }
         else {
            RetrieveBinderRequest binderRequest = new RetrieveBinderRequest();
            binderRequest.setBinderId(docId);
            binderRequest.setCallback(this);
            binderRequest.setContext(this);
            binderRequest.execute();
         }
      } else {
         handleDocumentLoad(response.getDocument());
      }
   }
   
   @Override
   public void onRetrieveBinderResponse(RetrieveBinderResponse response) {
      spinner.setVisibility(View.GONE);
      
      if (response.getResponseStatus() != ResponseStatus.SUCCESS) {
         if (response.getErrorType() != ErrorType.INVALID_BINDER) {
            Toast.makeText(this, "Error Retrieving Binder!", Toast.LENGTH_SHORT)
               .show();
         }
      } else {
         handleDocumentLoad(response.getBinderDocument());
      }
   }
   
   private void handleDocumentLoad(Document document) {
      this.document = document;
      sectionProperties = document.getPropertiesBySection();

      this.setTitle(getResources().getString(R.string.title_activity_docprops)
            + " " + document.getPropertyValue("name__v"));
      documentName.setText(document.getPropertyByName("name__v").getValue());
      
      setupButtons();

      listView = (ExpandableListView) findViewById(R.id.p_expandableListView);
      if (document.getThumbnail() != null) {
         thumbnail.setImageBitmap(document.getThumbnail());
      } else {
         requestDocumentThumbnail(document);
      }
      context = getBaseContext();

      listAdapter = new DocumentPropertiesListAdapter(context,
            sectionProperties);

      listView.setAdapter(listAdapter);
     /* listView.setOnGroupExpandListener(this);
      listView.setOnGroupCollapseListener(this); */
   }

   private void setupButtons() {
      if (document.getFilePath() != null) {
         openButton.setVisibility(View.VISIBLE);
         downloadButton.setVisibility(View.GONE);
         openButton.setOnClickListener(new DownloadClickListener());;
      }
      else {
         downloadButton.setVisibility(View.VISIBLE);
         openButton.setVisibility(View.GONE);
         downloadButton.setOnClickListener(new DownloadClickListener());
      }
      if ((document != null) && (document.getPropertyValue("binder__v") != null)) {
         if (document.getPropertyValue("binder__v").equals("true")) {
            downloadButton.setVisibility(View.INVISIBLE);
         }
      }
   }

   public void requestDocumentThumbnail(Document doc) {      
      RetrieveDocumentThumbnailRequest daoRequest = new RetrieveDocumentThumbnailRequest();

      daoRequest.setDocumentId(doc.getId());
      daoRequest.setMinorVersion(doc
            .getPropertyValue("minor_version_number__v"));
      daoRequest.setMajorVersion(doc
            .getPropertyValue("major_version_number__v"));
      daoRequest.setCallback(this);
      daoRequest.setContext(this);

      daoRequest.execute();
   }

   @Override
   public void onThumbnailResponse(RetrieveDocumentThumbnailResponse response) {
      if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
         document.setThumbnail(response.getImage());
         thumbnail.setImageBitmap(response.getImage());
      } else {
         Toast.makeText(this,
               getResources().getString(R.string.error_thumbnail),
               Toast.LENGTH_SHORT).show();
      }
   }   
   
   @Override
   public void onGroupExpand(int groupPosition) {
      groupsExpanded++;
      if (linLayout != null && groupsExpanded > 0) {       
         if (itsDown) {
            itsDown = false;
            DropDownAnim.collapse(linLayout);
         }
      }
   }

   @Override
   public void onGroupCollapse(int arg0) {
      groupsExpanded--;
      if (linLayout != null && groupsExpanded <= 0) {
         if (!itsDown) {
            itsDown = true;
            DropDownAnim.expand(linLayout);
         }
      }
   }
   
   class DownloadClickListener implements OnClickListener {

      @Override
      public void onClick(View v) {
         RetrieveDocumentFileUrlRequest req = new RetrieveDocumentFileUrlRequest();
         req.setDocument(document);
         req.setCallback(DocumentPropertiesActivity.this);
         req.setContext(DocumentPropertiesActivity.this);
         req.execute();
      }
      
   }
}

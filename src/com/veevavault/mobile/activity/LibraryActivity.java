package com.veevavault.mobile.activity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import lombok.Getter;

import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.rest.RestService;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.EditText;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.api.NamedFilter;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.fragment.DocumentListFragment.OnListItemSelect;
import com.veevavault.mobile.fragment.LibraryListFragment;
import com.veevavault.mobile.fragment.SearchHistoryFragment;
import com.veevavault.mobile.fragment.SearchHistoryFragment.OnSearchSelectListener;
import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.Document;

@EActivity
public class LibraryActivity extends TopLevelActivity implements OnListItemSelect, OnSearchSelectListener {

   private Map<String, NamedFilter> namedFiltersByTitle;
   private LibraryListFragment documentListFragment;
   private SharedPreferences viewPreference;
   public static final int LISTVIEW = 0;
   public static final int GRIDVIEW = 1;
   @Getter
   private int viewState;
   private String currentFilter;
   @Getter
   private Document selectedDocument;

   private AlertDialog searchDialog;
   private String searchString;
   private String searchScope;
   private boolean userDidASearch;
   private boolean mNaviFirstHit = false;
   private Menu mOptionsMenu;
   
   @RestService
   VeevaRestClientv7 veevaClient;

   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      createSearchDialog();
      loadSavedPreferences();
   }

   @Override
   protected void onResume() {
      super.onResume();
      loadSavedPreferences();
      if (documentListFragment != null) {
         documentListFragment.setViewType(viewState);
         if (mOptionsMenu != null) {
            if (viewState == LISTVIEW) {
               mOptionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_grid);
            }
            else {
               mOptionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_list);
            }
         }
      }
   }
   
   @Override
   protected void onPause() {
      super.onPause();
      savePreferences("view_Value", viewState);
   }

   @Override
   protected void initActionBar() {
      // must call super method to instantiate action bar and set proper
      // attributes
      super.initActionBar();

      // set the spinner titles array
      mSpinnerTitles = new ArrayList<String>(Arrays.asList(res
            .getStringArray(R.array.library_spinner_array)));

      // initialize the spinner array adapter
      mSpinnerAdapter = new ArrayAdapter<String>(this,
            R.layout.spinner_list_item, mSpinnerTitles);

      namedFiltersByTitle = new HashMap<String, NamedFilter>();
      namedFiltersByTitle.put(res.getString(R.string.all_documents),
            NamedFilter.ALL_DOCUMENTS);
      namedFiltersByTitle.put(
            res.getString(R.string.recent_documents),
            NamedFilter.RECENT_DOCUMENTS);
      namedFiltersByTitle.put(res.getString(R.string.my_documents),
            NamedFilter.MY_DOCUMENTS);
      namedFiltersByTitle.put(
            res.getString(R.string.favorite_documents),
            NamedFilter.FAVORITES);

      // set the actionbar callback to this (implements OnNavigationListener)
      mActionBar.setListNavigationCallbacks(mSpinnerAdapter, this);
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      mOptionsMenu = menu;
      return super.onCreateOptionsMenu(menu);
   }

   @Override
   public boolean onNavigationItemSelected(int position, long itemId) {
      //SEE:
      //http://stackoverflow.com/questions/10167162/
      //onnavigationitemselected-in-actionbar-is-being-called-at-startup-how-can-avoid-i
      if (!mNaviFirstHit) {
         mNaviFirstHit = true;
         int index = viewPreference.getInt("current_Filter", 0);
         currentFilter = mSpinnerTitles.get(index);
         this.mActionBar.setSelectedNavigationItem(index);
      }
      else {
         currentFilter = mSpinnerTitles.get(position);
         savePreferences("current_Filter", position);
      }
      launchListFragment();
      return true;
   }

   @Override
   public void onBackPressed() {
      if (mFragmentManager.getBackStackEntryCount() > 0) {
         mFragmentManager.popBackStack();
      } else
         super.onBackPressed();
   }

   @Override
   public boolean onPrepareOptionsMenu(Menu menu) {
      super.onPrepareOptionsMenu(menu);
      menu.findItem(R.id.action_switch_view).setVisible(true);
      if (viewState == LISTVIEW) {
         menu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_grid);
      }
      else {
         menu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_list);
      }
      return true;
   }

   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      super.onOptionsItemSelected(item);

      switch (item.getItemId()) {
      case R.id.action_switch_view:
         if (viewState == LISTVIEW) {
            item.setIcon(R.drawable.ic_action_view_as_list);
            viewState = GRIDVIEW;            
         } else if (viewState == GRIDVIEW) {
            item.setIcon(R.drawable.ic_action_view_as_grid);
            viewState = LISTVIEW;            
         }
         savePreferences("view_Value", viewState);
         documentListFragment.setViewType(viewState);
         return true;
      case R.id.refresh:
         if (!currentFilter.equals(res.getString(R.string.downloaded_documents))
               && !currentFilter.equals(res.getString(R.string.search_history))) {
            documentListFragment.refresh();
         }
         return true;
      case R.id.search_icon:
         searchDialog.show();
         searchDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
         return true;
      default:
         return super.onOptionsItemSelected(item);
      }
   }

   private void createSearchDialog() {
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      LayoutInflater inflater = this.getLayoutInflater();

      builder
            .setView(inflater.inflate(R.layout.search_dialog, null))
            .setTitle(res.getString(R.string.search_dialog_title))
            .setPositiveButton(R.string.search_prop_and_cont_button,
                  new DialogInterface.OnClickListener() {

                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                        launchSearchActivity("all",
                              ((EditText) searchDialog.getCurrentFocus()
                                    .findViewById(R.id.search_edit_text))
                                    .getText().toString());
                     }
                  })
            .setNegativeButton(R.string.search_properties_button,
                  new DialogInterface.OnClickListener() {

                     @Override
                     public void onClick(DialogInterface dialog, int which) {
                        EditText searchText = (EditText) searchDialog.getCurrentFocus().findViewById(R.id.search_edit_text);
                        if (searchText != null) {
                           String text = searchText.getText() == null ? "" : searchText.getText().toString();
                           launchSearchActivity("properties", text);
                        }
                     }
                  });

      searchDialog = builder.create();
   }
   
   @Override
   public void launchSearchActivity(String scope, String search) {
      Intent i = new Intent(this, SearchResultsActivity_.class);
      i.putExtra("search", search);
      i.putExtra("scope", scope);
      startActivity(i);
   }

   private void loadSavedPreferences() {
      this.viewPreference = PreferenceManager.getDefaultSharedPreferences(this);
      viewState = viewPreference.getInt("view_Value", LISTVIEW);
   }

   private void savePreferences(String key, int value) {
      SharedPreferences sharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(this);
      Editor editor = sharedPreferences.edit();
      editor.putInt(key, value);
      editor.commit();
   }

   private void launchListFragment() {
      Bundle fragmentArgs;
      FragmentTransaction ft;

      ft = mFragmentManager.beginTransaction();
      // Replace whatever is in the fragment container with this fragment
      // and give the fragment a tag name equal to the string at the position
      // selected
    /*  if (currentFilter.equals(this.res.getString(R.string.downloaded_documents))) {
         ft.replace(R.id.fragment_container, new DownloadedDocumentsFragment(),
               currentFilter);
      } 
      else */if (currentFilter.equals(this.res.getString(R.string.search_history))) {
         ft.replace(R.id.fragment_container, new SearchHistoryFragment(), currentFilter);
      }
      else {
         documentListFragment = new LibraryListFragment();
         documentListFragment.setViewType(viewState);
         fragmentArgs = new Bundle();
         if (currentFilter.equals(this.res.getString(R.string.downloaded_documents))) {
            fragmentArgs.putBoolean("onDevice", true);
         }
         else {
            fragmentArgs.putBoolean("onDevice", false);
            fragmentArgs.putString("title", this.namedFiltersByTitle.get(currentFilter).toString());
            fragmentArgs.putString("scope", searchScope);
            fragmentArgs.putString("search", searchString);
         }
         documentListFragment.setArguments(fragmentArgs);
         ft.replace(R.id.fragment_container, documentListFragment, currentFilter);
      }

      
      ft.commit();
   }

   @Override
   public void onListItemSelect(Bindable bindable) {
      Document doc = (Document)bindable;
      String typeOfDoc = doc.getPropertyValue("binder__v");

      if (typeOfDoc.equals("true")) {
         //Open a binder activity         
         Intent intent = new Intent(this, BinderActivity_.class);
         intent.putExtra("id", doc.getId().toString());
         intent.putExtra("view_Value", viewState);
         intent.putExtra("binderName", doc.getPropertyValue("name__v"));
         startActivity(intent);
      }
      else {
         Intent intent = new Intent(this, DocumentPropertiesActivity_.class);
         intent.putExtra("id", doc.getId().toString());
         startActivity(intent);
      }
   }
}

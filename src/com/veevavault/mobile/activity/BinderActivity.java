package com.veevavault.mobile.activity;

import java.math.BigInteger;
import java.util.Stack;

import org.androidannotations.annotations.EActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils.TruncateAt;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.fragment.BinderListFragment;
import com.veevavault.mobile.fragment.DocumentListFragment.OnListItemSelect;
import com.veevavault.mobile.fragment.SectionListFragment;
import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Section;

@EActivity
public class BinderActivity extends TopLevelActivity implements OnListItemSelect {
   private BigInteger binderId;
   private String binderName;
   public static final int LISTVIEW = 0;
   public static final int GRIDVIEW = 1;
   private SharedPreferences viewPreference;
   private int viewState;
   BinderListFragment fragment;
   private Menu mOptionsMenu;
   private Stack<BinderListFragment> fragmentsOnStack;
   private String title;
   
   
   @Override
   public boolean onPrepareOptionsMenu(Menu menu) {
      super.onPrepareOptionsMenu(menu);
      menu.findItem(R.id.search_icon).setIcon(R.drawable.ic_action_info);
      if (viewState == LISTVIEW) {
         menu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_grid);
      }
      else {
         menu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_list);
      }
      return true;
   }
   
   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      mOptionsMenu = menu;
      return super.onCreateOptionsMenu(menu);
   }
   
   @Override
   protected void onResume() {
      super.onResume();
      loadSavedPreferences();
      
      if (fragment != null) {
         fragment.setViewType(viewState);
         if (mOptionsMenu != null) {
            if (viewState == LISTVIEW) {
               mOptionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_grid);
            }
            else {
               mOptionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_list);
            }
         }
      }
   }
   
   @Override
   protected void initActionBar() {
      super.initActionBar();
      mActionBar.setDisplayShowTitleEnabled(true);
      mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
      mDrawerToggle = new ActionBarDrawerToggle(
            this,                  /* host Activity */
            mDrawerLayout,         /* DrawerLayout object */
            R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
            R.string.drawer_open,  /* "open drawer" description */
            R.string.drawer_close  /* "close drawer" description */
            ) {

        /** Called when a drawer has settled in a completely closed state. */
        public void onDrawerClosed(View view) {
            super.onDrawerClosed(view);
            getActionBar().setTitle(title);
            invalidateOptionsMenu();
        }

        /** Called when a drawer has settled in a completely open state. */
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
            getActionBar().setTitle(mDrawerTitle);
            invalidateOptionsMenu();
        }
    };
    mDrawerLayout.setDrawerListener(mDrawerToggle);
   }
   
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      super.onOptionsItemSelected(item);

      switch (item.getItemId()) {
      case R.id.action_switch_view:
         if (viewState == LISTVIEW) {
            item.setIcon(R.drawable.ic_action_view_as_list);
            viewState = GRIDVIEW;            
         } else if (viewState == GRIDVIEW) {
            item.setIcon(R.drawable.ic_action_view_as_grid);
            viewState = LISTVIEW;            
         }
         savePreferences("view_Value", viewState);
         fragment.setViewType(viewState);
         return true;
      case R.id.refresh:
         fragment.refresh();
         return true;
      case R.id.search_icon:
         Intent intent = new Intent(this, DocumentPropertiesActivity_.class);
         intent.putExtra("id", binderId.toString());
         startActivity(intent);
         return true;
      default:
         return super.onOptionsItemSelected(item);
      }
   }
   
   private void savePreferences(String key, int value) {
      SharedPreferences sharedPreferences = PreferenceManager
            .getDefaultSharedPreferences(this);
      Editor editor = sharedPreferences.edit();
      editor.putInt(key, value);
      editor.commit();
   }
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      
      binderId = new BigInteger(this.getIntent().getExtras().getString("id"));
      viewState = this.getIntent().getExtras().getInt("view_Value");
      binderName = this.getIntent().getExtras().getString("binderName");
      title = binderName;
      mActionBar.setTitle(title);
      FragmentManager fManager = getSupportFragmentManager();
      FragmentTransaction ft = fManager.beginTransaction();
      fragment = new BinderListFragment();
      fragment.setViewType(viewState);
      Bundle fragmentArgs = new Bundle();                           
      fragmentArgs.putString("binderId", binderId.toString());
      fragment.setArguments(fragmentArgs);
      
      ft.add(R.id.fragment_container, fragment);
      ft.commit();
      
      fragmentsOnStack = new Stack<BinderListFragment>();
   }


   private void loadSavedPreferences() {
      this.viewPreference = PreferenceManager.getDefaultSharedPreferences(this);
      viewState = viewPreference.getInt("view_Value", LISTVIEW);
   }
   
   @Override
   public void onBackPressed() {
      if (mFragmentManager.getBackStackEntryCount() > 0) {
         mFragmentManager.popBackStack();
         fragment = fragmentsOnStack.pop();
         fragment.setViewType(viewState);
         if (mOptionsMenu != null) {
            if (viewState == LISTVIEW) {
               mOptionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_grid);
            }
            else {
               mOptionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_list);
            }
         }
         title = title.substring(0, title.lastIndexOf(">"));
         this.mActionBar.setTitle(title);
      }
      else
         super.onBackPressed();
   }
   
   @Override
   public void onListItemSelect(Bindable bindItem) {
      if (bindItem.getClass() == Document.class) {
         Document doc = (Document)bindItem;
         String isBinder = doc.getPropertyValue("binder__v");
         //Open a binder activity
         if (isBinder.equals("true")) {
            Intent intent = new Intent(this, BinderActivity_.class);
            intent.putExtra("id", doc.getId().toString());
            intent.putExtra("view_Value", viewState);
            intent.putExtra("binderName", binderName);
            startActivity(intent);
         }
         //Open document's properties
         else {
            Intent intent = new Intent(this, DocumentPropertiesActivity_.class);
            intent.putExtra("id", doc.getId().toString());
            startActivity(intent);
         }
      }
      else {
         Section section = (Section) bindItem;
         Bundle fragmentArgs = new Bundle();
         FragmentTransaction ft = this.mFragmentManager.beginTransaction();
         fragmentsOnStack.push(fragment);
         fragment = new SectionListFragment();
         fragmentArgs.putString("binderId", binderId.toString());
         fragmentArgs.putString("sectionId", section.getId().toString());
         fragmentArgs.putString("binderName", binderName);
         fragment.setViewType(viewState);
         fragment.setArguments(fragmentArgs);
         
         ft.replace(R.id.fragment_container, fragment).addToBackStack(null);
         ft.commit();
         
         title = this.mActionBar.getTitle() + ">" + section.getName();
         this.mActionBar.setTitle(title);
      }
   }

   @Override
   public boolean onNavigationItemSelected(int arg0, long arg1) {
      // TODO Auto-generated method stub
      return false;
   }
}

package com.veevavault.mobile.activity;

import java.util.ArrayList;
import java.util.Arrays;

import lombok.Getter;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.fragment.ActiveWorkflowsFragment;
import com.veevavault.mobile.fragment.ActiveWorkflowsFragment.OnActiveWorkflowSelectListener;
import com.veevavault.mobile.fragment.DetailedWorkflowFragment;
import com.veevavault.mobile.fragment.TaskFragment;
import com.veevavault.mobile.model.ActiveWorkflow;

@EActivity()
public class HomeActivity extends TopLevelActivity implements OnActiveWorkflowSelectListener {
	
	/** Task Fragment **/
	TaskFragment taskFragment;
	
	/** ActiveWorkflow Fragment **/
	ActiveWorkflowsFragment workflowFragment;
	
	/** Selected ActiveWorkflow **/
	@Getter private ActiveWorkflow selectedWorkflow = null;
	
	AlertDialog dialog;
	
	private SharedPreferences sharedPreferencesPin;
	private static final String MY_APP_PREFS = "myAppPrefs";
	private static final int MIN_PIN_LEN = 4;
	private static final int MAX_PIN_LEN = 6;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		promptForOfflinePinSetup();
		//createDialog();
	}
	
	protected void promptForOfflinePinSetup()
	{
		sharedPreferencesPin = this.getApplicationContext().getSharedPreferences(MY_APP_PREFS, 0);
		String username = BaseDAO.getCurrentUser().getName();
		String pin = sharedPreferencesPin.getString(username+"pin", "");
		if (pin.equals(""))
		{
			final EditText input = new EditText(HomeActivity.this);
			input.setInputType(InputType.TYPE_CLASS_NUMBER);
			input.setTransformationMethod(PasswordTransformationMethod.getInstance());
			input.setImeOptions(EditorInfo.IME_ACTION_DONE);
			
	    	final AlertDialog pinDialog = new AlertDialog.Builder(HomeActivity.this)
		        .setView(input)
		        .setTitle(getResources().getString(R.string.offline_title))
		        .setMessage(getResources().getString(R.string.message_offline_prompt))
		        .setPositiveButton(getResources().getString(R.string.offline_button_name), null)
		        .create();
		 
			pinDialog.setOnShowListener(new DialogInterface.OnShowListener() {
			 
			    @Override
			    public void onShow(DialogInterface dialog) {
			 
			        Button b = pinDialog.getButton(AlertDialog.BUTTON_POSITIVE);
			        b.setOnClickListener(new View.OnClickListener() {	 
			            @Override
			            public void onClick(View view) {
			            	Boolean wantToCloseDialog = false;
			            	String pin = input.getText().toString();
					    	if (pin.length() >= MIN_PIN_LEN && pin.length() <= MAX_PIN_LEN) {
					    		String username = BaseDAO.getCurrentUser().getName();
					    		//check and see if the user already has a pin
					    		String oldPin = sharedPreferencesPin.getString(username + "pin", "");
					    		if (!oldPin.equals("")) {
					    			Editor prefsEditor = sharedPreferencesPin.edit();
					    			prefsEditor.remove(username+"pin");
					    			prefsEditor.commit();
					    		}
					    		
								Editor editor = sharedPreferencesPin.edit();
								editor.putString(username + "pin", pin);
								editor.commit();
					    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.offline_confirmation), Toast.LENGTH_LONG).show();
						    	wantToCloseDialog = true;
					    	}
					    	else if (pin.length() < MIN_PIN_LEN) {
					    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_offline_pin_short), Toast.LENGTH_LONG).show();
					    	}
					    	else if (pin.length() > MAX_PIN_LEN) {
					    		Toast.makeText(getApplicationContext(), getResources().getString(R.string.error_offline_pin_long), Toast.LENGTH_LONG).show();
					    	}
							if (wantToCloseDialog) {
								pinDialog.dismiss();
							}
			 
			            }
			        });
			    }
			});
			pinDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
			pinDialog.show();
		}
	}
	
	@Override
	protected void initActionBar() {
		// must call super method to instantiate action bar and set proper attributes
		super.initActionBar();
		
		// set the spinner titles array
      mSpinnerTitles = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.home_spinner_array)));
		
		// initialize the spinner array adapter
		mSpinnerAdapter = ArrayAdapter.createFromResource(this, R.array.home_spinner_array, R.layout.spinner_list_item);
		
		// set the actionbar callback to this (implements OnNavigationListener)
		mActionBar.setListNavigationCallbacks(mSpinnerAdapter, this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.home, menu);
      return true;
	}

	@Override
	public boolean onNavigationItemSelected(int position, long itemId) {
		Resources res = getResources();
		Fragment newFragment = null;
		
	    // Create the correct fragment
		if (res.getString(R.string.my_tasks).equals(mSpinnerTitles.get(position))) {
			newFragment = new TaskFragment();
		}
		else if (res.getString(R.string.active_workflows).equals(mSpinnerTitles.get(position))) {
			newFragment = new ActiveWorkflowsFragment();
		}
		else {
			Toast.makeText(this, "Spinner navigation problem", Toast.LENGTH_SHORT).show();
			return false;
		}
	    FragmentTransaction ft = mFragmentManager.beginTransaction();
	    // Replace whatever is in the fragment container with this fragment
	    //  and give the fragment a tag name equal to the string at the position selected
	    ft.replace(R.id.fragment_container, newFragment, mSpinnerTitles.get(position));
	    // Apply changes
	    ft.commit();
	    return true;
	}

	@Override
	public void onActiveWorkflowSelect(ActiveWorkflow workflow) {
		selectedWorkflow = workflow;
		FragmentTransaction ft = mFragmentManager.beginTransaction();
		ft.replace(R.id.fragment_container, new DetailedWorkflowFragment()).addToBackStack(null);
		ft.commit();
	}
	
	@Override
	public void onBackPressed() {
		if (mFragmentManager.getBackStackEntryCount() > 0)
			mFragmentManager.popBackStack();
		else
			super.onBackPressed();
	}
	
	public void displayDialog(String message) {
	   if (message != null && message.length() > 0) {
	      dialog.setMessage(message);
	   }
	   dialog.show();
	   dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
	}
	
	private void createDialog() {
	      AlertDialog.Builder builder = new AlertDialog.Builder(this);

	      builder
	            .setMessage(res.getString(R.string.no_comment))
	            .setTitle(res.getString(R.string.task_comment))
	            .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
	                  public void onClick(DialogInterface dialog, int id) {
	                  }
	               });

	      dialog = builder.create();
	}
}


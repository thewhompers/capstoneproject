package com.veevavault.mobile.activity;

import org.androidannotations.annotations.EActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.MenuItem;

import com.veevavault.mobile.R;
import com.veevavault.mobile.fragment.DocumentListFragment.OnListItemSelect;
import com.veevavault.mobile.fragment.LibraryListFragment;
import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.Document;

@EActivity
public class SearchResultsActivity extends FragmentActivity implements OnListItemSelect {

	protected ActionBar actionBar;
	protected Resources res;
	private int viewState;
	private Menu optionsMenu;
	public static final int LISTVIEW = 0;
	public static final int GRIDVIEW = 1;
	private LibraryListFragment frag;
	private FragmentManager fManager;
	private SharedPreferences viewPreference;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Bundle args = getIntent().getExtras();
		setTitle(getTitle() + ": " + args.getString("search"));
		setContentView(R.layout.activity_search_results);
		loadSavedPreferences();
		initActionBar();

		fManager = getSupportFragmentManager();
		FragmentTransaction ft = fManager.beginTransaction();

		frag = new LibraryListFragment();  
		args.putString("title", "");
		frag.setArguments(args);

		ft.replace(R.id.search_results_fragment_container, frag);
		//ft.add(R.id.search_results_fragment_container, frag);      
		ft.commit();
	}

	@Override
	public void onListItemSelect(Bindable bindable) {
		Document doc = (Document)bindable;
		String typeOfDoc = doc.getPropertyValue("binder__v");

		if (typeOfDoc.equals("true")) {
			//Open a binder activity         
			Intent intent = new Intent(this, BinderActivity_.class);
			intent.putExtra("id", doc.getId().toString());
			intent.putExtra("binderName", ((Document)bindable).getPropertyValue("name__v"));
			intent.putExtra("view_Value", viewState);
			startActivity(intent);
		}
		else {
			Intent intent = new Intent(this, DocumentPropertiesActivity_.class);
			intent.putExtra("id", doc.getId().toString());
			startActivity(intent);
		}
	} 

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		menu.findItem(R.id.action_switch_view).setVisible(true);
		if (viewState == LISTVIEW) {
			menu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_grid);
		}
		else {
			menu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_list);
		}
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);

		switch (item.getItemId()) {
		case R.id.action_switch_view:
			if (viewState == LISTVIEW) {
				item.setIcon(R.drawable.ic_action_view_as_list);
				viewState = GRIDVIEW;            
			} else if (viewState == GRIDVIEW) {
				item.setIcon(R.drawable.ic_action_view_as_grid);
				viewState = LISTVIEW;            
			}
			savePreferences("view_Value", viewState);
			frag.setViewType(viewState);
			return true;
		case R.id.refresh:
			frag.refresh();
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		savePreferences("view_Value", viewState);
	}

	@Override
	protected void onResume() {
		super.onResume();
		loadSavedPreferences();
		if (frag != null) {
			frag.setViewType(viewState);
			if (optionsMenu != null) {
				if (viewState == LISTVIEW) {
					optionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_grid);
				}
				else {
					optionsMenu.findItem(R.id.action_switch_view).setIcon(R.drawable.ic_action_view_as_list);
				}
			}
		}
	}

	private void loadSavedPreferences() {
		this.viewPreference = PreferenceManager.getDefaultSharedPreferences(this);
		viewState = viewPreference.getInt("view_Value", LISTVIEW);
	}

	private void savePreferences(String key, int value) {
		SharedPreferences sharedPreferences = PreferenceManager
				.getDefaultSharedPreferences(this);
		Editor editor = sharedPreferences.edit();
		editor.putInt(key, value);
		editor.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		optionsMenu = menu;
		getMenuInflater().inflate(R.menu.library_activity_menu, menu);
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public void onBackPressed() {
		if (fManager.getBackStackEntryCount() > 0) {
			fManager.popBackStack();
		} else
			super.onBackPressed();
	}

	protected void initActionBar() {
		actionBar = getActionBar();
		//actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
	}

}

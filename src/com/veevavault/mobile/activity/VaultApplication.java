package com.veevavault.mobile.activity;

import lombok.Getter;
import lombok.Setter;
import android.app.Application;
import android.widget.Toast;
import com.veevavault.mobile.dao.request.SaveOfflineItemsToDatabaseRequest;

public class VaultApplication extends Application {
    private static VaultApplication singleton;
    private boolean waitingForSaveResponse;
    @Setter
    @Getter
    private static int previousDownloadChacheHash;
    @Getter
    @Setter
    private static int previousUserHash;
    @Getter
    @Setter
    private static int previousEndpointsHash;
    @Setter
    @Getter
    private static boolean offline;

    public static VaultApplication getInstance() {
        return singleton;
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
    
    public void cleanupBeforeClosing() {
       if (waitingForSaveResponse) {
          Toast.makeText(this, "DEBUG: Still waiting for database response", Toast.LENGTH_SHORT).show();
          return;
       }
       
       SaveOfflineItemsToDatabaseRequest request = new SaveOfflineItemsToDatabaseRequest();
       request.setContext(this);
       request.execute();
    }
}

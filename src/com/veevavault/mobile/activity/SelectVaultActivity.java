package com.veevavault.mobile.activity;

import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.adapter.VaultArrayAdapter;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.request.LoadDownloadedDocumentsRequest;
import com.veevavault.mobile.dao.request.LoadDownloadedDocumentsRequest.Listener;
import com.veevavault.mobile.dao.request.SelectVaultRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.model.VaultEndpoint;


public class SelectVaultActivity extends Activity implements SelectVaultRequest.Listener, AuthenticateUserRequest.Listener, LoadDownloadedDocumentsRequest.Listener {

	VaultArrayAdapter vaultAdapter;
	ArrayList<VaultEndpoint> vaultList;
	ListView listView;
	protected Intent viewHomeIntent;
	protected Context context;
	
	private String username;
	private String vaultID;
	private String password;
	private boolean isOverflow;
	private boolean isReAuth = false;
	private boolean isButtonClicked;
	private SharedPreferences saveVaultEndpoint;
	private VaultEndpoint vaultEndpoint;
	protected SelectVaultRequest selectVault;
	private AlertDialog loginDialog;
	
	public static final String MY_APP_PREFS = "myAppPrefs";
	private static final String VAULTS = "vaults";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.context = this;
		selectVault = new SelectVaultRequest();
		saveVaultEndpoint = context.getSharedPreferences(MY_APP_PREFS, 0);
		this.vaultList = getIntent().getParcelableArrayListExtra(VAULTS);
		
		Bundle extras = getIntent().getExtras();
		if (extras != null) {
		    username = extras.getString("username");
		    password = extras.getString("password");
		    isOverflow = extras.getBoolean("overflow");
		    Log.d(null, "username is: " + username);
		}
		if (isOverflow)
		{
			Editor prefsEditor = saveVaultEndpoint.edit();
			prefsEditor.remove(username);
			prefsEditor.commit();
		}
		loadSavedPreference();
		
		if (vaultID.equals("")) {
			setContentView(R.layout.activity_select_vault);
			vaultAdapter = new VaultArrayAdapter(this, vaultList);
	
			listView = (ListView)findViewById(R.id.vault_listView);
			listView.setAdapter(vaultAdapter);
			listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View v, int position,
						long id) {
					vaultEndpoint = (VaultEndpoint) listView
							.getItemAtPosition(position);
					if (isOverflow) {
						promptForReLogin();
					}
					else {
						AuthenticateUserRequest loginRequest = new AuthenticateUserRequest();
						loginRequest.setUsername(username);
						loginRequest.setPassword(password);
						loginRequest.setCallback(SelectVaultActivity.this);
						loginRequest.setContext(context);
						loginRequest.setDomain(vaultEndpoint);
						loginRequest.execute();
					}
				}
			});
		}
		else
		{
			for (int i = 0; i < vaultList.size(); i++) {
				if (vaultID.equals(vaultList.get(i).getVaultID())) {
					vaultEndpoint = vaultList.get(i);
					break;
				}
			}
			Log.d(null, "vault id reloaded: " + vaultID);
			AuthenticateUserRequest loginRequest = new AuthenticateUserRequest();
			loginRequest.setUsername(username);
			loginRequest.setPassword(password);
			loginRequest.setCallback(SelectVaultActivity.this);
			loginRequest.setContext(context);
			loginRequest.setDomain(vaultEndpoint);
			loginRequest.execute();
		}
	}
	
	private void promptForReLogin()
	{
		final EditText usernameEditText = new EditText(SelectVaultActivity.this);
		usernameEditText.setHint(getResources().getString(R.string.username_text));
		usernameEditText.setText(username);
		usernameEditText.setInputType(InputType.TYPE_CLASS_TEXT);
		usernameEditText.setImeOptions(EditorInfo.IME_ACTION_NEXT);
		
		final EditText passwordEditText = new EditText(SelectVaultActivity.this);
		passwordEditText.setHint(getResources().getString(R.string.password_text));
		passwordEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
		passwordEditText.setImeOptions(EditorInfo.IME_ACTION_DONE);
		passwordEditText.requestFocus();
		
		LinearLayout linearLayout = new LinearLayout(this);
        linearLayout.setOrientation(LinearLayout.VERTICAL);
        linearLayout.addView(usernameEditText);
        linearLayout.addView(passwordEditText);
		
    	loginDialog = new AlertDialog.Builder(SelectVaultActivity.this)
    		.setView(linearLayout)
	        .setTitle("Re-Authenticate")
	        .setMessage(getResources().getString(R.string.login_prompt))
	        .setPositiveButton(getResources().getString(R.string.login), null)
	        .create();
	 
		loginDialog.setOnShowListener(new DialogInterface.OnShowListener() {
		 
		    @Override
		    public void onShow(DialogInterface dialog) {
		 
		        Button b = loginDialog.getButton(AlertDialog.BUTTON_POSITIVE);
		        b.setOnClickListener(new View.OnClickListener() {	 
		            @Override
		            public void onClick(View view) {
		            	if (!isButtonClicked)
		            	{
		            		isReAuth = true;
			            	String passwordStr = passwordEditText.getText().toString();
			            	AuthenticateUserRequest loginRequest = new AuthenticateUserRequest();
							loginRequest.setUsername(username);
							loginRequest.setPassword(passwordStr);
							Log.d(null, "new vault endpoint switch to: " + vaultEndpoint.getName());
							loginRequest.setDomain(vaultEndpoint);
							loginRequest.setCallback(SelectVaultActivity.this);
							loginRequest.setContext(context);
							loginRequest.execute();
		            	}
		            	isButtonClicked = true;
		            }
		        });
		    }
		});
		loginDialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
		loginDialog.show();
	}
	
	private void loadSavedPreference() {
		vaultID = saveVaultEndpoint.getString(username, "");
		Log.d(null, "loaded vault id: " + vaultID);
	}
	
	private void saveVaultEndpointPreference(VaultEndpoint vEndpoint) {
		Editor prefsEditor = saveVaultEndpoint.edit();
		Log.d(null, "saved vault id: " + vEndpoint.getVaultID());
		prefsEditor.putString(username, vEndpoint.getVaultID());
		prefsEditor.commit();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.select_vault, menu);
		return true;
	}

   @Override
   public void onSelectVaultResponse() {
      loadDownloadedDocumentsIntoCache();
      
      viewHomeIntent = new Intent(getApplicationContext(),
            HomeActivity_.class)
            .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
                  | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      startActivity(viewHomeIntent);
   }
   
   private void loadDownloadedDocumentsIntoCache() {
      LoadDownloadedDocumentsRequest request = new LoadDownloadedDocumentsRequest();
      request.setContext(VaultApplication.getInstance());
      request.setCallback(this);
      request.execute();
   }

   @Override
   public void onAuthenticateResponse(AuthenticateUserResponse response) {
	   isButtonClicked = false;
	   Log.d(null, "Response Status: " + response.getResponseStatus());
	   if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
		    if (isReAuth)
		    {
		    	loginDialog.dismiss();
		    	isReAuth = false;
		    }
			saveVaultEndpointPreference(vaultEndpoint);
			selectVault.setVaultEndpoint(vaultEndpoint);
         saveVaultEndpointPreference(vaultEndpoint);
			selectVault.setCallback(SelectVaultActivity.this);
			selectVault.setContext(SelectVaultActivity.this);
			selectVault.execute();
	   }
	   else {
         Toast.makeText(getApplicationContext(), response.getResponseMessage(), Toast.LENGTH_LONG).show();
	   }
   }

   @Override
   public void onLoadDocumentsResponse() {
      // TODO Auto-generated method stub
      
   }
}

package com.veevavault.mobile.activity;

import java.io.File;

import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.webkit.MimeTypeMap;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.DocumentDAO;
import com.veevavault.mobile.model.Document;

public class DownloadDocument{
	private String url;
	private String docName;
	private String docFormat;
	private String sessionId;
	private long enqueue;
   private DownloadManager dm;
   private final Context actContext;
   private File downloadedFile;
   Document document;
    
	public DownloadDocument(Document document, String url, Context context, String name, String format, String sessionId)
	{
		this.url = url;
		this.actContext = context;
		this.docName = name;
		this.docFormat = format.substring(format.lastIndexOf('/') + 1);
		Log.d(null, "download doc format: " + docFormat);
		this.sessionId = sessionId;
		this.document = document;
	}
	
	public File setUpBroadcastReciever()
	{
		BroadcastReceiver receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(
                            DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    Query query = new Query();
                    query.setFilterById(enqueue);
                    Cursor c = dm.query(query);
                    if (c.moveToFirst()) {
                        int columnIndex = c
                                .getColumnIndex(DownloadManager.COLUMN_STATUS);
                        if (DownloadManager.STATUS_SUCCESSFUL == c
                                .getInt(columnIndex)) {
                        	
                           document.setFilePath(getNewDocumentFilePath());
                           DocumentDAO.saveDocumentToDownloads(document);
                        	launchApplicationIntent();
                            
                        	actContext.unregisterReceiver(this);
                        }
                        else if (DownloadManager.ERROR_INSUFFICIENT_SPACE == c.getInt(columnIndex)) {
                           Toast.makeText(actContext, "Error downloading Document. Insufficient space.", Toast.LENGTH_SHORT).show(); 
                        }
                        else {
                           Toast.makeText(actContext, "Error downloading Document. Insufficient space.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            }
        };
		actContext.registerReceiver(receiver, new IntentFilter(
                DownloadManager.ACTION_DOWNLOAD_COMPLETE));
        return downloadedFile;
	}
	
	public void launchApplicationIntent()
	{
		downloadedFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/Vault_Mobile" + "/" + docName + "." + docFormat);
    	Log.d(null, "File path: " + downloadedFile);
    	MimeTypeMap mime = MimeTypeMap.getSingleton();
    	String type = mime.getMimeTypeFromExtension(docFormat);
    	Log.d(null, "mime type: " + type);
    	
    	if (type != null)
    	{
        	Intent appIntent = new Intent(Intent.ACTION_VIEW);
        	appIntent.setDataAndType(Uri.parse(("file://" + downloadedFile.getAbsolutePath())), type);
        	appIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
        	actContext.startActivity(appIntent);
    	}
    	else
    	{
    		Toast.makeText(actContext, actContext.getResources().getString(R.string.download_doc_mime_error), Toast.LENGTH_SHORT).show();
    	}
	}
	
	public void downloadDocument()
	{
		File direct = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/Vault_Mobile");

        if (!direct.exists()) {
            direct.mkdirs();
        }
   
		dm = (DownloadManager) actContext.getSystemService(Context.DOWNLOAD_SERVICE);
        Request request = new Request(
                Uri.parse(url));
        request.addRequestHeader("Authorization", sessionId);
        request.setAllowedNetworkTypes(
                DownloadManager.Request.NETWORK_WIFI
                        | DownloadManager.Request.NETWORK_MOBILE)
                .setAllowedOverRoaming(false).setTitle("Downloading Document")
                .setDescription(document.getPropertyByName("name__v").toString())
                .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS + "/Vault_Mobile", docName + "." + docFormat);
        enqueue = dm.enqueue(request);
	}
	
	public String getNewDocumentFilePath()
	{
		return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/Vault_Mobile" + "/" + docName + "." + docFormat;
	}

}

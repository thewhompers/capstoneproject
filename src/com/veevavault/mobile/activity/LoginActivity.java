package com.veevavault.mobile.activity;

import java.util.ArrayList;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.rest.RestService;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.api.rest.VeevaRestClientv7;
import com.veevavault.mobile.dao.request.AuthenticateUserRequest;
import com.veevavault.mobile.dao.response.AuthenticateUserResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.model.NetworkUtil;
import com.veevavault.mobile.model.VaultEndpoint;

@EActivity(R.layout.activity_login)
public class LoginActivity extends Activity implements AuthenticateUserRequest.Listener {

	protected AuthenticateUserRequest loginRequest;
	protected VaultEndpoint vaultEndpoint;
	protected Intent selectVaultIntent;
	protected Context context;
	private String username = "";
	private String storedUsername = "";
	private String password = "";
	private boolean isRememberMeChecked;
	private boolean isButtonClicked;
	private boolean isOfflineMode;
	private WebView webViewForgetPassword;
	
	@ViewById(R.id.forgotPasswordLink_textview)
	protected TextView forgetPassword;
	@ViewById(R.id.username_edittext)
	protected EditText usernameEditText;
	@ViewById(R.id.password_edittext)
	protected EditText passwordEditText;
	@ViewById(R.id.remember_me_check)
	protected CheckBox rememberMe;
	
	@RestService
	VeevaRestClientv7 veevaClient;

	@ViewById(R.id.failedLogin_textview)
	protected TextView failedLogin;
	@ViewById(R.id.login_button)
	protected Button loginButton;
	
	//for remembering username
	private SharedPreferences sharedPreferencesUsername;
	
	private static final String MY_APP_PREFS = "myAppPrefs";
	private static final String STORED_USERNAME = "storedUsername";
	private static final String REMEMBER_ME_FLAG = "rememberMeFlag";
	
	private BroadcastReceiver mConnReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
        	int conn = NetworkUtil.getConnectivityStatus(context);
            if (conn == NetworkUtil.TYPE_WIFI || conn == NetworkUtil.TYPE_MOBILE ) {
	    		isOfflineMode = false;
                loginButton.setText(getResources().getString(R.string.login));
                passwordEditText.setHint(getResources().getString(R.string.password_text));
	        	passwordEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_PASSWORD);
	    		passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            } else if (conn == NetworkUtil.TYPE_NOT_CONNECTED) {
	    		isOfflineMode = true;
                loginButton.setText(getResources().getString(R.string.offline_mode));
                passwordEditText.setHint(getResources().getString(R.string.password_offline_text));
	        	passwordEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
	    		passwordEditText.setTransformationMethod(PasswordTransformationMethod.getInstance());
	    		
	    		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
				    @Override
				    public void onClick(DialogInterface dialog, int which) {
				        dialog.dismiss();
				    }
				};
	
				AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
				builder.setTitle(getResources().getString(R.string.no_internet));
				builder.setMessage(getResources().getString(R.string.use_offline_pin));
				builder.setNeutralButton(getResources().getString(R.string.ok), dialogClickListener);
				builder.show();
            }
        }
	};
   private boolean offlineTest;
	
	@Override
	protected void onPause() {
	    super.onPause();
	    unregisterReceiver(mConnReceiver);
	}

	@Override
	protected void onResume() {
	    super.onResume();
	    registerReceiver(mConnReceiver, 
	    		new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
	}
	
	@Click(R.id.offline_pres_button)
   protected void onOfflineTest() {
      if (!offlineTest) {
         offlineTest = true;
         isOfflineMode = true;
         loginButton.setText(getResources().getString(R.string.offline_mode));
         passwordEditText.setHint(getResources().getString(
               R.string.password_offline_text));
         passwordEditText.setInputType(InputType.TYPE_CLASS_NUMBER);
         passwordEditText.setTransformationMethod(PasswordTransformationMethod
               .getInstance());

         DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
               dialog.dismiss();
            }
         };

         AlertDialog.Builder builder = new AlertDialog.Builder(
               LoginActivity.this);
         builder.setTitle(getResources().getString(R.string.no_internet));
         builder.setMessage(getResources().getString(R.string.use_offline_pin));
         builder.setNeutralButton(getResources().getString(R.string.ok),
               dialogClickListener);
         builder.show();
      } else {
         offlineTest = false;
         isOfflineMode = false;
         loginButton.setText(getResources().getString(R.string.login));
         passwordEditText.setHint(getResources().getString(
               R.string.password_text));
         passwordEditText.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
               | InputType.TYPE_TEXT_VARIATION_PASSWORD);
         passwordEditText.setTransformationMethod(PasswordTransformationMethod
               .getInstance());
      }
	}

	private void debug_SetUsernamePassword() {
		usernameEditText.setText("cwalsvick@calpoly-capstone.com");
		passwordEditText.setText("easyPass2");
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		Context myContext = this.getApplicationContext();
		sharedPreferencesUsername = myContext.getSharedPreferences(MY_APP_PREFS, 0);
		isButtonClicked = false;
		isOfflineMode = false;
	}
	
	private void loadSavedPreference() {
		storedUsername = sharedPreferencesUsername.getString(STORED_USERNAME, "");
		isRememberMeChecked = sharedPreferencesUsername.getBoolean(REMEMBER_ME_FLAG, false);
		usernameEditText.setText(storedUsername);
	}
	
	private void savePreferences(String key, String value) {
	    Editor editor = sharedPreferencesUsername.edit();
	    editor.putString(key, value);
	    editor.commit();
	}
	
	private void saveBoolPreferences(String key, boolean value) {
		Editor editor = sharedPreferencesUsername.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}

	
	@AfterViews
    void afterViews() {
		loadSavedPreference();
		debug_SetUsernamePassword();
		
		if(isRememberMeChecked) {
			rememberMe.setChecked(true);
		}
		
		forgetPassword.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
            	if (!isOfflineMode) {
            		Intent intent = new Intent(getApplicationContext(), WebViewForgotPasswordActivity.class);
            		startActivity(intent);
            	}
            	else {
            		DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        			    @Override
        			    public void onClick(DialogInterface dialog, int which) {
        			        dialog.dismiss();
        			    }
        			};

        			AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        			builder.setTitle(getResources().getString(R.string.no_internet));
        			builder.setMessage(getResources().getString(R.string.cant_reset_password));
        			builder.setNeutralButton(getResources().getString(R.string.ok), dialogClickListener);
        			builder.show();
            	}
            }
        });
    }
	
	@Click(R.id.login_button)
	protected void onLoginButtonClick() {
		failedLogin.setVisibility(View.GONE);
		Log.d(null, "am I offline? " + isOfflineMode);
		if (!isButtonClicked) {
			username = usernameEditText.getText().toString();
			password = passwordEditText.getText().toString();
			failedLogin.setVisibility(View.INVISIBLE);
			String pin = sharedPreferencesUsername.getString(username + "pin", "");
	
			if (isOfflineMode) {
				isButtonClicked = false;
				if (pin.equals("")) {
					DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
        			    @Override
        			    public void onClick(DialogInterface dialog, int which) {
        			        dialog.dismiss();
        			    }
        			};

        			AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        			builder.setTitle(getResources().getString(R.string.no_internet));
        			builder.setMessage(getResources().getString(R.string.offline_access_no_pin));
        			builder.setNeutralButton(getResources().getString(R.string.ok), dialogClickListener);
        			builder.show();
				}
				else {
					Log.d(null, "pin is: " + pin);
					if (password.equals(pin)) {
						if (rememberMe.isChecked()) {
							savePreferences(STORED_USERNAME, usernameEditText.getText()
									.toString());
							saveBoolPreferences(REMEMBER_ME_FLAG, true);
						} else {
							savePreferences(STORED_USERNAME, "");
							saveBoolPreferences(REMEMBER_ME_FLAG, false);
						}
						
						VaultApplication.setOffline(true);
						
						Intent i = new Intent(getApplicationContext(), OfflineActivity_.class);
						i.putExtra("userName", usernameEditText.getText().toString());
						i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
	                     | Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
			         startActivity(i);
					}
					else {
						failedLogin.setVisibility(View.VISIBLE);
					}
				}
			}
			else {
				isButtonClicked = true;
		        this.loginRequest = new AuthenticateUserRequest();
				loginRequest.setRestClient(veevaClient);
				loginRequest.setUsername(username);
				loginRequest.setPassword(password);
				loginRequest.setCallback(this);
				loginRequest.setContext(this);
				loginRequest.execute();
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.login, menu);
		return true;
	}

	@Override
	public void onAuthenticateResponse(AuthenticateUserResponse response) {
		isButtonClicked = false;
		if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
			if (rememberMe.isChecked()) {
				savePreferences(STORED_USERNAME, usernameEditText.getText()
						.toString());
				saveBoolPreferences(REMEMBER_ME_FLAG, true);
			} else {
				savePreferences(STORED_USERNAME, "");
				saveBoolPreferences(REMEMBER_ME_FLAG, false);
			}			

			VaultApplication.setOffline(false);
			
			// LAUNCH INTENT FOR SELECTING VAULT
			selectVaultIntent = new Intent(getApplicationContext(),
					SelectVaultActivity.class)
					.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
					| Intent.FLAG_ACTIVITY_NEW_TASK);
			selectVaultIntent.putParcelableArrayListExtra("vaults",
					(ArrayList<VaultEndpoint>) response.getVaults());
			selectVaultIntent.putExtra("username", username);
			selectVaultIntent.putExtra("password", password);
			selectVaultIntent.putExtra("overflow", false);
			startActivity(selectVaultIntent);
		} else {
			failedLogin.setText(response.getResponseMessage());
			failedLogin.setVisibility(View.VISIBLE);
		}
	}
}

package com.veevavault.mobile.activity;

import java.util.HashMap;
import java.util.List;

import org.androidannotations.annotations.EActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.dao.request.LoadDownloadedDocumentsRequest;
import com.veevavault.mobile.dao.request.LoadUserRequest;
import com.veevavault.mobile.dao.request.LoadVaultEndpointsRequest;
import com.veevavault.mobile.dao.response.LoadUserResponse;
import com.veevavault.mobile.dao.response.LoadVaultEndpointsResponse;
import com.veevavault.mobile.fragment.DocumentListFragment.OnListItemSelect;
import com.veevavault.mobile.fragment.LibraryListFragment;
import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.VaultEndpoint;

@EActivity()
public class OfflineActivity extends TopLevelActivity implements OnListItemSelect, LoadUserRequest.Listener, LoadVaultEndpointsRequest.Listener, LoadDownloadedDocumentsRequest.Listener {
   
   private static final int DOWNLOADS = 0;
   private List<VaultEndpoint> endpoints;
   private HashMap<String, VaultEndpoint> endpointsByName;
   private FragmentManager manager;
   private Menu menu;
   private boolean menuInit;
   
   private static int VAULT_GROUP = 99;
   
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      
      getUser();
      
      setTitle("Downloads");
        
      manager = getSupportFragmentManager();
   }
   
   private void getUser() {
      LoadUserRequest request = new LoadUserRequest();
      request.setContext(this);
      request.setCallback(this);
      request.setUsername(getIntent().getExtras().getString("userName"));
      request.execute();
   }
   
   @Override
   public void invalidateOptionsMenu() {
      menuInit = false;
      super.invalidateOptionsMenu();
   }
   
   @Override
   public boolean onPrepareOptionsMenu(Menu menu) {
      MenuItem item = menu.findItem(R.id.menu_filter);
      
      if (this.endpoints != null && !menuInit) {
         menuInit = true;
         for (int i = 0; i < endpoints.size(); i++) {
            item.getSubMenu().add(VAULT_GROUP, VAULT_GROUP + i, i, endpoints.get(i).getName());
         }
         
         item.setTitle(BaseDAO.getCurrentVaultEndpoint().getName());
      }
      return true;
   }

   @Override
   public boolean onCreateOptionsMenu(Menu menu) {
      getMenuInflater().inflate(R.menu.offline, menu);
      this.menu = menu;
      return true;
   }
   
   @Override
   public boolean onOptionsItemSelected(MenuItem item) {
      
      if (mDrawerToggle.onOptionsItemSelected(item)) {
         return true;
       }
            
      if (item.getItemId() == R.id.action_login) {
         Intent intent = new Intent(OfflineActivity.this, LoginActivity_.class);
         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP | 
               Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
         startActivity(intent);
         return true;
      }
      else if (item.getGroupId() == VAULT_GROUP) {
         BaseDAO.setCurrentVaultEndpoint(endpointsByName.get(item.getTitle()));
         loadDownloadedDocuments();
         
         menu.findItem(R.id.menu_filter).setTitle(item.getTitle());
         return true;
      }
      else {
      }
      return true;
   }
   

   public void initNavDrawer() {
      String[] drawerTitles = { getResources().getString(R.string.downloaded_documents) };
      mDrawerTitles = drawerTitles;
      mDrawerLayout = (DrawerLayout) findViewById(R.id.home_drawer_layout);
      mDrawerList = (ListView) findViewById(R.id.top_drawer_list);

      // Set the adapter for the nav drawer list view
      mDrawerList.setAdapter(new ArrayAdapter<String>(this,
            R.layout.drawer_list_item, mDrawerTitles));

      // Set the list's click listener
      mDrawerList.setOnItemClickListener(new OfflineDrawerItemClickListener());
   }
   
   @Override
   protected void initActionBar() {
      mActionBar = getActionBar();
      mDrawerTitle = getResources().getString(R.string.drawer_title);
      mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
                ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
           /*     getActionBar().setTitle(mTitle);
                getActionBar().setDisplayShowTitleEnabled(false);*/
                invalidateOptionsMenu();
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
               /* getActionBar().setTitle(mDrawerTitle);
                getActionBar().setDisplayShowTitleEnabled(true);*/
                invalidateOptionsMenu();
            }
        };
        
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        
        // Make action bar icon clickable and add the drawer indicator
        getActionBar().setDisplayHomeAsUpEnabled(true);
       // getActionBar().setHomeButtonEnabled(true);
   }


   @Override
   public boolean onNavigationItemSelected(int arg0, long arg1) {
      // TODO Auto-generated method stub
      return false;
   }
   
   class OfflineDrawerItemClickListener implements
      ListView.OnItemClickListener {

      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
         Intent intent = null;
         
         switch(position) {      
         case DOWNLOADS:
            intent = new Intent(OfflineActivity.this, OfflineActivity_.class);
            break;
         default:
            Toast.makeText(getBaseContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();
            return;
         }
         
         intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
         mDrawerList.setItemChecked(position, true);
         mDrawerLayout.closeDrawer(mDrawerList);
         
         startActivity(intent);
      }
   }

   @Override
   public void onListItemSelect(Bindable doc) {
      Intent i = new Intent(this, DocumentPropertiesActivity_.class);
      i.putExtra("id", doc.getId().toString());
      startActivity(i);
   }

   @Override
   public void onLoadUserResponse(LoadUserResponse response) {
      BaseDAO.setCurrentUser(response.getUser());
      
      getVaultEndpoints();
   }

   private void getVaultEndpoints() {
      LoadVaultEndpointsRequest request = new LoadVaultEndpointsRequest();
      request.setContext(this);
      request.setCallback(this);
      request.setUser(BaseDAO.getCurrentUser());
      request.execute();
   }

   @Override
   public void onLoadEnpointsResponse(LoadVaultEndpointsResponse response) {
      this.endpoints = response.getEndpoints();
      this.endpointsByName = new HashMap<String, VaultEndpoint>();
      
      for (VaultEndpoint endpoint : this.endpoints) {
         endpointsByName.put(endpoint.getName(), endpoint);
      }
      
      setCurrentEndpoint();
      invalidateOptionsMenu();
      
      loadDownloadedDocuments();
   }

   private void setCurrentEndpoint() {
      SharedPreferences saveVaultEndpoint = this.getSharedPreferences(SelectVaultActivity.MY_APP_PREFS, 0);
      String defaultVaultId = saveVaultEndpoint.getString(
            getIntent().getExtras().getString("userName"), "");
      
      for (VaultEndpoint endpoint : this.endpoints) {
         if (endpoint.getVaultID().equals(defaultVaultId)) {
            BaseDAO.setCurrentVaultEndpoint(endpoint);
            return;
         }
      }
   }

   private void loadDownloadedDocuments() {
      LoadDownloadedDocumentsRequest request = new LoadDownloadedDocumentsRequest();
      
      request.setContext(this);
      request.setOffline(true);
      request.setCallback(this);
      request.execute();
   }
   
   @Override
   public void onLoadDocumentsResponse() {
      FragmentTransaction ft = manager.beginTransaction();
      
      LibraryListFragment frag = new LibraryListFragment();  
      Bundle args = new Bundle();
      args.putString("title", "");
      args.putBoolean("onDevice", true);
      frag.setArguments(args);
      
      
      ft.replace(R.id.fragment_container, frag);      
      ft.commit();
   }
}

package com.veevavault.mobile.database;

import com.veevavault.mobile.database.tables.*;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;

/**
 * Class that hooks up to the CreatureContentProvider for initialization and
 * maintenance.
 */
public class DatabaseHelper extends android.database.sqlite.SQLiteOpenHelper {

	/** The name of the database. */
	public static final String DATABASE_NAME = "vaultmobile.db";
	
	/** The starting database version. */
	public static final int DATABASE_VERSION = 1;
	
	/**
	 * Create a helper object to create, open, and/or manage a database.
	 * 
	 * @param context
	 * 					The application context.
	 * @param name
	 * 					The name of the database.
	 * @param factory
	 * 					Factory used to create a cursor. Set to null for default behavior.
	 * @param version
	 * 					The starting database version.
	 */
	public DatabaseHelper(Context context, String name,
		CursorFactory factory, int version) {
		super(context, name, null, version);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		DocumentTable.onCreate(db);
		DocumentXPropertyTable.onCreate(db);
		PropertyTable.onCreate(db);
		RequestTable.onCreate(db);
		RequestXDocumentTable.onCreate(db);
		UserTable.onCreate(db);
		VaultEndpointTable.onCreate(db);
		UserXEndpointTable.onCreate(db);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		DocumentTable.onUpgrade(db, oldVersion, newVersion);
		DocumentXPropertyTable.onUpgrade(db, oldVersion, newVersion);
		PropertyTable.onUpgrade(db, oldVersion, newVersion);
		RequestTable.onUpgrade(db, oldVersion, newVersion);
		RequestXDocumentTable.onUpgrade(db, oldVersion, newVersion);
      UserTable.onUpgrade(db, oldVersion, newVersion);
      VaultEndpointTable.onUpgrade(db, oldVersion, newVersion);
      UserXEndpointTable.onUpgrade(db, oldVersion, newVersion);
	}
}
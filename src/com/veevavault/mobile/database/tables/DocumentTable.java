package com.veevavault.mobile.database.tables;

import java.util.Arrays;
import java.util.HashSet;

import android.database.sqlite.SQLiteDatabase;

public class DocumentTable {
	
	/** Document table in the database. */
	public static final String TABLE_NAME = "document_table";
	
	/** Column names and there respective column id's **/
	public static final String DOCUMENT_ID = "id";
	public static final int DOCUMENT_ID_COL = 0;
	
	public static final String DOCUMENT_VEEVA_ID = "veevaId";
	public static final int DOCUMENT_VEEVA_ID_COL = DOCUMENT_ID_COL + 1;
	
	public static final String DOCUMENT_VAULT_ID = "vaultId";
	public static final int DOCUMENT_VAULT_ID_COL = DOCUMENT_ID_COL + 2;
	
	public static final String DOCUMENT_FILE_PATH = "filePath";
	public static final int DOCUMENT_FILE_PATH_COL = DOCUMENT_ID_COL + 3;
	
	public static final String DOCUMENT_THUMBNAIL = "thumbnail";
	public static final int DOCUMENT_THUMBNAIL_COL = DOCUMENT_ID_COL + 4;
	
	public static final String DOCUMENT_CACHED_TIME = "cachedTime";
	public static final int DOCUMENT_CACHED_TIME_COL = DOCUMENT_ID_COL + 5;

	public static final String[] ALL_COLUMNS = { 
	   DocumentTable.DOCUMENT_ID, 
	   DocumentTable.DOCUMENT_VEEVA_ID,
	   DocumentTable.DOCUMENT_VAULT_ID, 
	   DocumentTable.DOCUMENT_FILE_PATH,
		DocumentTable.DOCUMENT_THUMBNAIL, 
		DocumentTable.DOCUMENT_CACHED_TIME };
	
	/** SQLite database creation statement. Auto-increments IDs of inserted documents.
	 * Document IDs are set after insertion into the database. */
	public static final String TABLE_CREATE = "create table " + TABLE_NAME + " (" + 
			DOCUMENT_ID 			+ " integer primary key autoincrement, " + 
			DOCUMENT_VEEVA_ID		+ " text not null," +
			DOCUMENT_VAULT_ID		+ " text not null," +
			DOCUMENT_FILE_PATH		+ " text default null, " +
			DOCUMENT_THUMBNAIL 		+ " blob default null, " +
			DOCUMENT_CACHED_TIME 	+ " text default null);";
	
	/** SQLite database table removal statement. Only used if upgrading database. */
	public static final String TABLE_DROP = "drop table if exists " + TABLE_NAME;
	
	/**
	 * Initializes the database.
	 * 
	 * @param database
	 * 				The database to initialize.	
	 */
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	
	/**
	 * Upgrades the database to a new version.
	 * 
	 * @param database
	 * 					The database to upgrade.
	 * @param oldVersion
	 * 					The old version of the database.
	 * @param newVersion
	 * 					The new version of the database.
	 */
	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL(TABLE_DROP);
		onCreate(database);
	}
	
	/**
	 * Verifies the correct set of columns to return data from when performing a query.
	 * 
	 * @param projection
	 * 						The set of columns about to be queried.
	 */
	public static void checkColumns(String[] projection) {
		String[] available = ALL_COLUMNS;
		
		if(projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
			
			if(!availableColumns.containsAll(requestedColumns))	{
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}

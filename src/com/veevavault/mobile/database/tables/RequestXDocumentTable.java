package com.veevavault.mobile.database.tables;

import java.util.Arrays;
import java.util.HashSet;

import android.database.sqlite.SQLiteDatabase;

public class RequestXDocumentTable {
	
	/** RequestXDocument table in the database. */
	public static final String TABLE_NAME = "request_x_document_table";
	
	/** Column names and there respective column id's **/
	public static final String REQUEST_ID = "requestId";
	public static final int REQUEST_ID_COL = 0;
	
	public static final String DOCUMENT_ID = "documentId";
	public static final int DOCUMENT_ID_COL = REQUEST_ID_COL + 1;

	public static final String[] ALL_COLUMNS = { RequestXDocumentTable.REQUEST_ID, RequestXDocumentTable.DOCUMENT_ID };

	
	/** SQLite database creation statement. **/
	public static final String TABLE_CREATE = "create table " + TABLE_NAME + " (" + 
			REQUEST_ID 	+ " integer not null, " + 
			DOCUMENT_ID	+ " integer not null, " +
			"FOREIGN KEY(" + REQUEST_ID + ") REFERENCES " + RequestTable.TABLE_NAME + "(" + RequestTable.REQUEST_ID +"), " +
			"FOREIGN KEY(" + DOCUMENT_ID + ") REFERENCES " + DocumentTable.TABLE_NAME + "(" + DocumentTable.DOCUMENT_ID +"));";
	
	/** SQLite database table removal statement. Only used if upgrading database. */
	public static final String TABLE_DROP = "drop table if exists " + TABLE_NAME;
	
	/**
	 * Initializes the database.
	 * 
	 * @param database
	 * 				The database to initialize.	
	 */
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	
	/**
	 * Upgrades the database to a new version.
	 * 
	 * @param database
	 * 					The database to upgrade.
	 * @param oldVersion
	 * 					The old version of the database.
	 * @param newVersion
	 * 					The new version of the database.
	 */
	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL(TABLE_DROP);
		onCreate(database);
	}
	
	/**
	 * Verifies the correct set of columns to return data from when performing a query.
	 * 
	 * @param projection
	 * 						The set of columns about to be queried.
	 */
	public static void checkColumns(String[] projection) {
		String[] available = ALL_COLUMNS;
		
		if(projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
			
			if(!availableColumns.containsAll(requestedColumns))	{
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}

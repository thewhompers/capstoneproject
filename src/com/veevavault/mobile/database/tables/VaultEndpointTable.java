package com.veevavault.mobile.database.tables;

import java.util.Arrays;
import java.util.HashSet;

import android.database.sqlite.SQLiteDatabase;

public class VaultEndpointTable {
	
	/** Document table in the database. */
	public static final String TABLE_NAME = "vault_endpoint_table";
	
	/** Column names and there respective column id's **/
	public static final String VAULT_ID = "id";
	public static final int VAULT_ID_COL = 0;
	
	public static final String VAULT_NAME = "vaultName";
	public static final int VAULT_NAME_COL = VAULT_ID_COL + 1;
	
	public static final String VAULT_DNS_NAME = "dnsName";
	public static final int VAULT_DNS_NAME_COL = VAULT_ID_COL + 2;

	public static final String[] ALL_COLUMNS = { 
	   VaultEndpointTable.VAULT_ID, 
	   VaultEndpointTable.VAULT_NAME,
	   VaultEndpointTable.VAULT_DNS_NAME };
	
	/** SQLite database creation statement. Auto-increments IDs of inserted documents.
	 * Document IDs are set after insertion into the database. */
	public static final String TABLE_CREATE = "create table " + TABLE_NAME + " (" + 
	      VAULT_ID 			+ " text primary key, " + 
	      VAULT_NAME		   + " text not null," +
	      VAULT_DNS_NAME		+ " text not null);";
	
	/** SQLite database table removal statement. Only used if upgrading database. */
	public static final String TABLE_DROP = "drop table if exists " + TABLE_NAME;
	
	/**
	 * Initializes the database.
	 * 
	 * @param database
	 * 				The database to initialize.	
	 */
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	
	/**
	 * Upgrades the database to a new version.
	 * 
	 * @param database
	 * 					The database to upgrade.
	 * @param oldVersion
	 * 					The old version of the database.
	 * @param newVersion
	 * 					The new version of the database.
	 */
	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL(TABLE_DROP);
		onCreate(database);
	}
	
	/**
	 * Verifies the correct set of columns to return data from when performing a query.
	 * 
	 * @param projection
	 * 						The set of columns about to be queried.
	 */
	public static void checkColumns(String[] projection) {
		String[] available = ALL_COLUMNS;
		
		if(projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
			
			if(!availableColumns.containsAll(requestedColumns))	{
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}

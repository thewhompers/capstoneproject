package com.veevavault.mobile.database.tables;

import java.util.Arrays;
import java.util.HashSet;

import android.database.sqlite.SQLiteDatabase;

public class DocumentXPropertyTable {
	
	/** DocumentXProperty table in the database. */
	public static final String TABLE_NAME = "document_x_property_table";
	
	/** Column names and there respective column id's **/
	public static final String DOCUMENT_ID = "documentId";
	public static final int DOCUMENT_ID_COL = 0;
	
	public static final String PROPERTY_ID = "propertyId";
	public static final int PROPERTY_ID_COL = DOCUMENT_ID_COL + 1;
	
	public static final String VALUE = "value";
	public static final int VALUE_COL = DOCUMENT_ID_COL + 2;

	public static final String[] ALL_COLUMNS = { 
	   DocumentXPropertyTable.DOCUMENT_ID, 
	   DocumentXPropertyTable.PROPERTY_ID, 
	   DocumentXPropertyTable.VALUE };

	
	/** SQLite database creation statement. **/
	public static final String TABLE_CREATE = "create table " + TABLE_NAME + " (" + 
			DOCUMENT_ID 	+ " integer not null, " + 
			PROPERTY_ID		+ " integer not null, " +
			VALUE 			+ " text not null);";
	
	/** SQLite database table removal statement. Only used if upgrading database. */
	public static final String TABLE_DROP = "drop table if exists " + TABLE_NAME;
	
	/**
	 * Initializes the database.
	 * 
	 * @param database
	 * 				The database to initialize.	
	 */
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	
	/**
	 * Upgrades the database to a new version.
	 * 
	 * @param database
	 * 					The database to upgrade.
	 * @param oldVersion
	 * 					The old version of the database.
	 * @param newVersion
	 * 					The new version of the database.
	 */
	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL(TABLE_DROP);
		onCreate(database);
	}
	
	/**
	 * Verifies the correct set of columns to return data from when performing a query.
	 * 
	 * @param projection
	 * 						The set of columns about to be queried.
	 */
	public static void checkColumns(String[] projection) {
		String[] available = ALL_COLUMNS;
		
		if(projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
			
			if(!availableColumns.containsAll(requestedColumns))	{
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}

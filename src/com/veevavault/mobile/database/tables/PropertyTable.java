package com.veevavault.mobile.database.tables;

import java.util.Arrays;
import java.util.HashSet;

import android.database.sqlite.SQLiteDatabase;

public class PropertyTable {
	
	/** Property table in the database. */
	public static final String TABLE_NAME = "property_table";
	
	/** Column names and there respective column id's **/
	public static final String PROPERTY_ID = "id";
	public static final int PROPERTY_ID_COL = 0;
	
	public static final String PROPERTY_NAME = "name";
	public static final int PROPERTY_NAME_COL = PROPERTY_ID_COL + 1;
	
	public static final String PROPERTY_LABEL = "label";
	public static final int PROPERTY_LABEL_COL = PROPERTY_ID_COL + 2;
	
	public static final String PROPERTY_SECTION = "section";
	public static final int PROPERTY_SECTION_COL = PROPERTY_ID_COL + 3; 
	
   public static final String PROPERTY_SECTION_POSITION = "sectionPosition";
   public static final int PROPERTY_SECTION_POSITION_COL = PROPERTY_ID_COL + 4; 
   
   public static final String PROPERTY_HIDDEN = "hidden";
   public static final int PROPERTY_HIDDEN_COL = PROPERTY_ID_COL + 5; 
   
   public static final String PROPERTY_QUERYABLE = "queryable";
   public static final int PROPERTY_QUERYABLE_COL = PROPERTY_ID_COL + 6;

	public static final String[] ALL_COLUMNS = { 
	   PropertyTable.PROPERTY_ID, 
	   PropertyTable.PROPERTY_NAME, 
	   PropertyTable.PROPERTY_LABEL, 
	   PropertyTable.PROPERTY_SECTION,
	   PropertyTable.PROPERTY_SECTION_POSITION, 
	   PropertyTable.PROPERTY_HIDDEN,
	   PropertyTable.PROPERTY_QUERYABLE };

	
	/** SQLite database creation statement. Auto-increments IDs of inserted requests.
	 * Property IDs are set after insertion into the database. */
	public static final String TABLE_CREATE = "create table " + TABLE_NAME + " (" + 
			PROPERTY_ID 		        + " integer primary key autoincrement, " + 
	      PROPERTY_NAME             + " text, " +
			PROPERTY_LABEL		        + " text, " + 
			PROPERTY_SECTION	        + " text, " +
			PROPERTY_SECTION_POSITION + " int, " +
			PROPERTY_HIDDEN           + " text, " +
         PROPERTY_QUERYABLE        + " text);";
	
	/** SQLite database table removal statement. Only used if upgrading database. */
	public static final String TABLE_DROP = "drop table if exists " + TABLE_NAME;
	
	/**
	 * Initializes the database.
	 * 
	 * @param database
	 * 				The database to initialize.	
	 */
	public static void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_CREATE);
	}
	
	
	/**
	 * Upgrades the database to a new version.
	 * 
	 * @param database
	 * 					The database to upgrade.
	 * @param oldVersion
	 * 					The old version of the database.
	 * @param newVersion
	 * 					The new version of the database.
	 */
	public static void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
		database.execSQL(TABLE_DROP);
		onCreate(database);
	}
	
	/**
	 * Verifies the correct set of columns to return data from when performing a query.
	 * 
	 * @param projection
	 * 						The set of columns about to be queried.
	 */
	public static void checkColumns(String[] projection) {
		String[] available = ALL_COLUMNS;
		
		if(projection != null) {
			HashSet<String> requestedColumns = new HashSet<String>(Arrays.asList(projection));
			HashSet<String> availableColumns = new HashSet<String>(Arrays.asList(available));
			
			if(!availableColumns.containsAll(requestedColumns))	{
				throw new IllegalArgumentException("Unknown columns in projection");
			}
		}
	}
}

package com.veevavault.mobile.database;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.util.Log;

import com.veevavault.mobile.dao.BaseDAO;
import com.veevavault.mobile.database.tables.DocumentTable;
import com.veevavault.mobile.database.tables.DocumentXPropertyTable;
import com.veevavault.mobile.database.tables.PropertyTable;
import com.veevavault.mobile.database.tables.UserTable;
import com.veevavault.mobile.database.tables.UserXEndpointTable;
import com.veevavault.mobile.database.tables.VaultEndpointTable;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Property;
import com.veevavault.mobile.model.User;
import com.veevavault.mobile.model.VaultEndpoint;
public class DatabaseAccessor {
	
	Context mContext;
	
	public DatabaseAccessor(Context context) {
		this.mContext = context;
	}

	public void saveDocumentToDb(Document document) {
	   Integer documentDatabaseId = document.getDatabaseId();
	   Uri uri = Uri.parse(DBContentProvider.DOCUMENT_CONTENT_URI + "/document/" + (documentDatabaseId == null ? 0 : documentDatabaseId));  // database ID is ignored in insert

      ContentValues values = new ContentValues();
      values.put(DocumentTable.DOCUMENT_VEEVA_ID, document.getId().toString());
      values.put(DocumentTable.DOCUMENT_FILE_PATH, document.getFilePath());
      values.put(DocumentTable.DOCUMENT_VAULT_ID, document.getVaultId().toString());
      
      ByteArrayOutputStream stream = new ByteArrayOutputStream();
      if (document.getThumbnail() != null) {
         document.getThumbnail().compress(Bitmap.CompressFormat.PNG, 100, stream);
      }
      byte[] byteArray = stream.toByteArray();
      values.put(DocumentTable.DOCUMENT_THUMBNAIL, byteArray);

      if (documentDatabaseId == null) {
         documentDatabaseId = Integer.parseInt(mContext.getContentResolver().insert(uri, values)
               .getLastPathSegment());
         document.setDatabaseId(documentDatabaseId);
      }
      else {
         mContext.getContentResolver().update(uri, values, null, null);
      }

      savePropertiesToDb(documentDatabaseId, document.getProperties());
	}
	
	private void savePropertiesToDb(int documentDatabaseId, Collection<Property> properties) {
	   Integer propertyId = null;
	   ContentValues values;
      Uri uri;
      
      for (Property p : properties) {
         propertyId = p.getId();
         values = new ContentValues();
         uri = Uri.parse(DBContentProvider.PROPERTY_CONTENT_URI + "/property/" + (propertyId == null ? 0 : propertyId)); // id ignored in insert 
         values.put(PropertyTable.PROPERTY_NAME, p.getName());
         values.put(PropertyTable.PROPERTY_LABEL, p.getLabel());
         values.put(PropertyTable.PROPERTY_SECTION, p.getSection());
         values.put(PropertyTable.PROPERTY_SECTION_POSITION, p.getSectionPosition());
         values.put(PropertyTable.PROPERTY_HIDDEN, Boolean.toString(p.isHidden()));
         values.put(PropertyTable.PROPERTY_QUERYABLE, Boolean.toString(p.isQueryable()));
         
         if (propertyId == null) {
            propertyId = Integer.parseInt(mContext.getContentResolver().insert(uri, values).getLastPathSegment());
            p.setId(propertyId);
            
            // INSERT VALUE INTO DXP TABLE
            uri = Uri.parse(DBContentProvider.DOCUMENT_X_PROPERTY_CONTENT_URI + "/propertyValue");
            
            values = new ContentValues();
            values.put(DocumentXPropertyTable.DOCUMENT_ID, documentDatabaseId);
            values.put(DocumentXPropertyTable.PROPERTY_ID, propertyId);
            values.put(DocumentXPropertyTable.VALUE, p.getValue());
            
            mContext.getContentResolver().insert(uri, values); 
         }
         else {
            
            // UPDATE VALUE IN DXP TABLE
            uri = Uri.parse(DBContentProvider.DOCUMENT_X_PROPERTY_CONTENT_URI + "/document/" + documentDatabaseId + "/property/" + propertyId);
            
            values = new ContentValues();
            values.put(DocumentXPropertyTable.DOCUMENT_ID, documentDatabaseId);
            values.put(DocumentXPropertyTable.PROPERTY_ID, propertyId);
            values.put(DocumentXPropertyTable.VALUE, p.getValue());
            
            mContext.getContentResolver().update(uri, values, null, null); 
         }
      }
	}

   public ArrayList<Document> retrieveDownloadedDocuments() {
      ArrayList<Document> rtn = new ArrayList<Document>();
      Document document;
      
      String[] projection = DocumentTable.ALL_COLUMNS;
      Uri uri = Uri.parse(DBContentProvider.DOCUMENT_CONTENT_URI + "/document/vaultId/" + BaseDAO.getCurrentVaultEndpoint().getVaultID());
      Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      if (cursor.getCount() > 0) {
         while (cursor.moveToNext()) {
            document = new Document();
            
            document.setDatabaseId(cursor.getInt(DocumentTable.DOCUMENT_ID_COL));
            document.setId(new BigInteger(cursor.getString(DocumentTable.DOCUMENT_VEEVA_ID_COL)));
            document.setFilePath(cursor.getString(DocumentTable.DOCUMENT_FILE_PATH_COL));
            document.setVaultId(cursor.getString(DocumentTable.DOCUMENT_VAULT_ID_COL));
            
            byte[] thumbnail = cursor.getBlob(DocumentTable.DOCUMENT_THUMBNAIL_COL);
            document.setThumbnail(BitmapFactory.decodeByteArray(thumbnail, 0, thumbnail.length));
            
            document.setProperties(getDocumentProperties(document.getDatabaseId()));
            rtn.add(document);
         }
      }
      cursor.close();
      
      return rtn;
   }

   private HashSet<Property> getDocumentProperties(int ourDocumentId) {   
      HashSet<Property> rtn = null;
      Property property;
      String[] projection;
      Uri uri;
      Cursor cursor;
      
      projection = DocumentXPropertyTable.ALL_COLUMNS;
      uri = Uri.parse(DBContentProvider.DOCUMENT_X_PROPERTY_CONTENT_URI + "/document/" + ourDocumentId);
      cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      if (cursor.getCount() > 0) {
         rtn = new HashSet<Property>();
         
         while (cursor.moveToNext()) {
            property = new Property();
            property.setValue(cursor.getString(DocumentXPropertyTable.VALUE_COL));
            property.setId(cursor.getInt(DocumentXPropertyTable.PROPERTY_ID_COL));
            
            retrievePropertyMetadata(property);
            
            rtn.add(property);
         }
      }
      cursor.close();
      
      return rtn;
   }

   private void retrievePropertyMetadata(Property property) {
      Uri uri = Uri.parse(DBContentProvider.PROPERTY_CONTENT_URI + "/property/" + property.getId());
      String[] projection = PropertyTable.ALL_COLUMNS;
      Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      if (cursor.getCount() > 0) {
         while (cursor.moveToNext()) {
            property.setHidden(Boolean.parseBoolean(cursor.getString(PropertyTable.PROPERTY_HIDDEN_COL)));
            property.setLabel(cursor.getString(PropertyTable.PROPERTY_LABEL_COL));
            property.setName(cursor.getString(PropertyTable.PROPERTY_NAME_COL));
            property.setQueryable(Boolean.parseBoolean(cursor.getString(PropertyTable.PROPERTY_QUERYABLE_COL)));
            property.setSection(cursor.getString(PropertyTable.PROPERTY_SECTION_COL));
            property.setSectionPosition(cursor.getInt(PropertyTable.PROPERTY_SECTION_POSITION_COL));
         }
      }
      cursor.close();
   }
   
   public void saveCurrentUser(User currentUser) {
      Uri uri = Uri.parse(DBContentProvider.USER_CONTENT_URI + "/user/" + currentUser.getId());
      String[] projection = UserTable.ALL_COLUMNS;
      Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      ContentValues values = new ContentValues();
      values.put(UserTable.USER_ID, currentUser.getId());
      values.put(UserTable.USER_NAME, currentUser.getName());
      values.put(UserTable.USER_FIRST_NAME, currentUser.getFirstName());
      values.put(UserTable.USER_LAST_NAME, currentUser.getLastName());
      
      if (cursor.getCount() > 0) {
         mContext.getContentResolver().update(uri, values, null, null);
         Log.d("saveCurrentUser", "user " + currentUser.getId() + " updated");
      }
      else {
         mContext.getContentResolver().insert(uri, values);
         Log.d("saveCurrentUser", "user " + currentUser.getId() + " inserted");
      }
      
      return;
   }

   public void saveVaultEndpoints(User user, List<VaultEndpoint> availableVaultEndpoints) {
      Uri uri;
      String[] projection;
      Cursor cursor;
      ArrayList<String> savedVaultIds = new ArrayList<String>();
      
      uri = Uri.parse(DBContentProvider.USER_X_ENDPOINT_CONTENT_URI + "/user/" + user.getId());
      projection = UserXEndpointTable.ALL_COLUMNS;
      cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      if (cursor.getCount() > 0) {
         while (cursor.moveToNext()) {
            savedVaultIds.add(cursor.getString(UserXEndpointTable.VAULT_ID_COL));
         }
      }
         
      if (availableVaultEndpoints != null) {
         for (VaultEndpoint endpoint : availableVaultEndpoints) {
            if (!savedVaultIds.contains(endpoint.getVaultID().toString())) {
               addUnknownVaultEndpoint(endpoint);
               addUserXEndpoint(user, endpoint);
            }
         }
      }
      cursor.close();
   }

   private void addUserXEndpoint(User user, VaultEndpoint endpoint) {
      Uri uri = Uri.parse(DBContentProvider.USER_X_ENDPOINT_CONTENT_URI 
            + "/user/" + user.getId() + "/endpoint/" + endpoint.getVaultID().toString());
      
      ContentValues values = new ContentValues();
      values.put(UserXEndpointTable.USER_ID, user.getId());
      values.put(UserXEndpointTable.VAULT_ID, endpoint.getVaultID().toString());
      
      mContext.getContentResolver().insert(uri, values);
   }

   private void addUnknownVaultEndpoint(VaultEndpoint endpoint) {
      Uri uri = Uri.parse(DBContentProvider.VAULT_ENDPOINT_CONTENT_URI + "/vaultEndpoint/" + endpoint.getVaultID().toString());
      String[] projection = VaultEndpointTable.ALL_COLUMNS;
      Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      ContentValues values = new ContentValues();
      values.put(VaultEndpointTable.VAULT_ID, endpoint.getVaultID().toString());
      values.put(VaultEndpointTable.VAULT_NAME, endpoint.getName());
      values.put(VaultEndpointTable.VAULT_DNS_NAME, endpoint.getDnsName());
      
      if (cursor.getCount() > 0) {
         mContext.getContentResolver().update(uri, values, null, null);
         Log.d("saveVaultEndpoints", "vault id " + endpoint.getVaultID().toString() + " updated");
      }
      else {
         mContext.getContentResolver().insert(uri, values);
         Log.d("saveVaultEndpoints", "vault id " + endpoint.getVaultID().toString() + " inserted");
      }
      cursor.close();
   }

   public User loadUser(String username) {
      Uri uri = Uri.parse(DBContentProvider.USER_CONTENT_URI + "/user");
      String[] projection = UserTable.ALL_COLUMNS;
      String selection = UserTable.USER_NAME + "=?";
      String selectionArgs[] = {username};
      Cursor cursor = mContext.getContentResolver().query(uri, projection, selection, selectionArgs, null);
      
      User user = null;
      
      if (cursor.getCount() > 0) {
         cursor.moveToFirst();
         
         user = new User();
         user.setId(cursor.getString(UserTable.USER_ID_COL));
         user.setFirstName(cursor.getString(UserTable.USER_FIRST_NAME_COL));
         user.setLastName(cursor.getString(UserTable.USER_LAST_NAME_COL));
         user.setName(cursor.getString(UserTable.USER_NAME_COL));
      }
      cursor.close();
      
      return user;
   }
   
   public List<VaultEndpoint> loadVaultEndpoints(User user) {
      List<VaultEndpoint> endpoints = new ArrayList<VaultEndpoint>();
      
      Uri uri = Uri.parse(DBContentProvider.USER_X_ENDPOINT_CONTENT_URI + "/user/" + user.getId());
      String[] projection = UserXEndpointTable.ALL_COLUMNS;
      Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      if (cursor.getCount() > 0) {
         VaultEndpoint endpoint;
         
         while (cursor.moveToNext()) {
            String endpointId = cursor.getString(UserXEndpointTable.VAULT_ID_COL);
            endpoint = getVaultEndpoint(endpointId);
            if (endpoint != null) {
               endpoints.add(endpoint);
            }
         }
      }
      cursor.close();
      
      return endpoints;      
   }

   private VaultEndpoint getVaultEndpoint(String endpointId) {
      VaultEndpoint endpoint = null;
      
      Uri uri = Uri.parse(DBContentProvider.VAULT_ENDPOINT_CONTENT_URI + "/vaultEndpoint/" + endpointId);
      String[] projection = VaultEndpointTable.ALL_COLUMNS;
      Cursor cursor = mContext.getContentResolver().query(uri, projection, null, null, null);
      
      if (cursor.getCount() > 0) {
         endpoint = new VaultEndpoint();
         
         while (cursor.moveToNext()) {
            endpoint.setDnsName(cursor.getString(VaultEndpointTable.VAULT_DNS_NAME_COL));
            endpoint.setName(cursor.getString(VaultEndpointTable.VAULT_NAME_COL));
            endpoint.setVaultID(endpointId);
         }
      }
      cursor.close();
      return endpoint;
   }
}

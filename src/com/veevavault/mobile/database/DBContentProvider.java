package com.veevavault.mobile.database;

import java.util.List;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

import com.veevavault.mobile.database.tables.DocumentTable;
import com.veevavault.mobile.database.tables.DocumentXPropertyTable;
import com.veevavault.mobile.database.tables.PropertyTable;
import com.veevavault.mobile.database.tables.UserTable;
import com.veevavault.mobile.database.tables.UserXEndpointTable;
import com.veevavault.mobile.database.tables.VaultEndpointTable;

public class DBContentProvider extends ContentProvider {
	/** The database. */
	private DatabaseHelper database;
	
	/** Values for the URIMatcher. */
	private static final int DOCUMENT = 1;
	private static final int PROPERTY = 2;
	private static final int DOCUMENT_X_PROPERTY = 3;
	private static final int DXP_DOCUMENT_ID = 4;
	private static final int DOCUMENTS = 5;
	private static final int PROPERTIES = 6;
	private static final int DXP_VALUE_UPDATE = 7;
	private static final int VAULT_ENDPOINT = 8;
	private static final int USER = 9;
	private static final int USER_X_ENDPOINT = 10;
	private static final int ENDPOINTS = 11;
	private static final int USER_BY_NAME = 12;
	
	/** The authority for this content provider. */
	private static final String AUTHORITY = "com.veevavault.mobile.database.contentprovider";
	
	/** The database table to read from and write to, and also the root path for use in the URI matcher.
	 * This is essentially a label to a two-dimensional array in the database filled with rows of jokes
	 * whose columns contain joke data. */
	private static final String DOCUMENT_TABLE = DocumentTable.TABLE_NAME;
	private static final String PROPERTY_TABLE = PropertyTable.TABLE_NAME;
   private static final String DOCUMENT_X_PROPERTY_TABLE = DocumentXPropertyTable.TABLE_NAME;
   private static final String VAULT_ENDPOINT_TABLE = VaultEndpointTable.TABLE_NAME;
   private static final String USER_TABLE = UserTable.TABLE_NAME;
   private static final String USER_X_ENDPOINT_TABLE = UserXEndpointTable.TABLE_NAME;
	
	/** This provider's content location. Used by accessing applications to
	 * interact with this provider. */
	public static final Uri DOCUMENT_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + DOCUMENT_TABLE);
	public static final Uri PROPERTY_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + PROPERTY_TABLE);
	public static final Uri DOCUMENT_X_PROPERTY_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + DOCUMENT_X_PROPERTY_TABLE);
	public static final Uri VAULT_ENDPOINT_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + VAULT_ENDPOINT_TABLE);
	public static final Uri USER_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + USER_TABLE);
	public static final Uri USER_X_ENDPOINT_CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/" + USER_X_ENDPOINT_TABLE);
	
	
	/** Matches content URIs requested by accessing applications with possible
	 * expected content URI formats to take specific actions in this provider. */
	private static final UriMatcher sURIMatcher;
	static {
		sURIMatcher = new UriMatcher(UriMatcher.NO_MATCH);
		sURIMatcher.addURI(AUTHORITY, DOCUMENT_TABLE + "/document/vaultId/#", DOCUMENTS);
		sURIMatcher.addURI(AUTHORITY, DOCUMENT_TABLE + "/document/#", DOCUMENT);
      sURIMatcher.addURI(AUTHORITY, PROPERTY_TABLE + "/property", PROPERTIES);
      sURIMatcher.addURI(AUTHORITY, PROPERTY_TABLE + "/property/#", PROPERTY);
		sURIMatcher.addURI(AUTHORITY, DOCUMENT_X_PROPERTY_TABLE + "/propertyValue", DOCUMENT_X_PROPERTY);
		sURIMatcher.addURI(AUTHORITY, DOCUMENT_X_PROPERTY_TABLE + "/document/#", DXP_DOCUMENT_ID);
		sURIMatcher.addURI(AUTHORITY, DOCUMENT_X_PROPERTY_TABLE + "/document/#/property/#", DXP_VALUE_UPDATE);
		sURIMatcher.addURI(AUTHORITY, VAULT_ENDPOINT_TABLE + "/vaultEndpoint/#", VAULT_ENDPOINT);
		sURIMatcher.addURI(AUTHORITY, USER_TABLE + "/user/#", USER);
		sURIMatcher.addURI(AUTHORITY, USER_TABLE + "/user", USER_BY_NAME);
		sURIMatcher.addURI(AUTHORITY, USER_X_ENDPOINT_TABLE + "/user/#/endpoint/#", USER_X_ENDPOINT);
		sURIMatcher.addURI(AUTHORITY, USER_X_ENDPOINT_TABLE + "/user/#", ENDPOINTS);
	}
	
	@Override
	public boolean onCreate() {
		database = new DatabaseHelper(getContext(), DatabaseHelper.DATABASE_NAME, null, DatabaseHelper.DATABASE_VERSION);
		return true;
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getType(Uri uri) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		/** Open the database for writing. */
		SQLiteDatabase sqlDB = this.database.getWritableDatabase();
		
		long id = -1;
		
		/** Match the passed-in URI to an expected URI format. */
		int uriType = sURIMatcher.match(uri);
		switch(uriType)	{
		case DOCUMENT:
			id = sqlDB.insert(DOCUMENT_TABLE, null, values);
			break;
		case PROPERTY:
		   id = sqlDB.insert(PROPERTY_TABLE, null, values);
		   break;
		case DOCUMENT_X_PROPERTY:
		   sqlDB.insert(DOCUMENT_X_PROPERTY_TABLE, null, values);
			break;
		case VAULT_ENDPOINT:
		   sqlDB.insert(VAULT_ENDPOINT_TABLE, null, values);
		   break;
		case USER:
		   sqlDB.insert(USER_TABLE, null, values);
		   break;
		case USER_X_ENDPOINT:
		   sqlDB.insert(USER_X_ENDPOINT_TABLE, null, values);
		   break;
		default:
			throw new IllegalArgumentException("Unknown URI: " + uri);
		}
		
		/** Alert any watchers of an underlying data change for content/view refreshing. */
		getContext().getContentResolver().notifyChange(uri, null);
		
		return Uri.parse("insertResultId" + "/" + id);
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		
		/** Use a helper class to perform a query for us. */
		SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();		
		
		int uriType = sURIMatcher.match(uri);
		
      switch (uriType) {
      case DOCUMENTS:
         queryBuilder.setTables(DOCUMENT_TABLE);
         int vault = Integer.parseInt(uri.getLastPathSegment());
         queryBuilder.appendWhere(DocumentTable.DOCUMENT_VAULT_ID + "=" + vault);
         break;
      case PROPERTIES:
         queryBuilder.setTables(PROPERTY_TABLE);
         break;
      case PROPERTY:
         queryBuilder.setTables(PROPERTY_TABLE);
         int propertyId = Integer.parseInt(uri.getLastPathSegment());
         queryBuilder.appendWhere(PropertyTable.PROPERTY_ID + "=" + propertyId);
         break;
      case DXP_DOCUMENT_ID:
         queryBuilder.setTables(DOCUMENT_X_PROPERTY_TABLE);
         int documentId = Integer.parseInt(uri.getLastPathSegment());
         queryBuilder.appendWhere(DocumentXPropertyTable.DOCUMENT_ID + "="
               + documentId);
         break;
      case USER:
         queryBuilder.setTables(USER_TABLE);
         queryBuilder.appendWhere(UserTable.USER_ID + "=" + Integer.parseInt(uri.getLastPathSegment()));
         break;
      case USER_BY_NAME:
         queryBuilder.setTables(USER_TABLE);
         break;
      case VAULT_ENDPOINT:
         queryBuilder.setTables(VAULT_ENDPOINT_TABLE);
         queryBuilder.appendWhere(VaultEndpointTable.VAULT_ID + "=" + Integer.parseInt(uri.getLastPathSegment()));
         break;
      case ENDPOINTS:
         queryBuilder.setTables(USER_X_ENDPOINT_TABLE);
         int user = Integer.parseInt(uri.getLastPathSegment());
         queryBuilder.appendWhere(UserXEndpointTable.USER_ID + "=" + user);
         break;
      default:
         throw new IllegalArgumentException("Unknown URI: " + uri);
      }
		
		/** Perform the database query. */
		SQLiteDatabase db = this.database.getWritableDatabase();
		Cursor cursor = queryBuilder.query(db, projection, selection, selectionArgs, null, null, null);
		
		/** Set the cursor to automatically alert listeners for content/view refreshing. */
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		
		return cursor;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
	   /** Open the database for writing. */
      SQLiteDatabase sqlDB = this.database.getWritableDatabase();   
      
      String where;
      
      /** Match the passed-in URI to an expected URI format. */
      int uriType = sURIMatcher.match(uri);
      switch(uriType)   {
      case DOCUMENT:
         where = DocumentTable.DOCUMENT_ID + "=" + Integer.parseInt(uri.getLastPathSegment());
         sqlDB.update(DOCUMENT_TABLE, values, where, null);
         break;
      case PROPERTY:
         where = PropertyTable.PROPERTY_ID + "=" + Integer.parseInt(uri.getLastPathSegment());
         sqlDB.update(PROPERTY_TABLE, values, where, null);
         break;
      case DXP_VALUE_UPDATE:
         List<String> pathSegments = uri.getPathSegments();
         String documentId = pathSegments.get(pathSegments.size()-3);
         String propertyId = pathSegments.get(pathSegments.size()-1);
         where = DocumentXPropertyTable.DOCUMENT_ID + "=" + documentId + " and " + DocumentXPropertyTable.PROPERTY_ID + "=" + propertyId; 
         sqlDB.update(DOCUMENT_X_PROPERTY_TABLE, values, where, null);
         break;
      case USER:
         where = UserTable.USER_ID + "=" + Integer.parseInt(uri.getLastPathSegment());
         sqlDB.update(USER_TABLE, values, where, null);
         break;
      case VAULT_ENDPOINT:
         where = VaultEndpointTable.VAULT_ID + "=" + Integer.parseInt(uri.getLastPathSegment());
         sqlDB.update(VAULT_ENDPOINT_TABLE, values, where, null);
         break;
      default:
         throw new IllegalArgumentException("Unknown URI: " + uri);
      }
      
      /** Alert any watchers of an underlying data change for content/view refreshing. */
      getContext().getContentResolver().notifyChange(uri, null);
      
      return 0;
	}
}

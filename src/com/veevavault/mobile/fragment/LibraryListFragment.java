package com.veevavault.mobile.fragment;

import java.util.List;

import org.androidannotations.annotations.EFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.activity.VaultApplication;
import com.veevavault.mobile.dao.request.RetrieveDocumentFileUrlRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentThumbnailRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.view.DocumentListItemView.OnDownloadDocumentListener;
import com.veevavault.mobile.view.EndlessScrollListener;

@EFragment
public class LibraryListFragment extends DocumentListFragment implements RetrieveDocumentsRequest.Listener, 
      RetrieveDocumentThumbnailRequest.Listener, RetrieveDocumentFileUrlRequest.Listener, OnDownloadDocumentListener {
   
	private static final int DOC_REQUEST_LIMIT = 20;
	private String named_filter;
	private String scope;
	private String search;	
	private int maxEntries;
	private int currentEntries;

	public void paginateListView() {
		listView.setOnScrollListener(new EndlessScrollListener(10) {
			@Override
			public void onLoadMore(int page, int totalItemsCount) {
				
				if (currentEntries < maxEntries) {
					//spinner.setVisibility(View.VISIBLE);
					requestRetrieveDocuments(named_filter, scope, search);
				} else {
					//spinner.setVisibility(View.GONE);
					//Toast.makeText(getActivity(), "All documents loaded!", Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	private void requestRetrieveDocuments(String named_filter,
			String scope, String search) {
		RetrieveDocumentsRequest retrieveDocuments = new RetrieveDocumentsRequest();

		retrieveDocuments.setLimit(DOC_REQUEST_LIMIT);
		retrieveDocuments.setStart(currentEntries);
		retrieveDocuments.setNamed_filter(named_filter);
		retrieveDocuments.setScope(scope);
		retrieveDocuments.setSearch(search);
		retrieveDocuments.setCallback(this);
		retrieveDocuments.setOnDevice(onDevice);
		retrieveDocuments.setOffline(onDevice);
      retrieveDocuments.setNoCache(onDevice);
		retrieveDocuments.setContext(context);
		retrieveDocuments.execute();

		currentEntries += DOC_REQUEST_LIMIT;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		this.named_filter = this.getArguments().getString("title");
		this.scope = this.getArguments().getString("scope");
		this.search = this.getArguments().getString("search");
		this.onDevice = this.getArguments().getBoolean("onDevice");
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		pagerView = inflater.inflate(R.layout.fragment_pager_list, container, false);
		context = inflater.getContext();
		spinner = (ProgressBar) pagerView.findViewById(R.id.l_progress_bar);
		initDocList();
		setViewType(viewState);
		spinner.setVisibility(View.VISIBLE);
		paginateListView();

		return pagerView;
	}

	public void initDocList() {
		super.initDocList();
		currentEntries = 0;
		requestRetrieveDocuments(this.named_filter, this.scope, this.search);
	}

	@Override
	public void onRetrieveDocumentsResponse(RetrieveDocumentsResponse response) {
		if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
			List<Document> responseDocs = response.getDocuments();
			maxEntries = response.getMaxSize();
			if (responseDocs != null && responseDocs.size() > 0) {
				for (Document doc : responseDocs) {
					bindableList.add(doc);
					if (doc.getThumbnail() == null) {
						requestDocumentThumbnail(doc);
					}
				}
			}
		} else if (!(response != null && response.getDocuments() != null
               && response.getDocuments().isEmpty())) {
	      Toast.makeText(getActivity(), "Error retrieving documents.", Toast.LENGTH_SHORT).show();   			
		}
		updateAdapterOnResponse();
	}

	public void refresh() {
		super.refresh();
		paginateListView();
	}
}

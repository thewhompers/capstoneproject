package com.veevavault.mobile.fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.activity.DocumentPropertiesActivity_;
import com.veevavault.mobile.adapter.TaskListAdapter;
import com.veevavault.mobile.dao.request.RetrieveTasksRequest;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveTasksResponse;
import com.veevavault.mobile.model.Task;

public class TaskFragment extends Fragment implements
      RetrieveTasksRequest.Listener {

   private static final int DUE_DATE = 1;
   private List<Task> taskList;
   TaskListAdapter adapter;
   private ListView listView;
   private TextView emptyView;
   private ProgressBar spinner;
   private Context context;

   public static Calendar convertStringDateToGregorianDate(String s) {
      String[] list = s.split("/");
      Calendar date = new GregorianCalendar();
      date.set(Calendar.YEAR, Integer.parseInt(list[2]) + 2000);
      date.set(Calendar.MONTH, Integer.parseInt(list[0]) - 1);
      date.set(Calendar.DAY_OF_MONTH, Integer.parseInt(list[1]));
      date.add(Calendar.DAY_OF_YEAR, -1);
      return date;
   }

   public static Comparator<Task> dueDateTaskComparator
         = new Comparator<Task>() {

      public int compare(Task task1, Task task2) {
      Calendar date1 = convertStringDateToGregorianDate(task1.getDateDue());
      Calendar date2 = convertStringDateToGregorianDate(task2.getDateDue());
      return date1.compareTo(date2);
      }
   };

   private void sortTasks(int sortBy, List<Task> taskList) {
      if (sortBy == DUE_DATE) {
         Collections.sort(taskList, dueDateTaskComparator);
      }
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
         Bundle savedInstanceState) {
      View v = inflater.inflate(R.layout.listview_generic, container, false);
      context = inflater.getContext();
      spinner = (ProgressBar) v.findViewById(R.id.progress_bar);

      emptyView = (TextView) v.findViewById(android.R.id.empty);
      listView = (ListView) v.findViewById(R.id.list);
      taskList = new ArrayList<Task>();
      adapter = new TaskListAdapter(inflater.getContext(), taskList);
      listView.setAdapter(adapter);
      
      listView.setOnItemClickListener(new OnItemClickListener() {
      		@Override
      		public void onItemClick(AdapterView<?> parent, View view, int position,
      				long id) {
      	          Intent intent = new Intent(getActivity(), DocumentPropertiesActivity_.class);
      	          intent.putExtra("id", taskList.get(position).getDocId().toString());
      	          startActivity(intent);
      		}
         });

      return v;
   }

   @Override
   public void onActivityCreated(Bundle savedInstanceState) {
      super.onActivityCreated(savedInstanceState);
      
      requestTasks();
   }

   public interface OnTaskSelectListener {
      public void onTaskSelectListener(Task task);
   }

   public void requestTasks() {
	  this.spinner.setVisibility(View.VISIBLE);
      RetrieveTasksRequest request = new RetrieveTasksRequest();
      request.setCallback(this);
      request.setContext(context);
      request.execute();
   }
   
   @Override
   public void onRetrieveTasksResponse(RetrieveTasksResponse response) {
      if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
         for (Task t : response.getTasks()) {
            taskList.add(t);
         }
         sortTasks(DUE_DATE, taskList);
         adapter.notifyDataSetChanged(); 
         spinner.setVisibility(View.GONE);
         
         if(taskList.isEmpty()){
        	 listView.setEmptyView(emptyView);
         }
         else {
        	 emptyView.setVisibility(View.GONE);
         }
      }
      else {
         Toast.makeText(getActivity(), "NO", Toast.LENGTH_SHORT).show();
      }
      
   }
   

}

package com.veevavault.mobile.fragment;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.adapter.DownloadedDocumentsListAdapter;

public class DownloadedDocumentsFragment extends Fragment{

	List<String> filePathList;
	DownloadedDocumentsListAdapter listAdapter;
	File dir;
	Context context;
	ListView listView;
	TextView emptyView;
	ProgressBar spinner;

	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container,  
			Bundle savedInstanceState) {  
		View v = inflater.inflate(R.layout.listview_generic, container, false);

		this.context = inflater.getContext();
		filePathList = new ArrayList<String>();
		listAdapter = new DownloadedDocumentsListAdapter(inflater.getContext(), filePathList);
		listView = (ListView) v.findViewById(R.id.list);
		emptyView = (TextView) v.findViewById(android.R.id.empty);
		listView.setAdapter(listAdapter);
		spinner = (ProgressBar) v.findViewById(R.id.progress_bar);

		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				String documentFilePath = filePathList.get(position);
				if(!(documentFilePath.equals(getResources().getString(R.string.no_results_text)))){
					String[] parsedFilePath = documentFilePath.split("\\.");
					String documentFileType = parsedFilePath[parsedFilePath.length-1];

					if (documentFileType != null)
					{
						MimeTypeMap mime = MimeTypeMap.getSingleton();
						String type = mime.getMimeTypeFromExtension(documentFileType);

						Intent appIntent = new Intent(Intent.ACTION_VIEW);
						appIntent.setDataAndType(Uri.parse("file://" +documentFilePath), type);
						appIntent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
						startActivity(appIntent);
					}
					else
					{
						Toast.makeText(context, getResources().getString(R.string.download_doc_mime_error), Toast.LENGTH_SHORT).show();
					}
				}

			}
		});

		return v;  
	}  

	@Override 
	public void onActivityCreated(Bundle savedInstanceState) {  
		super.onActivityCreated(savedInstanceState);  
		
		spinner.setVisibility(View.VISIBLE);
		fillFilePathList();
	} 

	private void fillFilePathList(){

		dir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/Vault_Mobile");

		if(!dir.exists()){
			dir.mkdirs();
		}

		File[] files = dir.listFiles();

		for (int i = 0; i<files.length;i++){
			filePathList.add(files[i].getAbsolutePath());
		}

		this.listAdapter.notifyDataSetChanged();
		spinner.setVisibility(View.GONE);
		if(filePathList.size() == 0){
			listView.setEmptyView(emptyView);
		}
	}

}

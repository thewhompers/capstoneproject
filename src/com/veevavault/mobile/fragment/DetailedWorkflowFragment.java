package com.veevavault.mobile.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.activity.HomeActivity;
import com.veevavault.mobile.model.ActiveWorkflow;
import com.veevavault.mobile.model.Document;

public class DetailedWorkflowFragment extends Fragment {
	
	/** Active Workflow **/
	ActiveWorkflow workflow;
	
	TextView workflowName;
	TextView workflowOwnerName;
	Document document;
	ImageView workflowOwnerProfilePicture;
	ListView participantListView;
	
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.workflow_detailed, container,
				false);
		
        
		return v;
	}
	
	/**
	 * Get the selected workflow from the home activity
	 */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        workflow = ((HomeActivity) activity).getSelectedWorkflow();
    }
}

package com.veevavault.mobile.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.androidannotations.annotations.EFragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.adapter.WorkflowListAdapter;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;
import com.veevavault.mobile.model.ActiveWorkflow;
import com.veevavault.mobile.model.Document;

// CLASS MAY OR MAY NOT NEED TO IMPLEMENT RETRIEVE DOCUMENTS LISTENER. IMPLEMENTED FOR FAKE DATA
@EFragment()
public class ActiveWorkflowsFragment extends Fragment implements RetrieveDocumentsRequest.Listener {
	
	/** ActiveWorkflows **/
	ArrayList<ActiveWorkflow> activeWorkflows;
	
	/** list adapter **/
	WorkflowListAdapter listAdapter;
	
	/** Callback to containing activity **/
	OnActiveWorkflowSelectListener callback;
	
	private ListView listView;
	private TextView emptyView;
	private ProgressBar spinner;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.listview_generic, container,
				false);
		
		spinner = (ProgressBar) v.findViewById(R.id.progress_bar);
        listView = (ListView) v.findViewById(R.id.list);
        emptyView = (TextView) v.findViewById(android.R.id.empty);
		activeWorkflows = new ArrayList<ActiveWorkflow>();
		fillData();
		listAdapter = new WorkflowListAdapter(inflater.getContext(), activeWorkflows);
        listView.setAdapter(listAdapter);
        
        if(activeWorkflows.isEmpty()){
        	listView.setEmptyView(emptyView);
        }
        
        listView.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				callback.onActiveWorkflowSelect(activeWorkflows.get(position));
			}
        	
        });
        
		return v;
	}
	
	//TODO: NOT ACTUAL IMPLEMENTATION - THIS IS COMPLETELY ARBITRARY - 
	private void fillData() {
		/*RetrieveDocumentsRequest retrieveDocuments = new RetrieveDocumentsRequest();

		retrieveDocuments.setLimit(10);
		retrieveDocuments.setNamed_filter(NamedFilter.MY_DOCUMENTS);
		retrieveDocuments.setCallback(this);
		
		Log.d("HERE", "before retrieve docs call");
		retrieveDocuments.execute();	*/
	}

	/**
	 * Makes sure the container activity has implemented the callback interface.
	 */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        
        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            callback = (OnActiveWorkflowSelectListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnCreatureSelectListener");
        }
    }
    
	/**
	 *  Interface for callback. 
	 *  Parent activity must implement this interface to communicate with fragment 
	 **/
	public interface OnActiveWorkflowSelectListener {
		public void onActiveWorkflowSelect(ActiveWorkflow workflow);
	}


	public void onRetrieveDocumentsResponse(RetrieveDocumentsResponse response) {
		Random rand = new Random();
		List<Document> docList = response.getDocuments();
		Log.d("HERE", "" + docList.size());
		for (Document d : docList) {
			Log.d("HERE", "" + d.getId());
			activeWorkflows.add(new ActiveWorkflow(d, (rand.nextInt(2) == 0 ? "Approval" : "Review")));
		}
		
		this.listAdapter.notifyDataSetChanged();
		
		spinner.setVisibility(View.GONE);
        
        if(activeWorkflows.isEmpty()){
        	listView.setEmptyView(emptyView);
        }
        else {
        	emptyView.setVisibility(View.GONE);
        }
		
	}
}

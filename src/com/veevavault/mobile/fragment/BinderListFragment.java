package com.veevavault.mobile.fragment;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.androidannotations.annotations.EFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.request.RetrieveDocumentRequest;
import com.veevavault.mobile.dao.request.RetrieveBinderRequest;
import com.veevavault.mobile.dao.request.RetrieveSectionRequest;
import com.veevavault.mobile.dao.response.DAOResponse;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveBinderResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentResponse;
import com.veevavault.mobile.dao.response.RetrieveSectionResponse;
import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.BindableIdentifier;
import com.veevavault.mobile.model.BindableType;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Section;
import com.veevavault.mobile.view.EndlessScrollListener;

@EFragment
public class BinderListFragment extends DocumentListFragment
   implements RetrieveDocumentRequest.Listener,
   RetrieveBinderRequest.Listener,
   RetrieveSectionRequest.Listener {
 
   ArrayList<Bindable> allTheBindablesList;
   protected String binderId;
   private int currentEntries;
   private static final int DOC_REQUEST_LIMIT = 20;
   
   public void paginateListView() {
      listView.setOnScrollListener(new EndlessScrollListener(10) {
         @Override
         public void onLoadMore(int page, int totalItemsCount) {
            if (currentEntries < allTheBindablesList.size()) {
               bindableList.add(allTheBindablesList.get(currentEntries));
               updateAdapterOnResponse();
               currentEntries++;
            } else {
               spinner.setVisibility(View.GONE);
            }
         }
      });
   }   

   private void requestRetrieveBinder() {
      RetrieveBinderRequest req = new RetrieveBinderRequest();
      req.setCallback(this);
      req.setContext(context);
      req.setOffline(onDevice);
      req.setBinderId(new BigInteger(binderId));
      System.out.println("binderId : " + binderId);
      req.execute();
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
         Bundle savedInstanceState) {
      this.binderId = this.getArguments().getString("binderId");
      pagerView = inflater.inflate(R.layout.fragment_pager_list, container, false);
      context = inflater.getContext();
      spinner = (ProgressBar) pagerView.findViewById(R.id.l_progress_bar);
      initDocList();
      setViewType(viewState);
      spinner.setVisibility(View.VISIBLE);
      paginateListView();

      return pagerView;
   }

   public void callSuperInitDocList() {
      super.initDocList();
      currentEntries = DOC_REQUEST_LIMIT;
      allTheBindablesList = new ArrayList<Bindable>();
   }
   
   public void initDocList() {  
      this.callSuperInitDocList();
      requestRetrieveBinder();
   }
   
   public void requestRetrieveDocument(BigInteger docId) {
      RetrieveDocumentRequest req = new RetrieveDocumentRequest();
      req.setDocumentId(docId);
      req.setCallback(this);
      req.setContext(getActivity());
      req.execute();
   }
   
   public void requestRetrieveSection(BigInteger binderId, BigInteger sectionId) {
      RetrieveSectionRequest req = new RetrieveSectionRequest();
      req.setCallback(this);
      req.setContext(context);
      req.setOffline(onDevice);
      req.setBinderId(binderId);
      req.setSectionId(sectionId);
      req.execute();
   }

   public void onRetrieveBinderResponse(RetrieveBinderResponse response) {
      if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
         handleBindableListOnResponse(response.getBinderContents());
      }
      else {
         if (!(response != null && response.getBinderContents() != null
               && response.getBinderContents().isEmpty())) {
            Toast.makeText(getActivity(), "Failure to retrieve binder!", Toast.LENGTH_SHORT).show();
         }
         updateAdapterOnResponse();
      }
   }
   
   public void handleBindableListOnResponse(List<BindableIdentifier> bindablelist) {
      if (bindablelist != null && bindablelist.size() > 0) {
         for (BindableIdentifier ident : bindablelist) {
            //Section case
            if (ident.getType() == BindableType.SECTION) {
               requestRetrieveSection(new BigInteger("" + binderId), ident.getId());
            }
            //Document and Binder case
            else {
               requestRetrieveDocument(ident.getId());
            }               
         }
      }
   }

   
   @Override
   public void onRetrieveDocumentResponse(RetrieveDocumentResponse response) {
      if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
         Document doc = response.getDocument();
         allTheBindablesList.add(doc);
         if (doc.getThumbnail() == null) {
            requestDocumentThumbnail(doc);
         }
         if (bindableList.size() < DOC_REQUEST_LIMIT) {
            bindableList.add(doc);
         }
      }
      updateAdapterOnResponse();
   }

   @Override
   public void onRetrieveSectionResponse(RetrieveSectionResponse response) {
      if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
         Section section = response.getSection();
         allTheBindablesList.add(section);
         if (bindableList.size() < DOC_REQUEST_LIMIT) {
            bindableList.add(section);
         }
      }
      updateAdapterOnResponse();
   }

   public void refresh() {
      super.refresh();
      paginateListView();
   }
}

package com.veevavault.mobile.fragment;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.adapter.SearchHistoryListAdapter;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;
import com.veevavault.mobile.dao.request.RetrieveSearchHistoryRequest;
import com.veevavault.mobile.dao.response.RetrieveDocumentsResponse;
import com.veevavault.mobile.dao.response.RetrieveSearchHistoryResponse;

public class SearchHistoryFragment extends Fragment implements RetrieveSearchHistoryRequest.Listener {
	RetrieveSearchHistoryRequest searchHistoryRequest;
	RetrieveSearchHistoryResponse searchHistoryResponse;
	Map<RetrieveDocumentsRequest, RetrieveDocumentsResponse> searchRequestMap;
	List<RetrieveDocumentsRequest> searchRequestList;
	SearchHistoryListAdapter listAdapter;
	OnSearchSelectListener callback;
	Context context;
	ListView listView;
	RelativeLayout searchText;
	TextView emptyView;
	ProgressBar spinner;

	@Override 
	public View onCreateView(LayoutInflater inflater, ViewGroup container,  
			Bundle savedInstanceState) {  
		View v = inflater.inflate(R.layout.search_history_view, container, false);
		this.context = inflater.getContext();
		listView = (ListView) v.findViewById(R.id.list);
		emptyView = (TextView) v.findViewById(android.R.id.empty);
		searchText = (RelativeLayout) v.findViewById(R.id.search_text_container);
		spinner = (ProgressBar) v.findViewById(R.id.s_progress_bar);
		this.searchRequestList = new ArrayList<RetrieveDocumentsRequest>();
		setHasOptionsMenu(true);
		
		listView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				RetrieveDocumentsRequest request = searchRequestList.get(position);
				request.setContext(context);
				callback.launchSearchActivity(request.getScope(), request.getSearch());	
			}
		});
		
		return v;  
	}  

	@Override 
	public void onActivityCreated(Bundle savedInstanceState) {  
		super.onActivityCreated(savedInstanceState);  

		fillRequestList();
	} 

	private void fillRequestList(){
		spinner.setVisibility(View.VISIBLE);
		searchHistoryRequest = new RetrieveSearchHistoryRequest();
		searchHistoryRequest.setCallback(this);
		searchHistoryRequest.setContext(context);
		searchHistoryRequest.execute();
	}

	@Override
	public void onRetrieveSearchHistory(RetrieveSearchHistoryResponse response) {

		searchHistoryResponse = response;
		this.searchRequestMap = searchHistoryResponse.getSearchHistory();

		if(this.searchRequestMap.isEmpty()){
			this.searchRequestList = new ArrayList<RetrieveDocumentsRequest>();
		}
		else {
			this.searchRequestList = new ArrayList<RetrieveDocumentsRequest>(this.searchRequestMap.keySet());
		}

		listAdapter = new SearchHistoryListAdapter(this.context, searchRequestList);
		listView.setAdapter(listAdapter);
			
		this.listAdapter.notifyDataSetChanged();
		spinner.setVisibility(View.GONE);
		if(searchRequestList.isEmpty()) {
			listView.setEmptyView(emptyView);
		}
		else {
			searchText.setVisibility(View.VISIBLE);
		}
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	    
	    menu.findItem(R.id.action_switch_view).setVisible(false);
	    menu.findItem(R.id.action_switch_view).setEnabled(false);
	    
	    menu.findItem(R.id.refresh).setVisible(false);
	    menu.findItem(R.id.refresh).setEnabled(false);
	}
	
	@Override
    public void onAttach(Activity activity) {
		callback = (OnSearchSelectListener) activity;
		//context = (AppContext) activity;
        super.onAttach(activity);
	}

	public interface OnSearchSelectListener {
		public void launchSearchActivity(String scope, String search);
	}
}

package com.veevavault.mobile.fragment;

import java.util.LinkedList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.InstanceState;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Point;
import android.os.Build;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.veevavault.mobile.R;
import com.veevavault.mobile.activity.DownloadDocument;
import com.veevavault.mobile.activity.LibraryActivity;
import com.veevavault.mobile.adapter.BindableGridAdapter;
import com.veevavault.mobile.adapter.BindableListAdapter;
import com.veevavault.mobile.dao.request.DAORequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentFileUrlRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentThumbnailRequest;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveDocumentFileUrlResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentThumbnailResponse;
import com.veevavault.mobile.model.Bindable;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.view.DocumentListItemView.OnDownloadDocumentListener;

@EFragment
public abstract class DocumentListFragment extends Fragment implements RetrieveDocumentThumbnailRequest.Listener, OnDownloadDocumentListener,
   RetrieveDocumentFileUrlRequest.Listener {
	protected List<Bindable> bindableList;
	protected BindableListAdapter adapter = null;

	@Getter
	protected View pagerView;
	protected ProgressBar spinner;
	
	protected OnListItemSelect callback;
	protected Context context;
	protected AbsListView listView;
	@InstanceState
	protected int viewState;	
	protected boolean onDevice;
	private TextView emptyView;
	private int width = -1;

	@Getter
	@Setter
	private static Document selectedDocument;

	public void requestDocumentThumbnail(Document doc) {
		RetrieveDocumentThumbnailRequest daoRequest = new RetrieveDocumentThumbnailRequest();
		daoRequest.setDocumentId(doc.getId());
		daoRequest.setMinorVersion(doc
				.getPropertyValue("minor_version_number__v"));
		daoRequest.setMajorVersion(doc
				.getPropertyValue("major_version_number__v"));
		daoRequest.setCallback(this);
		daoRequest.setContext(context);

		daoRequest.execute();
	}

	@Override
	public void onResume() {
		if(this.adapter != null){
			adapter.notifyDataSetChanged();
		}
		super.onResume();
	}
	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
      super.onConfigurationChanged(newConfig);
      width = -1;
      if (listView != null && viewState == LibraryActivity.GRIDVIEW) {
         updateGridColumns(listView);
      }
  }

	public void initDocList() {
		bindableList = new LinkedList<Bindable>();
		spinner.setVisibility(View.VISIBLE);
	}

	public void setViewType(int viewType) {
		this.viewState = viewType;      
		updateViewType();
	}

	@SuppressLint("NewApi")
   public void updateGridColumns(AbsListView view) {
	   GridView gridV = (GridView)listView;
	   listView.setEmptyView(pagerView.findViewById(android.R.id.empty));
       Point size = new Point();

       if (width == -1) {
          if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB)    {
              getActivity().getWindowManager().getDefaultDisplay().getSize(size);
              width = size.x;
          }else{
              Display d = getActivity().getWindowManager().getDefaultDisplay(); 
              width = d.getWidth();
          }
       }
       int column_width = (int) getResources().getDimension(R.dimen.gridview_width);
       gridV.setNumColumns(width / column_width);
	}

   public void updateViewType() {
		if (pagerView != null) {
			if (viewState == LibraryActivity.LISTVIEW) {
				adapter = new BindableListAdapter(getActivity(), this, bindableList);
				((AbsListView) pagerView.findViewById(R.id.pager_gridview)).setVisibility(View.GONE);            
				listView = (AbsListView) pagerView.findViewById(R.id.pager_listview);
			}
			else {
				adapter = new BindableGridAdapter(getActivity(), this, bindableList);
				((AbsListView) pagerView.findViewById(R.id.pager_listview)).setVisibility(View.GONE);
				listView = (AbsListView) pagerView.findViewById(R.id.pager_gridview);
				updateGridColumns(listView);
			}
			listView.setVisibility(View.VISIBLE);
			listView.setAdapter(adapter);
			listView.setOnItemClickListener(new OnItemClickListener() {
				@Override
				public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
					callback.onListItemSelect(bindableList.get(position));
				}
			});      
		}
	}
   
	@Override
	public void onThumbnailResponse(RetrieveDocumentThumbnailResponse response) {
		if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
			for (Bindable bindable : bindableList) {
			   if (bindable.getClass() == Document.class) {
   			   Document document = (Document)bindable;
   				if (document.getId().equals(response.getDocumentId())) {
   					document.setThumbnail(response.getImage());
   					break;
   				}
			   }
			}
		} else {
			Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.error_thumbnail), Toast.LENGTH_SHORT).show();
		}
	}

	public void refresh() {
		DAORequest.clearRetrieveDocumentCache();
		initDocList();
		updateViewType();
	}

	@Override
   public void onRetrieveDocumentFileUrlResponse(RetrieveDocumentFileUrlResponse response) {
      Document selectedDoc = response.getDocument();
      Log.d(null, "doc url: " + response.getDocumentFileUrl());

       String format = selectedDoc.getPropertyValue("format__v");
       if (format.equals(""))
       {
         Toast.makeText(getActivity(), getResources().getString(R.string.download_doc_property_null),
                   Toast.LENGTH_SHORT).show();
       }
       else
       {
          String name = selectedDoc.getPropertyValue("name__v");
          name = name.replace(" ", "%20");
          Log.d(null, "doc info: " + name + " " + format);
         DownloadDocument downloadDoc = new DownloadDocument(selectedDoc, response.getDocumentFileUrl(), getActivity(), name, format, response.getSessionId());
          if (selectedDoc.getFilePath() != null)
          {
            downloadDoc.launchApplicationIntent();
          }
          else
          {
            downloadDoc.downloadDocument();
            downloadDoc.setUpBroadcastReciever();
         }
      }
   }
	
  protected void updateAdapterOnResponse() {
      if (adapter != null) {
         adapter.notifyDataSetChanged();
      }
      spinner.setVisibility(View.GONE);
      if(bindableList.isEmpty()){
         emptyView = (TextView) pagerView.findViewById(android.R.id.empty);
         listView.setEmptyView(emptyView);
      }
   }

	@Override
	public void onDownloadDocumentSelect(Document doc) {
		RetrieveDocumentFileUrlRequest req = new RetrieveDocumentFileUrlRequest();
		req.setDocument(doc);
		req.setCallback(this);
		req.setContext(context);
		req.execute();
	}
	
	@Override
	public void onAttach(Activity a) {
		callback = (OnListItemSelect) a;
		//context = (AppContext) a;
		super.onAttach(a);
	}
	

   public interface OnListItemSelect {
      public void onListItemSelect(Bindable bindable);
   }
}

package com.veevavault.mobile.fragment;

import java.math.BigInteger;

import org.androidannotations.annotations.EFragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.veevavault.mobile.dao.request.RetrieveDocumentRequest;
import com.veevavault.mobile.dao.request.RetrieveSectionRequest;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveSectionResponse;
import com.veevavault.mobile.model.Section;

@EFragment
public class SectionListFragment extends BinderListFragment
   implements RetrieveDocumentRequest.Listener,
   RetrieveSectionRequest.Listener {
 
   private String sectionId;

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container,
         Bundle savedInstanceState) {
      this.sectionId = this.getArguments().getString("sectionId");
      return super.onCreateView(inflater, container, savedInstanceState);
   }
   
   @Override
   public void initDocList() {
      this.callSuperInitDocList();
      requestRetrieveSection(new BigInteger(binderId), new BigInteger(sectionId));
   }
   
   @Override
   public void onRetrieveSectionResponse(RetrieveSectionResponse response) {
      if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
         if (response.getSection().getId().equals(new BigInteger(sectionId))) {
            handleBindableListOnResponse(response.getSectionContents());
         }
         else {
            Section section = response.getSection();
            bindableList.add(section);
         }
      }
      else {
         if (!(response != null && response.getSectionContents() != null
               && response.getSectionContents().isEmpty())) {
            Toast.makeText(getActivity(), "Failure to retrieve section!", Toast.LENGTH_SHORT).show();
         }
      }
      updateAdapterOnResponse();
   }
}

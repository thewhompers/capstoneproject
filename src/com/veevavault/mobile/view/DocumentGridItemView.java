package com.veevavault.mobile.view;

import java.util.HashMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Property;
import com.veevavault.mobile.view.DocumentListItemView.OnDownloadDocumentListener;

public class DocumentGridItemView extends LinearLayout {
	   private static final int PROD_OFFSET = 2;

	   private TextView name, description, status;
	   private ImageView thumbnail;
	   private Document document;
	   private OnDownloadDocumentListener callback;

	   public DocumentGridItemView(Context context, OnDownloadDocumentListener callback2, Document doc) {
	      super(context);
	      
	      document = doc;
	      this.callback = (OnDownloadDocumentListener) callback2;
	      LayoutInflater inflater = (LayoutInflater) context
	            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	      HashMap<String, Property> dprops = doc.getPropertiesByName();
	      View rowView = inflater.inflate(R.layout.document_grid_item, this, true);
	      String nameStr = doc.getPropertyValue("name__v");
	      String statusStr = doc.getPropertyValue("status__v");
	      name = (TextView) rowView.findViewById(R.id.g_documentName);
	      status = (TextView) rowView.findViewById(R.id.g_documentStatus);
	      
	      if (doc.getThumbnail() != null) {
	         thumbnail = (ImageView) rowView.findViewById(R.id.g_documentThumbnail);
	         
	         Bitmap bmp = doc.getThumbnail();
	         int size = (bmp.getWidth() > bmp.getHeight()) ? bmp.getHeight() : bmp.getWidth();
	         
	         bmp = Bitmap.createBitmap(bmp, 0, 0, size, size);
	         thumbnail.setImageBitmap(bmp);
	      }
	      
	      name.setText(nameStr);
	      status.setText(statusStr);
	      //doc.setDocumentId(Integer.parseInt(dprops.get("id").getValue()));
	   }

}

package com.veevavault.mobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.Section;

public class SectionGridItemView extends LinearLayout {
	   private TextView name, status;
	   private ImageView thumbnail;

	   public SectionGridItemView(Context context, Section section) {
	      super(context);	      
	      LayoutInflater inflater = (LayoutInflater) context
	            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	      View rowView = inflater.inflate(R.layout.document_grid_item, this, true);
	      
	      String nameStr = section.getName();
	      name = (TextView) rowView.findViewById(R.id.g_documentName);
	      name.setText(nameStr);
	      
	      status = (TextView) rowView.findViewById(R.id.g_documentStatus);
	      status.setVisibility(View.INVISIBLE);
	      
         thumbnail = (ImageView) rowView.findViewById(R.id.g_documentThumbnail);
         thumbnail.setImageResource(R.drawable.icon_section);	      
	   }

}

package com.veevavault.mobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.request.RetrieveDocumentsRequest;

public class SearchHistoryItemView extends LinearLayout{

	TextView searchField;
	TextView searchScope;
	RetrieveDocumentsRequest searchHistoryRequest;
	Context context;

	public SearchHistoryItemView(Context context, RetrieveDocumentsRequest searchRequest){
		super(context);
		this.context = context;
		this.searchHistoryRequest = searchRequest;
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.search_history_list_item, this, true);

		this.searchField = (TextView) findViewById(R.id.s_searchedItem);
		this.searchScope = (TextView) findViewById(R.id.s_searchedOption);
		
		this.searchField.setText(searchRequest.getSearch());
		this.searchScope.setText(searchRequest.getScope());
	}


}
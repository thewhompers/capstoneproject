package com.veevavault.mobile.view;

import java.util.Calendar;
import java.util.GregorianCalendar;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.dao.request.RetrieveBinderRequest;
import com.veevavault.mobile.dao.request.RetrieveDocumentRequest;
import com.veevavault.mobile.dao.response.ResponseStatus;
import com.veevavault.mobile.dao.response.RetrieveBinderResponse;
import com.veevavault.mobile.dao.response.RetrieveDocumentResponse;
import com.veevavault.mobile.model.Document;
import com.veevavault.mobile.model.Task;

public class TaskListItemView extends LinearLayout implements
   RetrieveDocumentRequest.Listener, RetrieveBinderRequest.Listener {
	TextView taskName, docName, dateAssigned,
						dueDate, workflowOwner, taskComment;
	ImageView statusIcon;
	Task task;
	Document document;
	Context context;

	public Drawable setThumbnailIconBasedOnDate(String dateDueStr) {
	   String[] list = dateDueStr.split("/");
	   Calendar duedate = new GregorianCalendar();
	   duedate.set(Calendar.YEAR, Integer.parseInt(list[2]) + 2000);
	   duedate.set(Calendar.MONTH, Integer.parseInt(list[0]) - 1);
	   duedate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(list[1]));
	   duedate.add(Calendar.DAY_OF_YEAR, -1);
	   
	   Calendar rightNow = Calendar.getInstance();
	   
	   if (rightNow.after(duedate)) {
	      return getResources().getDrawable(R.drawable.workflow_status_late);	      
	   }
	   
	   duedate.add(Calendar.DAY_OF_YEAR, -5);
	   if (rightNow.after(duedate)) {
	      return getResources().getDrawable(R.drawable.workflow_status_warn);
	   }
	   
	   return getResources().getDrawable(R.drawable.workflow_status_good);
	}
	
	//This is my branch
	public TaskListItemView(Context context, Task task) {
		super(context);
		this.context = context;
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.task_list_item, this, true);
		
		this.taskName = (TextView) findViewById(R.id.t_task);
		this.docName = (TextView) findViewById(R.id.t_documentName);
		this.dateAssigned = (TextView) findViewById(R.id.t_dateAssigned);
		this.dueDate = (TextView) findViewById(R.id.t_dueDate);
		this.workflowOwner = (TextView) findViewById(R.id.t_workflowOwner);
		this.statusIcon = (ImageView) findViewById(R.id.task_thumbnail);

		setTask(task);
	}

	public void setTask(Task task) {
		this.task = task;
		this.taskName.setText(task.getTaskName());
        //TODO: Going to need to access some form of document id list to get the name
		if (task.getDocName() != null) {
		   this.docName.setText(task.getDocName());
		}
		else {
		   this.docName.setText("ID " + task.getDocId());
		}
		this.dateAssigned.setText(task.getDateAssigned());
		this.dueDate.setText(task.getDateDue());
		this.workflowOwner.setText(task.getWorkflowInitiatorName());		
		this.statusIcon.setImageDrawable(setThumbnailIconBasedOnDate(task.getDateDue()));
		
		RetrieveDocumentRequest req = new RetrieveDocumentRequest();
	   req.setDocumentId(task.getDocId());
	   req.setCallback(this);
	   req.setContext(context);
	   req.execute();
	    
	   RetrieveBinderRequest binderRequest = new RetrieveBinderRequest();
	   binderRequest.setBinderId(task.getDocId());
	   binderRequest.setCallback(this);
	   binderRequest.setContext(context);
	   binderRequest.execute();
	}
	
	public Task getTask(){
		return this.task;
	}
	
	@Override
	public void onRetrieveDocumentResponse(RetrieveDocumentResponse response) {
	    if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
	       document = response.getDocument();
	       task.setDocName(document.getPropertyValue("name__v"));
	       this.docName.setText(document.getPropertyValue("name__v"));
	    }
	}
   
   @Override
   public void onRetrieveBinderResponse(RetrieveBinderResponse response) {
       if (response.getResponseStatus() == ResponseStatus.SUCCESS) {
          document = response.getBinderDocument();
          task.setDocName(document.getPropertyValue("name__v"));
          this.docName.setText(document.getPropertyValue("name__v"));
       }
   }
}

package com.veevavault.mobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.Section;

public class SectionListItemView extends RelativeLayout {

   private TextView name, description, status;
   private ImageView actionButton, thumbnail;

   public SectionListItemView(Context context, Section section) {
      super(context);
      
      LayoutInflater inflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View rowView = inflater.inflate(R.layout.view_document, this, true);
      
      name = (TextView) rowView.findViewById(R.id.doc_docname);      
      name.setText(section.getName());
      
      thumbnail = (ImageView) rowView.findViewById(R.id.doc_thumbnail);
      thumbnail.setImageResource(R.drawable.icon_section);
      
      description = (TextView) rowView.findViewById(R.id.doc_docdescription);
      status = (TextView) rowView.findViewById(R.id.doc_status);
      actionButton = (ImageView) rowView.findViewById(R.id.doc_action_icon);
      
      description.setVisibility(View.INVISIBLE);
      status.setVisibility(View.INVISIBLE);
      actionButton.setVisibility(View.INVISIBLE);      
   }   
}

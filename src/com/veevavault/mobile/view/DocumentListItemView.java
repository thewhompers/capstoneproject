package com.veevavault.mobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.Document;

public class DocumentListItemView extends RelativeLayout {
   //private static final int MAX_TITLE_LEN = 18;

   private TextView name, description, status;
   private ImageView actionButton, thumbnail;
   private Document document;
   private OnDownloadDocumentListener callback;

   private String buildDescription() {
      String product = document.getPropertyValueByName("product__v");
      String docNum = document.getPropertyValueByName("document_number__v");
      String type = document.getPropertyValueByName("type__v");

      return docNum + "\n" + product + " | " + type;
   }

   public DocumentListItemView(Context context, OnDownloadDocumentListener callback, Document doc) {
      super(context);
      
      document = doc;
      this.callback = callback;
      LayoutInflater inflater = (LayoutInflater) context
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
      View rowView = inflater.inflate(R.layout.view_document, this, true);
      String nameStr = doc.getPropertyValue("name__v");
      String statusStr = doc.getPropertyValue("status__v");
      name = (TextView) rowView.findViewById(R.id.doc_docname);
      description = (TextView) rowView.findViewById(R.id.doc_docdescription);
      status = (TextView) rowView.findViewById(R.id.doc_status);
      actionButton = (ImageView) rowView.findViewById(R.id.doc_action_icon);
      
      if (doc.getFilePath() != null) {
         actionButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_action_open));
      }
      
      if (doc.getThumbnail() != null) {
         thumbnail = (ImageView) rowView.findViewById(R.id.doc_thumbnail);
         thumbnail.setImageBitmap(doc.getThumbnail());
      }
      
      description.setText(buildDescription());
      name.setText(nameStr);
      status.setText(statusStr);
      actionButton.setTag(doc);
      if (doc.getPropertyValue("binder__v").equals("true")) {
         actionButton.setVisibility(View.INVISIBLE);
      }
      else {
         initListeners();
      }
   }
   
   private void initListeners() {
      actionButton.setOnClickListener(new OnClickListener() {
         @Override
         public void onClick(View v) {
            callback.onDownloadDocumentSelect(document);
         }
      });
   }

   public interface OnDownloadDocumentListener {
 	  public void onDownloadDocumentSelect(Document doc);
   }

}

package com.veevavault.mobile.view;

import java.util.Random;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.ActiveWorkflow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class WorkflowListItemView extends LinearLayout {
	
	/** Active Workflow associated with this view **/
	ActiveWorkflow activeWorkflow;
	
	/** Workflow status icon:
	 * 		Green: 5+ days until a task is due
	 * 		Yellow: <= 5 days until a task is due
	 * 		Red: Task is past due **/
	ImageView workflowStatusIcon;
	
	/** LinearLayout containing the textviews **/
	LinearLayout infoContainer;
	
	/** Workflow Name **/
	TextView workflowName;
	
	/** Document name associated with the given workflow **/
	TextView documentName;
	
	/** Workflow status description **/
	TextView statusDescription;

	
	/** Image view in layout **/
	ImageView m_vwCreatureImageView;

	public WorkflowListItemView(Context context, ActiveWorkflow activeWorkflow) {
		super(context);
		
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.workflow_listitem_view, this, true);

		this.workflowStatusIcon = (ImageView) findViewById(R.id.workflow_listitem_status_icon);
		this.workflowName = (TextView) findViewById(R.id.workflow_listitem_name);
		this.statusDescription = (TextView) findViewById(R.id.workflow_listitem_status_description);
		this.infoContainer = (LinearLayout) findViewById(R.id.workflow_listitem_info_container);
		
		setActiveWorkflow(activeWorkflow);
	}
	
	public void setActiveWorkflow(ActiveWorkflow workflow) {
		this.activeWorkflow = workflow;
		this.workflowName.setText(workflow.getWorkflowName() + ":" + " " + workflow.getDocument().getPropertyValue("name__v"));
		
		setStatus();
	}
	
	private void setStatus() {
		Random rand = new Random();
		switch (rand.nextInt(3)) {
		case 0:
			this.workflowStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.workflow_status_good));
			this.statusDescription.setVisibility(View.GONE);
			break;
		case 1:
			this.workflowStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.workflow_status_warn));
			this.statusDescription.setText(getResources().getString(R.string.workflow_listview_status_warn));
			break;
		case 2:
			this.workflowStatusIcon.setImageDrawable(getResources().getDrawable(R.drawable.workflow_status_late));
			this.statusDescription.setText(getResources().getString(R.string.workflow_listview_status_late));
			break;
		default:
		}
	}
	
	public ActiveWorkflow getWorkflow() {
		return this.activeWorkflow;
	}
}













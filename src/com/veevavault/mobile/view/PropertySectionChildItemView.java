package com.veevavault.mobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.Property;

public class PropertySectionChildItemView extends LinearLayout {
	
	TextView label;
	TextView value;

	public PropertySectionChildItemView(Context context, String label, Property list) {
		super(context);

	    LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	      inflater.inflate(R.layout.properties_section_child_item, this, true);
		
		this.label = (TextView) findViewById(R.id.p_propertyLabel);
		this.value = (TextView) findViewById(R.id.p_propertyValue);
		
		this.label.setText(list.getLabel());
		this.value.setText(list.getValue());
	}

}

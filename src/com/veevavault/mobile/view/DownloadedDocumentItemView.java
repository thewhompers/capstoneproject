package com.veevavault.mobile.view;

import com.veevavault.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DownloadedDocumentItemView extends LinearLayout{

	TextView documentName;
	String documentFilePath;
	String documentFileType;
	Context context;

	public DownloadedDocumentItemView(Context context, String documentFilePath){
		super(context);
		this.context = context;
		this.documentFilePath = documentFilePath;

		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.downloaded_document_list_item, this, true);

		this.documentName = (TextView) findViewById(R.id.d_documentName);

		setDocumentName();
	}

	private void setDocumentName(){


		String[] parsedFilePath = documentFilePath.split("/");
		documentName.setText(parsedFilePath[parsedFilePath.length-1]);

	}

}

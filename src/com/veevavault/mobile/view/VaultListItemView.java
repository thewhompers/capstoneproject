package com.veevavault.mobile.view;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.veevavault.mobile.R;
import com.veevavault.mobile.model.VaultEndpoint;

public class VaultListItemView extends LinearLayout {

	private TextView vaultName;

	public VaultListItemView(Context context, VaultEndpoint vault) {
		
		super(context);
		LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		inflater.inflate(R.layout.vault_listitem, this, true);
		
		vaultName = (TextView) findViewById(R.id.vault_nameTextView);
		vaultName.setText(vault.getName());
		
	}
}

package com.veevavault.mobile.view;

import com.veevavault.mobile.R;

import android.content.Context;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class SupportingSectionChildItemView extends LinearLayout {
	
	TextView documentName;
	TextView documentNumber;
	TextView documentVersion;
	TextView documentStatus;

	public SupportingSectionChildItemView(Context context, String name, String number, 
											String version, String status) {
		
		super(context);
	    LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    inflater.inflate(R.layout.supporting_section_child_item, this, true);
		
		this.documentName = (TextView) findViewById(R.id.p_documentName);
		this.documentNumber = (TextView) findViewById(R.id.p_documentNumber);
		this.documentVersion = (TextView) findViewById(R.id.p_documentVersion);
		this.documentStatus = (TextView) findViewById(R.id.p_documentStatus);
		
		this.documentName.setText(name);
		this.documentNumber.setText(number);
		this.documentVersion.setText(version);
		this.documentStatus.setText(status);
	}
}

package com.veevavault.mobile.model;

import lombok.Data;

@Data
public class Group {
   private String id;
   private String name;
}

package com.veevavault.mobile.model;

import lombok.Data;

@Data
public class Product {
   private String id;
   private String name;
}

package com.veevavault.mobile.model;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import lombok.Data;
import lombok.EqualsAndHashCode;

import org.springframework.util.LinkedMultiValueMap;

import android.graphics.Bitmap;

@Data 
@EqualsAndHashCode(callSuper=false, exclude={"databaseId", "thumbnail", "filePath"})
public class Document extends Bindable {
   
	private Integer databaseId;
	private String filePath;
   private Set<Property> properties;
	private String vaultId;
	private Bitmap thumbnail;
	private boolean isBinder;
   
   public Document() {
      
   }
   
   public Document(String vaultId) {
      this.vaultId = vaultId;
   }
    
    public String getPropertyValue(String str) {
       for (Property p : properties) {
          if (p.getName().equals(str))
             return p.getValue();
       }
       return "";
    }
    
    public Property getPropertyByName(String str) {
       for (Property p : properties) {
          if (p.getName().equals(str))
             return p;
       }
       
       return null;
    }
    
    public String getPropertyValueByName(String str) {
       for (Property p : properties) {
          if (p.getName().equals(str))
             return p.getValue();
       }
       
       return null;
    }
    
    public void addProperty(Property p) {
       if (properties == null) {
          this.properties = new HashSet<Property>();
       }
       properties.add(p);
    }
    
    public byte[] getThumbnailAsByteArray() {
       ByteArrayOutputStream stream = new ByteArrayOutputStream();
       thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream);
       byte[] byteArray = stream.toByteArray();

       return byteArray;
    }
    
    public HashMap<String, Property> getPropertiesByName() {
       HashMap<String, Property> rtn = null;
       
       if (properties != null) {
          rtn = new HashMap<String, Property>();
          for (Property p : properties) {
             if (!p.isHidden())
                rtn.put(p.getName(), p);
          }
       }
       
       return rtn;
    }
    
   public LinkedMultiValueMap<String, Property> getPropertiesBySection() {
      if (properties == null) {
         return null;
      }
    
      LinkedMultiValueMap<String, Property> unsortedSections =
         new LinkedMultiValueMap<String, Property>();
      
      for (Property p : properties) {
         if (!p.isHidden())
            unsortedSections.add(p.getSection(), p);
      }

      List<String> sectionNames = new ArrayList<String>(unsortedSections.keySet());
      Collections.sort(sectionNames, new Property.PropertySectionComparator());
      
      LinkedMultiValueMap<String, Property> sortedSections =
         new LinkedMultiValueMap<String, Property>();
      for (String sectionName : sectionNames) {
         sortedSections.put(sectionName, unsortedSections.get(sectionName));
      }
      
      return sortedSections;
   }
    
  /*  @Override
    public boolean equals(Object o) {
       Document other = (Document) o;
       boolean rtn = true;
       
       if (o == null || !(o instanceof Document))
          return false;
       if (this == o)
          return true;
       
       if (this.databaseId != other.databaseId
             || this.documentId != other.documentId
             || ((this.filePath == null && other.filePath == null) || (this.filePath != null
                   && other.filePath != null
                   && !this.filePath.equals(other.filePath)))
             || this.vaultId != other.vaultId)
          rtn = false;
       
       HashSet<Property> thisProperties = new HashSet<Property>(properties);
       HashSet<Property> otherProperties = new HashSet<Property>(other.properties);
       
       if (!thisProperties.containsAll(otherProperties)
             || thisProperties.size() != otherProperties.size())
          rtn = false;
       
       return rtn;    
    }*/
}

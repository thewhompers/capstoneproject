package com.veevavault.mobile.model;

import java.math.BigInteger;

import lombok.Data;

@Data
public class BindableIdentifier {
   private BindableType type;
   private BigInteger id;
}

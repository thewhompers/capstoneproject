package com.veevavault.mobile.model;

public enum BindableType {
   BINDER,
   DOCUMENT,
   SECTION;
}

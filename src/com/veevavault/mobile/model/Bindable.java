package com.veevavault.mobile.model;

import java.math.BigInteger;

import lombok.Data;

@Data
public abstract class Bindable {
   private BigInteger id;
}

package com.veevavault.mobile.model;

import java.io.ByteArrayOutputStream;
import java.math.BigInteger;

import lombok.Data;

import org.joda.time.DateTime;

import android.graphics.Bitmap;

@Data
public class Task {
   
   public enum TaskStatus {
      AVAILABLE,
      ASSIGNED,
      COMPLETED,
      CANCELLED;
   }
   
	private String taskName;
   private BigInteger docId;
	private String docName;
	private String dateAssigned;
	private String dateDue;
	private String workflowInitiatorName;
	private String taskComment;
	private TaskStatus status;
	private int vaultId;
	private DateTime cachedTime;
	private Bitmap thumbnail;
	private String assigneeId;
	
	public byte[] getThumbnailAsByteArray() {
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		thumbnail.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();

		return byteArray;
	}
}

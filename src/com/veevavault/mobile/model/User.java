package com.veevavault.mobile.model;

import java.util.Arrays;

import lombok.Data;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeZone;

@Data
public class User {
   private String id;
   private String name;
   private String firstName;
   private String lastName;
   private DateTimeZone timezone;
   
   public String getFullName() {      
      return StringUtils.join(Arrays.asList(firstName, lastName), " ");
   }
}

package com.veevavault.mobile.model;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

public class ActiveWorkflow {
	
	/** Document associated with the workflow **/
	@Getter @Setter private Document document;
	
	/** Workflow name (workflow_name__v)
	 * 		Approval, Review, ...
	 */
	@Getter @Setter private String workflowName;
	
	/** Tasks associated with the workflow **/
	@Getter @Setter private List<Task> tasks;
	
	public ActiveWorkflow() {
		
	}
	
	public ActiveWorkflow(Document document, String name) {
		this.document = document;
		this.workflowName = name;
	}

}

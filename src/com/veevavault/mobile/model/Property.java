package com.veevavault.mobile.model;

import java.util.Comparator;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(exclude={"id"})
public class Property implements Comparable<Property> {
   private static final int GENERAL_PROPERTIES_PRECEDENCE = 0;
   private static final int CUSTOM_SECTION_PRECEDENCE = 1;
   private static final int PRODUCT_INFORMATION_PRECEDENCE = 2;
   private static final int SHARING_SETTINGS_PRECEDENCE = 3;
   
   public static class PropertySectionComparator implements Comparator<String> {
      public int compare(String a, String b) {
         int valA = getSectionPrecedence(a);
         int valB = getSectionPrecedence(b);
         
         if (valA == valB) {
            return a.compareTo(b);
         }
         
         return valA - valB;
      }
      
      private int getSectionPrecedence(String sectionName) {
         if (sectionName.equals("General Properties")) {
            return GENERAL_PROPERTIES_PRECEDENCE;
         } else if (sectionName.equals("Product Information")) {
            return PRODUCT_INFORMATION_PRECEDENCE;
         } else if (sectionName.equals("Sharing Settings")) {
            return SHARING_SETTINGS_PRECEDENCE;
         } else {
            return CUSTOM_SECTION_PRECEDENCE;
         }
      }
   }
   
	/** DatabaseID **/
	private Integer id;
	
	/** Property Name **/
	private String name;
	
	/** Label **/
	private String label;
	
	/** Section **/
	private String section;

	/** Section position **/
	private int sectionPosition;
	
	/** Whether or not the property is hidden from the user **/
	private boolean hidden;
	
	/** Whether or not the property is querably **/
	private boolean queryable;
	
	private String value;
	
	public Property() {
		
	}
	
	public Property(Property p) {
	   this.id = p.getId();
	   this.name = p.getName();
	   this.label = p.getLabel();
	   this.section = p.getSection();
	   this.sectionPosition = p.getSectionPosition();
	   this.hidden = p.isHidden();
	   this.queryable = p.isQueryable();
	   this.value = p.getValue();	   
	}

   @Override
   public int compareTo(Property another) {
      return this.sectionPosition - another.getSectionPosition();
   }
	
}

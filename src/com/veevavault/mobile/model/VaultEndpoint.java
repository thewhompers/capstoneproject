package com.veevavault.mobile.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import android.os.Parcel;
import android.os.Parcelable;

@Data
@EqualsAndHashCode(callSuper=false)
public class VaultEndpoint implements Parcelable {
   private String vaultID;
   private String name;
   private String dnsName;
   
   public static final Parcelable.Creator<VaultEndpoint> CREATOR = new Parcelable.Creator<VaultEndpoint>() {
      public VaultEndpoint createFromParcel(Parcel in) {
         VaultEndpoint toReturn = new VaultEndpoint();
         toReturn.setVaultID(in.readString());
         toReturn.setName(in.readString());
         toReturn.setDnsName(in.readString());
         return toReturn;
      }

      public VaultEndpoint[] newArray(int size) {
         return new VaultEndpoint[size];
      }
   };

   @Override
   public int describeContents() {
      // TODO Auto-generated method stub
      return 0;
   }

   @Override
   public void writeToParcel(Parcel dest, int arg1) {
      dest.writeString(vaultID);
      dest.writeString(name);
      dest.writeString(dnsName);
   }
}
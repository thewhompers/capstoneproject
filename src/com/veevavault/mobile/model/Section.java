package com.veevavault.mobile.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class Section extends Bindable {
   private int number;
   private String name;
}
